var mysql = require('mysql');
var mongoose = require('mongoose');
var pool = null;
var notificationSchema = require('../mongo_schemas/notification_schema');

exports.mysqlInit = function (config) {
	pool = mysql.createPool({
    host: config.HOST,
    port: config.PORT,
    user: config.USER,
    password: config.PASSWORD,
    database: config.DB
//    connectionLimit: config.MYSQL.CONNECTIONLIMIT,
//    waitForConnections: config.MYSQL.WAITFORCONNECTIONS
  });
  return pool;
};

exports.mongoInit = function (config) {
  var option = {
    user: config.USER,
    pass: config.PASSWORD
  };
  var DB_URI = 'mongodb://' + config.HOST + ':'+ config.PORT + '/' + config.DB;
  var connection = mongoose.createConnection(DB_URI, option);
  registerModels(connection);
};

function registerModels(connection) {
  var models = {};
  models.Notification = connection.model('Notification', notificationSchema.NotificationSchema);
  exports.models = models;
}