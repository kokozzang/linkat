module.exports = (function () {
  var enums = {};
  var reverseKeyAndValue = function (obj) {
	var keys = Object.keys(obj);
	var newObj = {};
	keys.forEach(function (key) {
    newObj[obj[key]] = key;
	});
    return newObj;
  };


  //결과
  enums.RESULT = {
    FAIL: false,
    SUCCESS: true
  };

  //OAuth
  enums.OAUTH = {
    LOCAL: 0,
    FACEBOOK: 1, //페이스북
    TWITTER: 2  //트위터
  };
  //카테고리

  enums.CATEGORY = {
    CLOTHES: 1, //의류
    SHOES: 2,  //신발
    ETCS: 3 //잡화
  };

  //가격
  enums.PRICE = {
    UNLIMITED: 0,  //무제한
    WON1: 1,  //1만원
    WON3: 3,  //3만원
    WON5: 5   //5만원
  };

  //등급
  enums.USER_GRADE = {
    BRONZE: 1, //브론즈
    SILVER: 2,  //실버
    GOLD: 3       //골드
  };
  //계정상태
  enums.USER_STATE = {
    SUSPENDED: -2,   //계정정지
    DEACTIVATED: -1, //비활성화
    PENDING: 0,      //회원정보미입력
    UNCERTIFIED: 1,  //미인증
    ACTIVE: 2        //활성
  };
  //성별
  enums.USER_GENDER = {
    UNISEX: 0,  //무관
    MALE: 1,    //남성
    FEMALE: 2  //여성
  };
  //사용자 스타일
  enums.USER_STYLE = {
    SEXY: 1,    //섹시한
    MACHO: 2,   //상남자
    FEMININE: 3,//상여자
    CUTE: 4,    //귀여운
    NATURAL: 5, //네츄럴한
    CHIC: 6,    //시크한
    FREE: 7,    //후리한
    LOVELY: 8   //러블리
  };

  enums.EVENT_ID = {
    COMMENT: 1,       //댓글
    QUESTION_LIKE: 2, //나도 궁금
    ANSWER: 3,        //답변
    ANSWER_LIKE: 4   //갖고싶어
  };

  enums.LIKE_TYPE = {
    QUESTION: 0,  //질문
    ANSWER: 1     //답변
  };

  enums.PROFILE_PHOTO_WIDTH = {
    SMALL: 236,
    MEDIUM: 400,
    LARGE: 700
  };
  enums.QUESTION_PHOTO_WIDTH = {
    SMALL: 360,
    MEDIUM: 720,
    LARGE: 1080
  };
  enums.ANSWER_PHOTO_WIDTH = {
    SMALL: 360,
    MEDIUM: 720,
    LARGE: 1080
  };

  enums.IMAGE_CONTENT_TYPE = {
    jpeg: 'image/jpeg',
    jpg: 'image/jpeg',
    png: 'image/png',
    bmp: 'image/bmp'
  };

  enums.LIMIT = {
    QUESTION_CONTENT: 300,  //질문 내용
    ANSWER_CONTENT: 300,  //답변 내용
    ANSWER_PRODUCT: 100, //답변 상품명
    ANSWER_LINK: 300,  //질문 댓글 내용
    QUESTION_COMMENT_CONTENT: 300,  //질문 댓글 내용
    QUESTION_COMMENT_LOAD: 5,  //질문 댓글 로드 갯수
    NOTIFICATION_CONTENT: 50, //알림 내용
    PAGE_LOAD: 25, //한 페이지의 컨텐츠 갯수
    EMAIL: 100,  //이메일
    INTRODUCE: 60,  //자기소개
    NICKNAME: {
      MIN: 2,
      MAX: 16
    },
    USER_URL: {
      MIN: 2,
      MAX: 16
    },
    PASSWORD: { //패스워드
      MIN: 6,
      MAX: 16
    }

  };
  enums.HOST = 'localhost';
  enums.UPLOAD_BASE = 'public/upload/';
  enums.DEFAULT_AVATAR_URL = 'http://localhost:3000/avatar_none.jpg';
  enums.DOMAINS = {
    PRODUCT: 'http://www.chopster.co.kr/',
    DEVELOP: 'http://localhost:3000/'
  };
  enums.NODE_ENV = {
    PRODUCTION: 'production',
    DEVELOP: 'DEVELOP'
  };

  enums.HTTP_STATUS = {
    OK: {
      CODE: 200,
      MESSAGE: 'Success!'
    },
//    NOT_MODIFIED: {
//      CODE: 304,
//      MESSAGE: ''
//    },
    UNAUTHORIZED: {
      CODE: 401,
      MESSAGE: 'Could not authenticate you'
    },
    FORBBIDEN: {
      CODE: 403,
      MESSAGE: 'Your account is suspended and is not permitted to access this feature!'
    },
    NOT_FOUND: {
      CODE: 404,
      MESSAGE: 'Sorry, that page does not exist'
    },
    INTERNAL_SERVER_ERROR: {
      CODE: 500,
      MESSAGE: 'Internal error'
    }
  };
  enums.MSG_VALIDATION = {
    NOT_EMPTY: 'notEmpty() - 파라미터가 비어있습니다.',
    IS_IN: 'isIn() - 상수 정의에 없는 값입니다.',
    IS_INT: 'isInt() - 정수가 아닙니다.',
    LEN: 'len() - 파라미터의 길이가 맞지 않습니다.',
    IS_POSITIVE_NUMBER: 'isPositiveNumber() - 양의 정수가 아닙니다.',
    IS_IMAGE: 'isImage() - 첨부파일이 이미지가 아닙니다.',
    IS_URL: 'isURL() - URL 형식에 맞지 않습니다.',
    IS_EMAIL: 'isEmail() - Email 형식에 맞지 않습니다.',
    IS_DATE: 'isDate() - 날짜 형식에 맞지 않습니다.',
    IS_ALPHANUMERIC: 'isAlphanumeric() - 영어, 숫자외의 값이 포함되어 있습니다.',
    IS_ALPHAKORNUMERIC: 'isAlphaKorNumeric() - 영어, 한글, 숫자외의 값이 포함되어 있습니다.'
  };
  (function setNames() {
    enums.OAUTH_NAMES = reverseKeyAndValue(enums.OAUTH);
    enums.CATEGORY_NAMES = reverseKeyAndValue(enums.CATEGORY);
    enums.USER_GRADE_NAMES = reverseKeyAndValue(enums.USER_GRADE);
    enums.USER_STATE_NAMES = reverseKeyAndValue(enums.USER_STATE);
    enums.USER_GENDER_NAMES = reverseKeyAndValue(enums.USER_GENDER);
    enums.USER_STYLE_NAMES = reverseKeyAndValue(enums.USER_STYLE);
    enums.LIKE_TYPE_NAMES = reverseKeyAndValue(enums.LIKE_TYPE);
    enums.PROFILE_PHOTO_WIDTH_NAMES = reverseKeyAndValue(enums.PROFILE_PHOTO_WIDTH);
    enums.QUESTION_PHOTO_WIDTH_NAMES = reverseKeyAndValue(enums.QUESTION_PHOTO_WIDTH);
    enums.EVENT_ID_NAMES = reverseKeyAndValue(enums.EVENT_ID);
  })();

  return enums;
})();