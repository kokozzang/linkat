var async = require('async');
var _ = require('underscore');
var Notification = require('../db').models.Notification;  //mongodb 알림 모델


module.exports.selectNotificationCount = selectNotificationCount; // 알림 갯수


// 댓글 및 알림대상,알림갯수 가져오기
module.exports.selectCommentAndUserIdsForCommentNotification = selectCommentAndUserIdsForCommentNotification;      // 댓글 입력

// 질문 좋아요 알림대상,알림갯수 가져오기
module.exports.selectUserIdsForQuestionLikeNotification = selectUserIdsForQuestionLikeNotification; //질문 좋아요 입력

module.exports.selectQuestionLikeCount = selectQuestionLikeCount; //질문 좋아요 갯수 가져오기

/**
 *답변
 */
// 답변 및 알림대상,알림갯수 가져오기 //답변 입력용
module.exports.selectAnswerAndUserIdsForAnswerNotification = selectAnswerAndUserIdsForAnswerNotification;
// 알림대상,알림갯수 가져오기 //답변 삭제용
module.exports.selectUserIdsAndCountForAnswerNotification = selectUserIdsAndCountForAnswerNotification;


/**
 *찹
 */
// 알림대상, 알림갯수 가져오기
module.exports.selectUserIdsAndCountForChopNotification = selectUserIdsAndCountForChopNotification;




module.exports.selectNotificationList = selectNotificationList; //알림 목록

module.exports.getEmailAddress = getEmailAddress;



function selectNotificationCount (userId, callback){
  Notification.count({ruid: userId, seen: false}, function(err, count){
    if(err)
      return callback(err, null);
    return callback(null, count);
  });
}

function selectCommentAndUserIdsForCommentNotification (commentId, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }
    async.parallel({
      //댓글 내용
      comment: function (cb){
        var stmt = 'SELECT qc.id, qc.user_id, u.nickname, qc.content, qc.created_date '
        + 'FROM question_comment AS qc '
        + 'INNER JOIN user AS u ON qc.user_id = u.id '
        + 'WHERE qc.id = ?';
        connection.query(stmt, commentId, function (err, result){
          if (err)
            return cb(err, null);
          return cb(null, result[0]);
        });
      },
      //알림대상과 알림 갯수. 답변을 단 본인을 제외한 사용자
      //질문자, 답변자, 질문좋아요, 답변좋아요, 댓글
      userIdsAndCount: function (cb) {
        //댓글 아이디를 기준으로 알림 수신자인 유저들의 아이디를 가져옴
        Notification.find({qcid: commentId}, {_id: 0, ruid: 1}, function(err, userIds){
          var ruid = [];
          for(var i=0; i < userIds.length; i++)
            ruid.push(userIds[i].ruid);

          //알림 수신자인 유저들의 아이디로 아이디와 알림 갯수를 가져옴
          Notification.aggregate(
            {$match: {ruid: {$in : ruid}}},
            {$group: {_id: '$ruid', cnt: {$sum: 1}}}, function (err, result){
              if (err)
                return cb(err, null);
              return cb(null, result);
          });
        });
      }
    }, function (err, results){
      connection.release();
      if (err)
        return callback(err, null);
      return callback(null, results);
    });
  });//end of getConnecton
}

function selectUserIdsForQuestionLikeNotification (questionId, userId, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }
    async.parallel({
      //댓글 내용
      questionLikeCount: function (cb){
        var stmt = 'SELECT count(*) AS cnt FROM question_like WHERE question_id = ?';

        connection.query(stmt, questionId, function (err, result){
          if (err)
            return cb(err, null);
          return cb(null, result[0].cnt);
        });
      },
      userIdsAndCount: function (cb) {
        //질문 좋아요 아이디를 기준으로 알림 수신자인 유저들의 아이디를 가져옴
        Notification.find({qid: questionId, suid: userId, evt: ENUMS.EVENT_ID.QUESTION_LIKE}, {_id: 0, ruid: 1}, function(err, userIds){
          var ruid = [];
          for(var i=0; i < userIds.length; i++)
            ruid.push(userIds[i].ruid);

          //알림 수신자인 유저들의 아이디로 아이디와 알림 갯수를 가져옴
          Notification.aggregate(
            {$match: {ruid: {$in : ruid}}},
            {$group: {_id: '$ruid', cnt: {$sum: 1}}}, function (err, result){
              if (err)
                return cb(err, null);
              return cb(null, result);
            });
        });
      },
      userNickname: function (cb) {
        var stmt = 'SELECT nickname FROM user WHERE id = ?';
        connection.query(stmt, [userId], function (err, result) {
          if (err) { return cb(err, null) }
          return cb(null, result);
        })
      }
    }, function (err, results){
      connection.release();
      if (err)
        return callback(err, null);
      return callback(null, results);
    });
  });//end of getConnecton
}

function selectQuestionLikeCount (questionId, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }
    var stmt = 'SELECT count(*) AS cnt FROM question_like WHERE question_id = ?';
    connection.query(stmt, [questionId], function (err, result){
      if (err)
        return callback(err, null);
      return callback(null, result[0].cnt);
    });
  });//end of getConnecton
}

function selectAnswerAndUserIdsForAnswerNotification (questionId, answerId, userId, callback){
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }
    async.parallel({
      //답변내용
      answer: function (cb){
        var stmt = 'SELECT a.id, a.question_id, u.nickname, a.user_id, '
          + 'a.product_name, a.price, a.link, a.content, g.name AS grade_name, '
          + 'a.created_date, ap.url AS answer_image_url, '
          + 'ap.width AS answer_image_width, ap.height AS answer_image_height, pp.small_url AS profile_small_url '
          + 'FROM answer AS a '
          + 'INNER JOIN answer_photo AS ap '
          + 'ON a.id = ap.answer_id '
          + 'INNER JOIN user AS u '
          + 'ON a.user_id = u.id '
          + 'INNER JOIN grade AS g '
          + 'ON u.grade_id = g.id '
          + 'INNER JOIN profile_photo AS pp '
          + 'ON a.user_id = pp.user_id '
          + 'WHERE a.id = ? AND ap.width = ?';

        connection.query(stmt, [answerId, ENUMS.ANSWER_PHOTO_WIDTH['400x']], function (err, result){
          if (err)
            return cb(err, null);
          return cb(null, result[0]);
        });
      },
      //알림대상과 알림 갯수. 답변을 단 본인을 제외한 사용자
      //질문자, 답변자, 질문좋아요, 답변좋아요, 댓글
      userIdsAndCount: function (cb) {
        //질문 좋아요 아이디를 기준으로 알림 수신자인 유저들의 아이디를 가져옴
        Notification.find({qid: questionId, aid: answerId, evt: ENUMS.EVENT_ID.ANSWER}, {_id: 0, ruid: 1}, function(err, userIds){
          var ruid = [];
          for(var i = 0; i < userIds.length; i++)
            ruid.push(userIds[i].ruid);

          //알림 수신자인 유저들의 아이디로 아이디와 알림 갯수를 가져옴
          Notification.aggregate(
            {$match: {ruid: {$in : ruid}}},
            {$group: {_id: '$ruid', cnt: {$sum: 1}}}, function (err, result){
              if (err)
                return cb(err, null);
              return cb(null, result);
            });
        });
      }
    }, function (err, results){
      connection.release();
      if (err)
        return callback(err, null);
      return callback(null, results);
    });

  });
}

function selectUserIdsAndCountForAnswerNotification (questionId, userId, callback){
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }
    //알림대상과 알림 갯수. 답변을 단 본인을 제외한 사용자
    //질문자, 답변자, 질문좋아요, 답변좋아요, 댓글
    var stmt = 'SELECT user_id FROM ('
      + 'SELECT user_id FROM question WHERE id = ? '
      + 'UNION SELECT user_id FROM question_like WHERE question_id = ? '
      + 'UNION SELECT user_id FROM answer WHERE question_id = ? '
      + 'UNION SELECT user_id FROM answer_like WHERE answer_id IN (SELECT id FROM answer WHERE question_id = ?) '
      + 'UNION SELECT DISTINCT user_id FROM question_comment WHERE question_id = ?'
      + ') AS u WHERE u.user_id <> ?';

    var queryValues = [questionId, questionId, questionId, questionId, questionId, userId];
    connection.query(stmt, queryValues, function (err, userIds){
      if (err){
        connection.release();
        return callback(err, null);
      }
      //알림대상을 이용해 알림 갯수를 가져옴
      selectNotificationCountByUserIds(connection, userIds, function (err, userIdsAndCount) {
        connection.release();
        if (err)
          return callback(err, null);
        return callback(null, userIdsAndCount);
      });
    });
  });
}

function selectUserIdsAndCountForChopNotification (questionId, answerId, userId, callback) {
  //알림 수신자인 유저들의 아이디를 가져옴
  Notification.find({qid: questionId, aid: answerId, suid: userId, evt: ENUMS.EVENT_ID.chop}, {_id: 0, ruid: 1}, function(err, userIds){
    var ruid = [];
    for(var i = 0; i < userIds.length; i++)
      ruid.push(userIds[i].ruid);

    //알림 수신자인 유저들의 아이디로 아이디와 알림 갯수를 가져옴
    Notification.aggregate(
      {$match: {ruid: {$in : ruid}}},
      {$group: {_id: '$ruid', cnt: {$sum: 1}}}, function (err, result){
        if (err)
          return callback(err, null);
        return callback(null, result);
      });
  });
}


function selectNotificationList (userId, callback){
  Notification.aggregate({$match: {ruid: userId}}, {$sort: {cdate: -1}}, {$skip: 0}, {$limit: 50}, function(err, result){
    if (err)
      return callback(err, null);
    return callback(null, result);
  });
}


/**
 * 유저의 아이디들로 각 유저의 알림의 갯수를 가져옴
 * @param connection
 * @param userIds 유저아이디 배열 [{userId: 1}]
 * @param callback
 */
function selectNotificationCountByUserIds (connection, userIds, callback){
  if(connection === null)
    return callback(null, null);
  if(_.isEmpty(userIds))
    return callback(null, null);
  var arrUserIds = [];

  //유저아이디로만 이루어진 배열을 생성
  for(var i = 0; i < userIds.length; i++)
    arrUserIds.push(userIds[i].user_id);

  var stmt = 'SELECT user_id, count(*) AS cnt FROM '
    + 'notification WHERE user_id IN (?) AND viewed = 0 '
    + 'GROUP BY user_id';

  connection.query(stmt, [arrUserIds], function (err, userIdsAndCount){
    if (err)
      return callback(err, null);
    return callback(null, userIdsAndCount);
  });
}


/**
 * 좋아요 발생 알림을 받을 유저의 아이디 리스트를 리턴
 * 알림대상: 질문자, 질문 좋아요 누른 유저
 * 알림제외: 알림을 유발 한 유저
 * @param connnection 커넥션
 * @param questionId  알림이 발생한 질문의 아이디
 * @param userId      알림을 발생시킨 유저의 아이디
 * @param callback    알림 받을 유저의 아이디 리스트
 */
function selectUserListForQuestionLike (connection, questionId, userId, callback){
  //질문자, 댓글 단 유저
  var stmt = 'SELECT user_id '
    + 'FROM ('
    + 'SELECT user_id FROM question WHERE id = ? UNION '
    + 'SELECT DISTINCT user_id FROM question_like WHERE question_id = ?'
    + ') AS u '
    + 'WHERE u.user_id <> ?';

  connection.query(stmt, [questionId, questionId, userId], function (err, result){
    if (err)
      return callback(err, null);
    return callback(null, result);
  });
}
/**
 * 해당 userId의 닉네임을 가져옴
 * @param connection  커넥션
 * @param userId      유저아이디
 */
function selectUserNickname (connection, userId, callback){
  var stmt = 'SELECT nickname FROM user WHERE id = ?';
  connection.query(stmt, [userId], function (err, result){
    if (err)
      return callback(err, null);
    return callback(null, result);
  });

}

function getEmailAddress (userIds, callback) {
  POOL.getConnection(function (err, connection ) {
    if (err) { return callback(err, null);}
    connection.query('SELECT email FROM user WHERE id IN (?)', userIds, function (err, result) {
      connection.release();
      if(err) {
        console.log('err', err);
        return callback(err, null);
      }
      else {
        return callback(null, result);

      }
    })
  });
}
