var async = require('async');
var serviceUtil = require('./service_util');

module.exports.emailDuplicationCheck = emailDuplicationCheck; // 이메일 중복검사
module.exports.userUrlDuplicationCheck = userUrlDuplicationCheck; // url 중복검사
module.exports.registerInfo = registerInfo;      // 회원 정보 입력
module.exports.getUserIdByOAuth = getUserIdByOAuth; // oauth 아이디로 유저 아이디 조회
module.exports.saveUserToken = saveUserToken;
module.exports.checkUserToken = checkUserToken;


function emailDuplicationCheck (email, callback){
  serviceUtil.duplicationChecker('user', 'email', email, function (err, result){
    if(err){ return callback(err, null); }
    return callback(null, result);
  });
}

function userUrlDuplicationCheck (userUrl, callback){
  serviceUtil.duplicationChecker('user', 'user_url', userUrl, function (err, result){
    if(err){ return callback(err, null); }
    return callback(null, result);
  });
}

function registerInfo (userInfo, callback){
  var today = new Date();

  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }
    async.waterfall([
      function transactionStart(cb){
        connection.beginTransaction(function (err){
          if(err)
            return cb(err, null);
          return cb(null, null);
        });
      },
      function updateUserInfo (result, cb) {//user 테이블에 사용자를 정보를 입력함
        var stmt = 'UPDATE user '
          + 'SET '
          + 'nickname = ?, user_url = ?, birth = ?, gender_id = ?, grade_id = ?, '
          + 'password = ?, salt = ? '
          + 'WHERE id = ?';
        var values = [userInfo.nickname, userInfo.user_url, userInfo.birth, userInfo.gender,
          userInfo.grade_id, userInfo.password, userInfo.salt, userInfo.id];
        connection.query(stmt, values, function (err, result){
          if(err)
            return cb(err, null);
          return cb(null, result);
        });
      },
      function updateUserState (result, cb) {//user_state 테이블에 사용자를 상태를 입력함
        var stmt = 'UPDATE user_state '
          + 'SET '
          + 'end_date = ? '
          + 'WHERE user_id = ? AND state_id =  ? AND end_date IS NULL';
        var values = [today, userInfo.id, ENUMS.USER_STATE.PENDING];
        connection.query(stmt, values, function (err, result){
          if(err)
            return cb(err, null);
          return cb(null, result);
        });
      },
      function insertUserState (result, cb) {//user_state 테이블에 사용자를 상태를 입력함\
        var user_state = {
          user_id: userInfo.id,                    //추가된 사용자 아이디
          state_id: ENUMS.USER_STATE.UNCERTIFIED,  //미인증 상태 (메일인증 대기)
          change_date: today,                      //상태 변경 일시
          end_date: null                           //상태 종료 일시
        };
        connection.query('INSERT INTO user_state SET ?', user_state, function (err, result){
          if(err)
            return cb(err, null);
          return cb(null, result);
        });
      },
      function getEmailAddress (result, cb) {
        connection.query('SELECT email FROM user WHERE id = ?', [userInfo.id], function (err, result) {
          if(err)
            return cb(err, null);
          return cb(null, result);
        })
      }
    ],
    function (err, result){
      if(err){
        connection.rollback();
        connection.release();
        return callback(err, null);
      }
      connection.commit(function(err) {
        if (err) {
          connection.rollback();
          connection.release();
          return callback(err, null);
        }
        serviceUtil.getUserInfoById(connection, userInfo.id, function(err, userInfo){
          connection.release();
          if(err)
            return callback(err, null);
          return callback(null, userInfo[0]);
        });//end of getUserInfoById

        //connection.release();
        //console.log('이메일 result', result);
        //return callback(null, result);
      });
    });//end of waterfall
  });//end of getConnecton

}

function saveUserToken(userId, universalId, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) {
      return callback(err, null);
    }
    connection.query('INSERT INTO verification SET user_id = ?, token = ?, created_date = ?', [userId, universalId, new Date()], function (err, result) {
      if (err) {
        return callback(err, null);
      }
      else {
        connection.query('SELECT email FROM user WHERE id = ?', [userId], function (err, email) {
          if (err) {
            return callback(err, null);
          }
          else {
            connection.release();
            console.log('email', email);
            return callback(null, email[0].email);
          }
        });
      }
    });
  });
}

function checkUserToken(userId, token, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) {
      return callback(err, null);
    }
    async.waterfall([
        function transactionStart(cb) {
          connection.beginTransaction(function (err) {
            if (err)
              return cb(err, null);
            return cb(null, null);
          });
        },
        function checkToken(err, cb) {
          var query = 'SELECT * FROM verification WHERE user_id = ? AND token = ?';
          connection.query(query, [userId, token], function (err, result) {
            if (err)
              return cb(err, null);
            console.log('ctr', result);
            return cb(null, result);
          });
        },
        function changeState(ctr, cb) {
          if(ctr[0] == null) {
            console.log('잘 들어왔니?') ;
            return cb(null, false);
          }
          var query = 'UPDATE user_state SET state_id = 2 WHERE user_id = ?';
          connection.query(query, [userId], function (err, result) {
            if (err)
              return cb(err, null);
            console.log('csr', result);
            return cb(null, result);
          });
        },
        function deleteToken(csr, cb) {
          if(!csr) {
            return cb(null, false);
          }
          var query = 'DELETE FROM verification WHERE user_id = ?';
          connection.query(query, [userId], function (err, result) {
            if (err)
              return cb(err, null);
            console.log('dtr', result);
            return cb(null, result);
          });
        },
        function getNickname(dtr, cb) {
          if(!dtr) {
            return cb(null, false);
          }
          var query = 'SELECT nickname FROM user WHERE id = ?';
          connection.query(query, [userId], function (err, result) {
            if (err)
              return cb(err, null);
            console.log('gnr', result);
            return cb(null, result);
          })
        }
      ],
      function (err, result) {
        if (err) {
          connection.rollback();
          connection.release();
          return callback(err, null);
        }
        connection.commit(function (err) {
          if (err) {
            connection.rollback();
            connection.release();
            return callback(err, null);
          }
          connection.release();
          if(!result) {
            return callback(null, null);
          }
          return callback(null, result[0].nickname);
        });
      });
  });
}

function getUserIdByOAuth (oauth_id, oauth_account, callback){
  POOL.getConnection(function (err, connection){
    if (err) { return callback(err, null); }

    var stmt = 'SELECT '
      + 'u.id '
      + 'FROM oauth_connected AS oc, user AS u, user_state AS us '
      + 'WHERE oc.oauth_id = ? AND oc.oauth_account = ? AND '
      + 'oc.user_id = u.id AND oc.user_id = us.user_id';

    connection.query(stmt, [oauth_id, oauth_account],
      function (err, result){
        connection.release();
        if (err) { return callback(err, null); }

        return callback(null, result);
      });
  });
}