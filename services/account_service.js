var async = require('async');

module.exports.selectAccount = selectAccount; //계정정보 가져오기
module.exports.updateProfilePhoto = updateProfilePhoto; //프로필 이미지 수정
module.exports.updateEmail = updateEmail; //이메일 수정
module.exports.updateNickname = updateNickname; //닉네임 중복 체크
module.exports.updateUserURL = updateUserURL; //userURL 수정
module.exports.updateIntroduce = updateIntroduce; //자기소개 수정
module.exports.updateNotification = updateNotification; //알림 설정 수정
module.exports.updatePassword = updatePassword; //패스워드 수정
module.exports.insertDevice = insertDevice; //device 추가

function selectAccount (userId, callback){
  POOL.getConnection(function (err, connection) {
    if (err) return callback(err, null);

    var stmt = 'SELECT pp.medium_url AS profile_medium_url, u.email, '
      + 'u.nickname, u.user_url, u.introduce, n.on_all AS notification '
      + 'FROM user u '
      + 'INNER JOIN profile_photo pp ON u.id = pp.user_id '
      + 'INNER JOIN notification n ON u.id = n.user_id '
      + 'WHERE u.id = ?'
    connection.query(stmt, userId, function (err, account) {
      connection.release( );
      if (err)
        return callback(err, null);
      return callback(null, account[0]);
    });
  });//end of getConection
}

function updateProfilePhoto (userId, profilePhotos, callback){
  POOL.getConnection(function (err, connection) {
    if (err) return callback(err, null);

    var stmt = 'UPDATE profile_photo SET small_url = ?, medium_url = ?, large_url = ? WHERE user_id = ?';
    connection.query(stmt, [profilePhotos.small_url, profilePhotos.medium_url, profilePhotos.large_url, userId], function (err, result) {
      connection.release();
      if (err)
        return callback(err, null);
      return callback(null, null);
    });
  });//end of getConection
}

function updateEmail (userId, email, callback){
  POOL.getConnection(function (err, connection) {
    if (err) return callback(err, null);

    var stmt = 'UPDATE user SET email = ? WHERE id = ?';
    var sql = connection.query(stmt, [email, userId], function (err, result) {
      connection.release();
      if (err)
        return callback(err, null);
      return callback(null, null);
    });
  });//end of getConection
}

function updateNickname (userId, nickname, callback){
  POOL.getConnection(function (err, connection) {
    if (err) return callback(err, null);

    var stmt = 'UPDATE user SET nickname = ? WHERE id = ?';
    connection.query(stmt, [nickname, userId], function (err, result) {
      connection.release();
      if (err)
        return callback(err, null);
      return callback(null, null);
    });
  });//end of getConection
}

function updateUserURL (userId, userUrl, callback){
  POOL.getConnection(function (err, connection) {
    if (err) return callback(err, null);

    var stmt = 'UPDATE user SET user_url = ? WHERE id = ?';
    connection.query(stmt, [userUrl, userId], function (err, result) {
      connection.release();
      if (err)
        return callback(err, null);
      return callback(null, null);
    });
  });//end of getConection
}

function updateIntroduce (userId, introduce, callback){
  POOL.getConnection(function (err, connection) {
    if (err) return callback(err, null);

    var stmt = 'UPDATE user SET introduce = ? WHERE id = ?';
    connection.query(stmt, [introduce, userId], function (err, result) {
      connection.release();
      if (err)
        return callback(err, null);
      return callback(null, null);
    });
  });//end of getConection
}

function updateNotification (userId, callback){
  POOL.getConnection(function (err, connection) {
    if (err) return callback(err, null);

    var stmt = 'UPDATE notification SET on_all = !on_all WHERE user_id = ?';
    connection.query(stmt, userId, function (err, result) {
      connection.release();
      if (err)
        return callback(err, null);
      return callback(null, null);
    });
  });//end of getConection
}

function updatePassword (userId, presentPassword, inputPassword, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }

    connection.query('SELECT password FROM user WHERE id = ?', userId, function (err, pass) {
      console.log('presentPassword', presentPassword);
      console.log('pass', pass);
      console.log('inputPassword', inputPassword);
      console.log('userId', userId);

      if(pass[0].password === presentPassword) {
        connection.query('UPDATE user SET password = ? WHERE id = ?', [inputPassword, userId], function (err, result) {
          connection.release();
          if(err) {
            return callback(err, null);
          }
          else {
            return callback(null, true);
          }
        });
      }
      else {
        connection.release();
        return callback(null, false);

      }
    })
  });
}

function insertDevice (device, callback){
  POOL.getConnection(function (err, connection) {
    if (err) return callback(err, null);

    var stmt = 'INSERT INTO device SET ?';
    connection.query(stmt, device, function (err, result) {
      connection.release();
      if (err)
        return callback(err, null);
      return callback(null, null);
    });
  });//end of getConection
}