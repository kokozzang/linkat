var _ = require('underscore');

module.exports.duplicationChecker = duplicationChecker; // 중복검사

//알림 대상 리스트
module.exports.selectNotificationCountByUserIds = selectNotificationCountByUserIds;
module.exports.selectUserIdsForComment = selectUserIdsForComment; //댓글
module.exports.selectUserIdsForQuestionLike = selectUserIdsForQuestionLike; //질문 좋아요
module.exports.selectUserIdsForAnswer = selectUserIdsForAnswer; //답변
module.exports.selectUserIdsForAnswerLike = selectUserIdsForAnswerLike; //촵
module.exports.selectUserNickname = selectUserNickname;
module.exports.selectQuestionLikeCount = selectQuestionLikeCount;
module.exports.getUserInfoById = getUserInfoById;
module.exports.selectPushListByUserIds = selectPushListByUserIds;

/**
 * 이메일 중복검사
 * @param email 중복검사 할 이메일
 * @param callback(error, result)
 * error
 */
function duplicationChecker (table, column, value, callback) {
  POOL.getConnection(function (err, connection){
    if(err){
      console.error(err);
      return callback(err, null);
    }
    var stmt = 'SELECT COUNT(*) AS isUsing FROM ' + table + ' WHERE ' + column + '=?';
    connection.query(stmt, [value], function (err, result){
      connection.release();
      if(err){
        return callback(err, null);
      }
      return callback(null, result);
    });
  });
}


/**
 * 유저의 아이디들로 각 유저의 알림의 갯수를 가져옴
 * @param connection
 * @param userIds 유저아이디 배열 [{userId: 1}]
 * @param callback
 */
function selectNotificationCountByUserIds (connection, userIds, callback){
  if(connection === null)
    return callback(null, null);
  if(_.isEmpty(userIds))
    return callback(null, null);
  var arrUserIds = [];

  //유저아이디로만 이루어진 배열을 생성
  for(var i = 0; i < userIds.length; i++)
    arrUserIds.push(userIds[i].user_id);

  var stmt = 'SELECT user_id, count(*) AS cnt FROM '
    + 'notification WHERE user_id IN (?) AND viewed = 0 '
    + 'GROUP BY user_id';

  connection.query(stmt, [arrUserIds], function (err, userIdsAndCount){
    if (err)
      return callback(err, null);
    return callback(null, userIdsAndCount);
  });
}

//function selectNotificationCountByActivity (connection, activity, callback){
//  var stmt = 'SELECT user_id, count(*) AS cnt FROM notification '
//    + 'WHERE user_id in (SELECT user_id FROM notification WHERE activity = ?) '
//    + 'AND viewed = 0 '
//    + 'GROUP BY user_id';
//
//  var sql = connection.query(stmt, [activity], function (err, userIdsAndCount){
//    if (err)
//      return callback(err, null);
//    return callback(null, userIdsAndCount);
//  });
//}

/**
 * 댓글 작성시 알림을 받을 유저의 아이디 리스트를 리턴
 * 알림대상: 질문자, 댓글을 단 유저
 * 알림제외: 알림을 유발 한 유저, 댓글을 삭제한 유저
 * @param connnection 커넥션
 * @param questionId  알림이 발생한 질문의 아이디
 * @param userId      알림을 발생시킨 유저의 아이디
 * @param callback    알림 받을 유저의 아이디 리스트
 */
//function selectUserIdsForComment (connection, questionId, userId, callback){
//  //질문자, 댓글 단 유저
//  var stmt = 'SELECT user_id '
//    + 'FROM ('
//    + 'SELECT user_id FROM question WHERE id = ? UNION '
//    + 'SELECT DISTINCT user_id FROM question_comment WHERE question_id = ? AND deleted_date IS NULL'
//    + ') AS u '
//    + 'WHERE u.user_id <> ?';
//
//  connection.query(stmt, [questionId, questionId, userId], function (err, userIds){
//    if (err)
//      return callback(err, null);
//    return callback(null, userIds);
//  });
//}

function selectUserIdsForComment (connection, questionId, userId, callback){
  //질문자, 댓글 단 유저
  var stmt = 'SELECT u.user_id, nt.on_all '
    + 'FROM ('
    + 'SELECT user_id FROM question WHERE id = ? UNION '
    + 'SELECT DISTINCT user_id FROM question_comment WHERE question_id = ? AND deleted_date IS NULL '
    + ') AS u INNER JOIN notification nt ON u.user_id = nt.user_id '
    + 'WHERE u.user_id <> ?';

  connection.query(stmt, [questionId, questionId, userId], function (err, userIds){
    if (err){
      return callback(err, null);
    }
    return callback(null, userIds);
  });
}

/**
 * 좋아요 발생 알림을 받을 유저의 아이디 리스트를 리턴
 * 알림대상: 질문자, 질문 좋아요 누른 유저
 * 알림제외: 알림을 유발 한 유저
 * @param connnection 커넥션
 * @param questionId  알림이 발생한 질문의 아이디
 * @param userId      알림을 발생시킨 유저의 아이디
 * @param callback    알림 받을 유저의 아이디 리스트
 */
function selectUserIdsForQuestionLike (connection, questionId, userId, callback){
  //질문자, 댓글 단 유저
  var stmt = 'SELECT user_id '
    + 'FROM ('
    + 'SELECT user_id FROM question WHERE id = ? UNION '
    + 'SELECT DISTINCT user_id FROM question_like WHERE question_id = ?'
    + ') AS u '
    + 'WHERE u.user_id <> ?';

  connection.query(stmt, [questionId, questionId, userId], function (err, userIds){
    if (err)
      return callback(err, null);
    return callback(null, userIds);
  });
}

/**
 * 답변 알림을 받을 유저의 아이디 리스트
 * 질문자, 답변자들, 댓글단유저, 질문 좋아요
 * @param connnection 커넥션
 * @param questionId  알림이 발생한 질문의 아이디
 * @param userId      알림을 발생시킨 유저의 아이디
 * @param callback    알림 받을 유저의 아이디 리스트
 */
function selectUserIdsForAnswer (connection, questionId, userId, callback){
  var stmt = 'SELECT u.user_id, nt.on_all '
    + 'FROM ('
    + 'SELECT user_id FROM question WHERE id = ? '
    + 'UNION SELECT user_id FROM question_like WHERE question_id = ? '
    + 'UNION SELECT user_id FROM answer WHERE question_id = ? '
    + 'UNION SELECT DISTINCT user_id FROM question_comment WHERE question_id = ?'
    + ') AS u INNER JOIN notification nt ON u.user_id = nt.user_id '
    + 'WHERE u.user_id <> ?';

  var queryValues = [questionId, questionId, questionId, questionId, userId];
  var sql = connection.query(stmt, queryValues, function (err, userIds){
    if (err)
      return callback(err, null);
    console.log('sql.sql', sql.sql);
    console.log('userIds', userIds);

    return callback(null, userIds);
  });
}


/**
 * 알림을 받을 유저의 알림 리스트를 리턴
 * @param connection  커넥션
 * @param questionId  알림이 발생한 질문의 아이디
 * @param userId      알림을 발생시킨 유저의 아이디
 * @param callback    알림 받을 유저의 아이디 리스트
 */
function selectUserIdsForAnswerLike (connection, answerId, userId, callback){
  //답변자
  var stmt = 'SELECT user_id FROM answer WHERE id = ? AND user_id <> ?';

  connection.query(stmt, [answerId, userId], function (err, userIds){
    if (err)
      return callback(err, null);
    return callback(null, userIds);
  });
}

/**
 * 해당 userId의 닉네임을 가져옴
 * @param connection  커넥션
 * @param userId      유저아이디
 */
function selectUserNickname (connection, userId, callback){
  var stmt = 'SELECT nickname FROM user WHERE id = ?';
  connection.query(stmt, [userId], function (err, result){
    if (err)
      return callback(err, null);
    return callback(null, result);
  });
}

/**
 * 해당 질문의 좋아요 갯수를 가져옴
 * @param connection  커넥션
 * @param questionId  질문 아이디
 */
function selectQuestionLikeCount (connection, questionId, callback){
  var stmt = 'SELECT count(*) AS cnt FROM question_like WHERE question_id = ?';
  connection.query(stmt, [questionId], function (err, result){
    if (err)
      return callback(err, null);
    return callback(null, result[0].cnt);
  });
}


/**
 * 회원정보
 * 유저아이디로 회원정보를 가져옴
 * @param id 유저 아이디
 * @param callback
 */
function getUserInfoById(connection, id, callback){
  var stmt = 'SELECT '
    + 'u.id as id, u.user_url AS user_url, pp.small_url AS profile_small_url, pp.medium_url AS profile_medium_url, '
    + 'u.nickname AS nickname, us.state_id as state_id, us.change_date as change_date, '
    + 'us.end_date as end_date, u.gender_id AS gender_id '
    + 'FROM user u, user_state us, profile_photo pp '
    + 'WHERE u.id = ? AND u.id = us.user_id AND us.end_date IS NULL AND u.id = pp.user_id';

  var sql = connection.query(stmt, [id], function (err, result){
    console.log('getUserInfoById', sql.sql);
    if (err) { return callback(err, null); }
    return callback(null, result);
  });
}


function selectPushListByUserIds (userIds, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null);}
    var stmt = 'SELECT user_id, regid FROM device WHERE user_id IN (?)';
    var sql = connection.query(stmt, userIds, function (err, result) {
      connection.release();
      if(err) {console.log('sql.sql', sql.sql);return callback(err, null);}
      return callback(null, result);
    })
  });
}