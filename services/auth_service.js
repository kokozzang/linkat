var async = require('async');
var serviceUtil = require('./service_util');

module.exports.getUserInfoByEmailAndPassword = getUserInfoByEmailAndPassword; // 회원정보
module.exports.getUserInfoByOAuth = getUserInfoByOAuth; // 회원정보
module.exports.registerUser = registerUser; // 회원추가
module.exports.getSalt = getSalt;

/**
 * 회원정보
 * 이메일과 패스워드로 회원정보를 가져옴
 * @param email     이메일
 * @param password  패스워드
 * @param callback
 */
function getUserInfoByEmailAndPassword(email, password, callback){
  POOL.getConnection(function (err, connection){
    if (err) { return callback(err, null); }

    var stmt = 'SELECT '
      + 'u.id AS id, u.nickname AS nickname, us.state_id AS state_id, us.change_date AS change_date, '
      + 'us.end_date AS end_date '
      + 'FROM user AS u, user_state AS us '
      + 'WHERE u.email=? AND u.password=? AND us.end_date IS NULL AND u.id=us.user_id';

    connection.query(stmt, [email, password],
    function (err, result){
      connection.release();
      if (err) { return callback(err, null); }

      return callback(null, result);
    });
  });
}

/**
 * 회원정보
 * OAuth로 회원정보를 가져옴
 * @param oauth_id     이메일
 * @param oauth_account  패스워드
 * @param callback
 */
function getUserInfoByOAuth(oauth_id, oauth_account, callback){
  POOL.getConnection(function (err, connection){
    if (err) { return callback(err, null); }

    var stmt = 'SELECT '
      + 'u.id as id, u.user_url AS user_url, pp.small_url AS profile_small_url, pp.medium_url AS profile_medium_url, '
      + 'u.nickname AS nickname, us.state_id as state_id, us.change_date as change_date, '
      + 'us.end_date as end_date, u.gender_id AS gender_id '
      + 'FROM oauth_connected oc, user u, user_state us, profile_photo pp '
      + 'WHERE oc.oauth_id = ? AND oc.oauth_account = ? '
      + 'AND oc.user_id = u.id AND oc.user_id = us.user_id '
      + 'AND us.end_date IS NULL AND u.id = pp.user_id';

    connection.query(stmt, [oauth_id, oauth_account], function (err, result){
      connection.release();
      if (err) { return callback(err, null); }
      return callback(null, result);
    });
  });
}

/**
 * 회원추가
 * 회원을 추가하고 key를 리턴
 * @param user  회원정보
 * @param callback  콜백
 */

function registerUser (user, profilePhoto, oauth, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }
    async.waterfall([
      function transactionStart(cb){
        connection.beginTransaction(function (err){
          if(err)
            return cb(err, null);
          return cb(null, null);
        });
      },
      function insertUser (result, cb) {//user 테이블에 사용자를 추가함
        connection.query('INSERT INTO user SET ?', user, function (err, result){
          if(err)
            return cb(err, null);
          return cb(null, result.insertId, user.join_date);
        });
      },
      function insertUserState (user_id, join_date, cb) {//state 테이블에 상태를 미승인 상태로 추가함
        var user_state = {
          user_id: user_id,             //추가된 사용자 키
          state_id: ENUMS.USER_STATE.PENDING,  //미승인 상태
          change_date: join_date,      //가입 일시
          end_date: null             //상태 종료 일시
        };
        connection.query('INSERT INTO user_state SET ?', user_state, function (err, result){
          if(err)
            return cb(err, null);
          return cb(null, user_id);
        });
      },
      function inserNotification (user_id, cb) {//notifcation 설정
        var notification = {
          user_id: user_id,
          on_all: true
        };
        connection.query('INSERT INTO notification SET ?', notification, function (err, result){
          if(err)
            return cb(err, null);
          return cb(null, user_id);
        });
      },
      function inserProfilePhoto (user_id, cb) {//프로필 이미지
        profilePhoto.user_id = user_id;
        connection.query('INSERT INTO profile_photo SET ?', profilePhoto, function (err, result){
          if(err)
            return cb(err, null);
          return cb(null, user_id);
        });
      },
      function insertOAuthConnected (user_id, cb) {
        if(oauth === ENUMS.OAUTH.LOCAL){ return cb(null, null); }
        else{
          var oauth_conneted = {
            user_id: user_id,             //추가된 사용자 키
            oauth_id: oauth.oauth_id,  //미승인 상태
            oauth_account: oauth.oauth_account //가입 일시
          };
          connection.query('INSERT INTO oauth_connected SET ?', oauth_conneted, function (err, result){
            if(err)
              return cb(err, null);
            return cb(null, user_id);
          });
        }
      }
    ],
    function (err, result){
      if(err){
        connection.rollback();
        connection.release();
        return callback(err, null);
      }
      connection.commit(function(err) {
        if (err) {
          connection.rollback();
          connection.release();
          return callback(err, null);
        }
        serviceUtil.getUserInfoById(connection, result, function(err, userInfo){
          connection.release();
          if(err)
            return callback(err, null);
          return callback(null, userInfo[0]);
        });//end of getUserInfoById
      });//end of commit
    });//end of waterfall
  });//end of getConnecton
}

/**
 * salt값을 가지고 온다.
 * @param email salt를 가지고 올 email 주소
 * @param callback  콜백
 */
function getSalt (email, callback) {
  POOL.getConnection(function (err, connection) {
    var stmt = 'SELECT salt FROM user WHERE email = ?';
    connection.query(stmt, [email], function (err, result) {
      connection.release();
      if (err) {return callback(err, null); }
      return callback(null, result[0]);
    })
  })

}
