var async = require('async');
var _ = require('underscore');

module.exports.index = index;

module.exports.selectLikedQuestions = selectLikedQuestions;
module.exports.selectQuestions = selectQuestions;
module.exports.selectLikedAnswers =  selectLikedAnswers;
module.exports.selectAnswers = selectAnswers;

module.exports.insertFollow = insertFollow;
module.exports.deleteFollow = deleteFollow;

module.exports.selectFollowers = selectFollowers;
module.exports.selectFollowings = selectFollowings;

module.exports.modifyPage = modifyPage;
module.exports.postModify = postModify;



function index (userURL, userId, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }

    var stmt = 'SELECT u.id, u.nickname, u.user_url, pp.medium_url AS profile_medium_url, pp.large_url AS profile_large_url, '
      + 'u.introduce, flrc.follower_count, flgc.following_count, fld.followed, '
      + 'qlc.qlikes_count, alc.alikes_count, qc.question_count, ac.answer_count '
    + 'FROM user AS u '
      + 'INNER JOIN profile_photo pp ON u.id = pp.user_id '
      + 'INNER JOIN '
        + '(SELECT u.id AS user_id, count(*) AS follower_count '
        + 'FROM user AS u '
        + 'INNER JOIN follower flr ON u.id = flr.user_id '
        + 'WHERE u.user_url = ?) AS flrc ON u.id = flrc.user_id '
      + 'INNER JOIN '
        + '(SELECT u.id AS user_id, count(*) AS following_count '
        + 'FROM user u '
        + 'INNER JOIN following flg ON u.id = flg.user_id '
        + 'WHERE u.user_url = ?) AS flgc ON u.id = flgc.user_id '
      + 'INNER JOIN '
        + '(SELECT u.id AS user_id, count(*) AS qlikes_count '
        + 'FROM user u '
        + 'INNER JOIN question_like ql ON ql.user_id = u.id '
        + 'WHERE u.user_url = ?) AS qlc ON u.id = qlc.user_id '
      + 'INNER JOIN '
        + '(SELECT u.id AS user_id, count(*) AS alikes_count '
        + 'FROM user u '
        + 'INNER JOIN answer_like al ON al.user_id = u.id '
        + 'WHERE u.user_url = ?) AS alc ON u.id = alc.user_id '
      + 'INNER JOIN '
      + '(SELECT u.id AS user_id, count(*) AS question_count '
        + 'FROM user u '
        + 'INNER JOIN question q ON q.user_id = u.id '
        + 'WHERE u.user_url = ?) AS qc ON u.id = qc.user_id '
      + 'INNER JOIN '
        + '(SELECT u.id AS user_id, count(*) AS answer_count '
        + 'FROM user u '
        + 'INNER JOIN answer a ON a.user_id = u.id '
        + 'WHERE u.user_url = ?) AS ac ON u.id = ac.user_id '
      + 'LEFT OUTER JOIN '
        +'(SELECT user_id, count(*) AS followed FROM follower WHERE follower_user_id = ? GROUP BY user_id) AS fld '
        + 'ON u.id = fld.user_id '
      + 'WHERE u.user_url = ?';

    connection.query(stmt, [userURL, userURL, userURL, userURL, userURL, userURL, userId, userURL], function (err, user) {
      connection.release();
      if (err) {
        return callback(err, null);
      }
      return callback(null, user[0]);
    });
  });
}


function selectLikedQuestions (userUrl, userId, startId, page, limit, callback){
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }

    var arr = [userUrl, userId, ENUMS.QUESTION_PHOTO_WIDTH.MEDIUM, (page-1) * limit, limit];

    var stmt = 'SELECT q.id, q.user_id, q.content, u.nickname, u.user_url, c.name AS category_name, '
      + 'qp.url AS question_image_url, qp.width AS question_image_width, qp.height AS question_image_height, '
      + 'pp.small_url AS profile_small_url, ac.answer_count, qlc.like_count, qld.liked '
      + 'FROM question_like ql '
      + 'INNER JOIN user qlu ON ql.user_id = qlu.id AND qlu.user_url = ? '
      + 'INNER JOIN question q ON ql.question_id = q.id '
      + 'INNER JOIN user u ON q.user_id = u.id '
      + 'INNER JOIN profile_photo pp ON q.user_id = pp.user_id '
      + 'INNER JOIN question_photo qp ON q.id = qp.question_id '
      + 'INNER JOIN category c ON q.category_id = c.id '
      + 'LEFT OUTER JOIN (SELECT question_id, count(*) AS answer_count FROM answer WHERE deleted_date IS NULL GROUP BY question_id) AS ac '
      + 'ON q.id = ac.question_id '
      + 'LEFT OUTER JOIN (SELECT question_id, count(*) AS like_count FROM question_like GROUP BY question_id) AS qlc '
      + 'ON q.id = qlc.question_id '
      + 'LEFT OUTER JOIN (SELECT question_id, count(*) AS liked FROM question_like WHERE user_id = ? GROUP BY question_id) AS qld '
      + 'ON q.id = qld.question_id '
      + 'WHERE ';
    if(startId !== null) {
      stmt += 'q.id < ? AND ';
      arr = [userUrl, userId, startId + 1, ENUMS.QUESTION_PHOTO_WIDTH.MEDIUM, (page-1) * limit, limit];
    }
    stmt += 'q.deleted_date IS NULL AND qp.width = ? ORDER BY q.id DESC LIMIT ?, ?';

    connection.query(stmt, arr, function (err, result){
      connection.release();
      if(err)
        return callback(err, null);
      return callback(null, result);
    });
  });
}

function selectLikedAnswers (userUrl, userId, startId, page, limit, callback){
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }
    var arr = [userUrl, userId, ENUMS.ANSWER_PHOTO_WIDTH.MEDIUM, (page-1) * limit, limit];

    var stmt = 'SELECT a.id, a.question_id, a.user_id, a.price, a.link, a.content, ald.liked, alc.like_count, '
      + 'c.name AS category_name, u.nickname, u.user_url, pp.small_url AS profile_small_url, g.name AS grade_name, '
      + 'ap.url AS answer_image_url, ap.width AS answer_image_width, ap.height AS answer_image_height '
      + 'FROM answer_like AS al '
      + 'INNER JOIN user AS alu ON al.user_id = alu.id AND alu.user_url = ? '
      + 'INNER JOIN answer AS a ON al.answer_id = a.id '
      + 'INNER JOIN user u ON a.user_id = u.id '
      + 'INNER JOIN profile_photo AS pp ON a.user_id = pp.user_id '
      + 'INNER JOIN answer_photo AS ap ON a.id = ap.answer_id '
      + 'INNER JOIN question AS q ON q.id = a.question_id '
      + 'INNER JOIN category AS c ON q.category_id = c.id '
      + 'INNER JOIN grade AS g ON u.grade_id = g.id '
      + 'LEFT OUTER JOIN (SELECT answer_id, count(*) AS liked FROM answer_like WHERE user_id = ? GROUP BY answer_id) AS ald '
      + 'ON a.id = ald.answer_id '
      + 'LEFT OUTER JOIN (SELECT answer_id, count(*) AS like_count FROM answer_like GROUP BY answer_id) AS alc '
      + 'ON a.id = alc.answer_id '
      + 'WHERE ';
    if(startId !== null) {
      stmt += 'a.id < ? AND ';
      arr = [userUrl, userId, startId + 1, ENUMS.ANSWER_PHOTO_WIDTH.MEDIUM, (page-1) * limit, limit];
    }
    stmt += 'a.deleted_date IS NULL AND ap.width = ? ORDER BY a.id DESC LIMIT ?, ?';

    connection.query(stmt, arr, function (err, result){
      connection.release();
      if(err)
        return callback(err, null);
      return callback(null, result);
    });
  });
}

function selectQuestions (userUrl, userId, startId, page, limit, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }
    var arr = [userId, userUrl, ENUMS.QUESTION_PHOTO_WIDTH.MEDIUM, (page-1) * limit, limit];

    var stmt = 'SELECT q.id, q.user_id, q.content, u.nickname, u.user_url, c.name AS category_name, '
      + 'qp.url AS question_image_url, qp.width AS question_image_width, qp.height AS question_image_height, '
      + 'pp.small_url AS profile_small_url, ac.answer_count, qlc.like_count, ql.liked '
      + 'FROM question q '
      + 'INNER JOIN user u ON q.user_id = u.id '
      + 'INNER JOIN profile_photo pp ON q.user_id = pp.user_id '
      + 'INNER JOIN question_photo qp ON q.id = qp.question_id '
      + 'INNER JOIN category c ON q.category_id = c.id '
      + 'LEFT OUTER JOIN (SELECT question_id, count(*) AS answer_count FROM answer WHERE deleted_date IS NULL GROUP BY question_id) AS ac '
      + 'ON q.id = ac.question_id '
      + 'LEFT OUTER JOIN (SELECT question_id, count(*) AS like_count FROM question_like GROUP BY question_id) AS qlc '
      + 'ON q.id = qlc.question_id '
      + 'LEFT OUTER JOIN (SELECT question_id, count(*) AS liked FROM question_like WHERE user_id = ? GROUP BY question_id) AS ql '
      + 'ON q.id = ql.question_id '
      + 'WHERE ';
    if(startId !== null) {
      stmt += 'q.id < ? AND ';
      arr = [userId, startId + 1, userUrl, ENUMS.QUESTION_PHOTO_WIDTH.MEDIUM, (page-1) * limit, limit];
    }
    stmt += 'u.user_url = ? AND q.deleted_date IS NULL AND qp.width = ? ORDER BY q.id DESC LIMIT ?, ?';

    connection.query(stmt, arr, function (err, result){
      connection.release();
      if(err)
        return callback(err, null);
      return callback(null, result);
    });
  });
}

function selectAnswers (userUrl, userId, startId, page, limit, callback){
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }
    var arr = [userId, userUrl, ENUMS.ANSWER_PHOTO_WIDTH.MEDIUM, (page-1) * limit, limit];

    var stmt = 'SELECT a.id, a.question_id, a.user_id, a.price, a.link, a.content, ald.liked, alc.like_count, '
      + 'c.name AS category_name, u.nickname, u.user_url, pp.small_url AS profile_small_url, g.name AS grade_name, '
      + 'ap.url AS answer_image_url, ap.width AS answer_image_width, ap.height AS answer_image_height '
      + 'FROM answer AS a '
      + 'INNER JOIN user AS u ON a.user_id = u.id '
      + 'INNER JOIN profile_photo AS pp ON a.user_id = pp.user_id '
      + 'INNER JOIN answer_photo AS ap ON a.id = ap.answer_id '
      + 'INNER JOIN question AS q ON q.id = a.question_id '
      + 'INNER JOIN category AS c ON q.category_id = c.id '
      + 'INNER JOIN grade AS g ON u.grade_id = g.id '
      + 'LEFT OUTER JOIN (SELECT answer_id, count(*) AS liked FROM answer_like WHERE user_id = ? GROUP BY answer_id) AS ald '
      + 'ON a.id = ald.answer_id '
      + 'LEFT OUTER JOIN (SELECT answer_id, count(*) AS like_count FROM answer_like GROUP BY answer_id) AS alc '
      + 'ON a.id = alc.answer_id '
      + 'WHERE ';
    if(startId !== null) {
      stmt += 'a.id < ? AND ';
      arr = [userId, startId + 1, userUrl, ENUMS.ANSWER_PHOTO_WIDTH.MEDIUM, (page-1) * limit, limit];
    }
    stmt += 'u.user_url = ? AND a.deleted_date IS NULL AND ap.width = ? ORDER BY a.id DESC LIMIT ?, ?';

    connection.query(stmt, arr, function (err, result){
      connection.release();
      if(err)
        return callback(err, null);
      return callback(null, result);
    });
  });
}

function insertFollow (targetId, userId, callback){
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }

    connection.beginTransaction(function (err){
      if(err) return callback(err, null);

      async.parallel([
        //follower테이블에 입력
        function insertFollower (cb) {
          var stmt = 'INSERT INTO follower SET ?';
          var follower = {
            user_id: targetId,
            follower_user_id: userId
          };
          connection.query(stmt, follower, function (err, result) {
            if(err)
              return cb(err, null);
            return cb(null, null);
          });
        },
        //following 테이블에 입력
        function insertFollowing (cb) {
          var stmt = 'INSERT INTO following SET ?';
          var following = {
            user_id: userId,
            following_user_id: targetId
          };
          connection.query(stmt, following, function (err, result) {
            if(err)
              return cb(err, null);
            return cb(null, null);
          });
        }
      ], function (err, results){
        if(err){
          connection.rollback();
          connection.release();
          return callback(err, null);
        }
        connection.commit(function(err) {
          if (err) {
            connection.rollback();
            connection.release();
            return callback(err, null);
          }
          connection.release();
          return callback(null, null);
        });
      });//end of parallel

    });//end of transction

  });//end of getConection
}

function deleteFollow (targetId, userId, callback){
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }

    connection.beginTransaction(function (err){
      if(err) return callback(err, null);

      async.parallel([
        //follower테이블에서 삭제
        function deleteFollower (cb) {
          var stmt = 'DELETE FROM follower WHERE user_id = ? AND follower_user_id = ?';

          connection.query(stmt, [targetId, userId], function (err, result) {
            if(err)
              return cb(err, null);
            return cb(null, null);
          });
        },
        //following 테이블에서 삭제
        function deleteFollowing (cb) {
          var stmt = 'DELETE FROM following WHERE user_id = ? AND following_user_id = ?';

          connection.query(stmt, [userId, targetId], function (err, result) {
            if(err)
              return cb(err, null);
            return cb(null, null);
          });
        }
      ], function (err, results){
        if(err){
          connection.rollback();
          connection.release();
          return callback(err, null);
        }
        connection.commit(function(err) {
          if (err) {
            connection.rollback();
            connection.release();
            return callback(err, null);
          }
          connection.release();
          return callback(null, null);
        });
      });//end of parallel

    });//end of transction

  });//end of getConection
}

function selectFollowers (userUrl, userId, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }


    //var stmt = 'SELECT u.id, u.user_url, u2.id, u2.user_url, u2.nickname, u2.introduce, pp.small_url AS profile_small_url, flwd.followed, if(u2.id = ?, true, false) AS isMine '
    var stmt = 'SELECT u2.id, u2.user_url, u2.nickname, u2.introduce, pp.small_url AS profile_small_url, flwd.followed, if(u2.id = ?, true, false) AS isMine '
      + 'FROM follower flr '
      + 'INNER JOIN user u ON flr.user_id = u.id '
      + 'LEFT OUTER JOIN user u2 ON flr.follower_user_id = u2.id '
      + 'INNER JOIN profile_photo pp ON flr.follower_user_id = pp.user_id '
      + 'LEFT OUTER JOIN '
        + '(SELECT user_id, count(*) AS followed FROM follower WHERE follower_user_id = ? GROUP BY user_id) AS flwd '
        + 'ON flr.follower_user_id = flwd.user_id '
      + 'WHERE u.user_url = ?';

    connection.query(stmt, [userId, userId, userUrl], function (err, followers) {
      connection.release();
      if(err) {return callback(err, null); }
      return callback(null, followers);
    });
  });
}

function selectFollowings (userUrl, userId, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }

    //var stmt = 'SELECT u.id, u.user_url, u2.id, u2.user_url, u2.nickname, u2.introduce, pp.small_url AS profile_small_url, flwd.followed, if(u2.id = ?, true, false) AS isMine '
    var stmt = 'SELECT u2.id, u2.user_url, u2.nickname, u2.introduce, pp.small_url AS profile_small_url, flwd.followed, if(u2.id = ?, true, false) AS isMine '
      + 'FROM following flg '
      + 'INNER JOIN user u ON flg.user_id = u.id '
      + 'LEFT OUTER JOIN user u2 ON flg.following_user_id = u2.id '
      + 'INNER JOIN profile_photo pp ON flg.following_user_id = pp.user_id '
      + 'LEFT OUTER JOIN '
        + '(SELECT user_id, count(*) AS followed FROM following WHERE following_user_id = ? GROUP BY user_id) AS flwd '
        + 'ON flg.following_user_id = flwd.user_id '
      + 'WHERE u.user_url = ?';

    connection.query(stmt, [userId, userId, userUrl], function (err, followings) {
      connection.release();
      if(err) {return callback(err, null); }
      return callback(null, followings);
    });
  });
}

function modifyPage (userId, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }

    connection.query('SELECT email, nickname, user_url FROM user WHERE id = ?', [userId],function (err, result) {
      connection.release();
      if(err) {
        return callback(err, null);
      }
      else {
        return callback(null, result[0]);
      }
    });
  });
}
function postModify (userId, data, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) {
      console.log('err', err);
    }
    connection.query('UPDATE user set ? WHERE id = ?', [data, userId], function (err, result) {
      connection.release();
      if(err) {
        return callback(err, null);
      }
      else {
        return callback(null, result);
      }
    });
  });
}