var async = require('async');

exports.follow = function (followingId, followedId, callback) {
  POOL.getConnection(function (err, connection) {
    async.series([
        function (firstCallback) {
          connection.query('INSERT INTO follower (user_id, follower_user_id) VALUES (?, ?)', [followingId, followedId], function (err, followerResult) {
            if (err) {
              console.log('err', err);
              connection.rollback();
            }
            else {
              return firstCallback(null, followerResult);
            }
          })
        },
        function (secondCallback) {
          connection.query('INSERT INTO following (user_id, following_user_id) VALUES (?, ?)', [followedId, followingId], function (err, followingResult) {
            if (err) {
              console.log('err', err);
              connection.rollback();
            }
            else {
              return secondCallback(null, followingResult);
            }
          });
        }
      ],
      function (err, result) {
        connection.release();
        if (err) {
          console.log('err', err);
          return callback(err, false);
        }
        else {
          return callback(null, true);
        }
      })
  });
};

exports.unfollow = function (followingId, followedId, callback) {
  POOL.getConnection(function (err, connection) {
    async.series([
      function (firstCallback) {
        connection.query('DELETE FROM follower WHERE user_id = ? AND follower_user_id', [followingId, followedId], function (err, followerResult) {
          if (err) {
            console.log('err', err);
            connection.rollback();
          }
          else {
            return firstCallback(null, followerResult);
          }
        });
      },
      function (secondCallback) {
        console.log('followedId', followedId);
        console.log('followingId', followingId);
        connection.query('DELETE FROM following WHERE user_id = ? AND following_user_id = ?', [followedId, followingId], function (err, followingResult) {
          console.log('followingResult', followingResult);
          if (err) {
            console.log('err', err);
            connection.rollback();
          }
          else {
            return secondCallback(null, followingResult);
          }
        });
      }
    ],
    function (err, reuslt) {
      connection.release();
      if(err) {
        console.log('err', err);
        return callback(err, false);
      }
      else {
        return callback(null, true);
      }
    });
  });
}