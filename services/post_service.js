var async = require('async');
var _ = require('underscore');
var serviceUtil = require('./service_util');
var Notification = require('../db').models.Notification;  //mongodb 알림 모델


module.exports.newPosts = newPosts; //뉴
//module.exports.hotPosts = hotPosts; //hot
module.exports.categorizedPosts = categorizedPosts; //카테고리별

module.exports.insertComment = insertComment;
module.exports.selectComments = selectComments;
module.exports.deleteComment = deleteComment;

module.exports.insertQuestionLike = insertQuestionLike;
module.exports.deleteQuestionLike = deleteQuestionLike;

module.exports.insertAnswerLike = insertAnswerLike;
module.exports.deleteAnswerLike = deleteAnswerLike;


//질문 서비스 CRUD
module.exports.insertQuestion = insertQuestion; //질문 삽입
module.exports.selectQuestion = selectQuestion; //질문 조회
module.exports.updateQuestion = updateQuestion; //질문 수정
module.exports.deleteQuestion = deleteQuestion; //질문 삭제

//답번 서비스 CUD
module.exports.insertAnswer = insertAnswer; //답변 삽입
module.exports.deleteAnswer = deleteAnswer; //답변 삭제


function newPosts (userId, startId, page, limit, callback) {
  POOL.getConnection (function (err, connection) {
    if (err) { return callback(err, null); }
    var arr = [userId, ENUMS.QUESTION_PHOTO_WIDTH.MEDIUM, (page-1) * limit, limit];

    var stmt = 'SELECT q.id, q.user_id, q.content, u.nickname, u.user_url, c.name AS category_name, '
      + 'g.id AS gender_id, c.id AS category_id, '
      + 'qp.url AS question_image_url, qp.width AS question_image_width, qp.height AS question_image_height, '
      + 'pp.small_url AS profile_small_url, ac.answer_count, qlc.like_count, ql.liked '
      + 'FROM question q '
      + 'INNER JOIN user u ON q.user_id = u.id '
      + 'INNER JOIN gender AS g ON q.gender_id = g.id '
      + 'INNER JOIN category AS c ON q.category_id = c.id '
      + 'INNER JOIN profile_photo pp ON q.user_id = pp.user_id '
      + 'INNER JOIN question_photo qp ON q.id = qp.question_id '
      + 'LEFT OUTER JOIN (SELECT question_id, count(*) AS answer_count FROM answer WHERE deleted_date IS NULL GROUP BY question_id) AS ac '
      + 'ON q.id = ac.question_id '
      + 'LEFT OUTER JOIN (SELECT question_id, count(*) AS like_count FROM question_like GROUP BY question_id) AS qlc '
      + 'ON q.id = qlc.question_id '
      + 'LEFT OUTER JOIN (SELECT question_id, count(*) AS liked FROM question_like WHERE user_id = ? GROUP BY question_id) AS ql '
      + 'ON q.id = ql.question_id '
      + 'WHERE ';
    if(startId !== null) {
      stmt += 'q.id < ? AND ';
      arr = [userId, startId + 1, ENUMS.QUESTION_PHOTO_WIDTH.MEDIUM, (page-1) * limit, limit];
    }
    stmt += 'q.deleted_date IS NULL AND qp.width = ? ORDER BY q.id DESC LIMIT ?, ?';

    connection.query(stmt, arr, function (err, result){
      connection.release();
      if(err)
        return callback(err, null);
      return callback(null, result);
    });
  });
}

function categorizedPosts (userId, category, startId, page, limit, callback) {
  POOL.getConnection (function (err, connection) {
    if (err) { return callback(err, null); }
    var arr = [userId, category, ENUMS.QUESTION_PHOTO_WIDTH.MEDIUM, (page-1)*limit, limit];

    var stmt = 'SELECT q.id, q.user_id, q.content, u.nickname, u.user_url, c.name AS category_name, '
      + 'g.id AS gender_id, c.id AS category_id, '
      + 'qp.url AS question_image_url, qp.width AS question_image_width, qp.height AS question_image_height, '
      + 'pp.small_url AS profile_small_url, ac.answer_count, qlc.like_count, ql.liked '
      + 'FROM question q '
      + 'INNER JOIN user u ON q.user_id = u.id '
      + 'INNER JOIN gender AS g ON q.gender_id = g.id '
      + 'INNER JOIN category AS c ON q.category_id = c.id '
      + 'INNER JOIN profile_photo pp ON q.user_id = pp.user_id '
      + 'INNER JOIN question_photo qp ON q.id = qp.question_id '
      + 'LEFT OUTER JOIN (SELECT question_id, count(*) AS answer_count FROM answer WHERE deleted_date IS NULL GROUP BY question_id) AS ac '
      + 'ON q.id = ac.question_id '
      + 'LEFT OUTER JOIN (SELECT question_id, count(*) AS like_count FROM question_like GROUP BY question_id) AS qlc '
      + 'ON q.id = qlc.question_id '
      + 'LEFT OUTER JOIN (SELECT question_id, count(*) AS liked FROM question_like WHERE user_id = ? GROUP BY question_id) AS ql '
      + 'ON q.id = ql.question_id '
      + 'WHERE '
    if(startId !== null) {
      stmt += 'q.id < ? AND ';
      arr = [userId, startId + 1, category,  ENUMS.QUESTION_PHOTO_WIDTH.MEDIUM, (page-1) * limit, limit];
    }
    stmt += 'c.id = ? AND q.deleted_date IS NULL AND qp.width = ? ORDER BY q.id DESC LIMIT ?, ?';

    connection.query(stmt, arr, function (err, result){
      connection.release();
      if(err)
        return callback(err, null);
      return callback(null, result);
    });
  });
}

function insertComment (comment, ownerId, sender, callback) {
  var activityId = null;
  var notifications = [];

  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }

    async.waterfall([
      function transactionStart(cb){
        connection.beginTransaction(function (err){
          if(err)
            return cb(err, null);
          return cb(null, null);
        });
      },
      //댓글을 디비에 insert
      function (result, cb) {
        var stmt = 'INSERT INTO question_comment SET ?';
        connection.query(stmt, comment, function (err, result){
          if(err){
            return cb(err, null);
          }
          activityId = result.insertId; //댓글 아이디
          return cb(null, connection, comment.question_id, comment.user_id);
        });
      },
      //알림 받을 유저 리스트
      //이벤트를 발생시킨 유저는 제외
      serviceUtil.selectUserIdsForComment,
      //알림 테이블에 알림 내용 삽입
      function (userIds, cb) {
        if(_.isEmpty(userIds))  //알림 대상이 없는 경우 (첫 댓글이 자플)
          return cb(null, null);

        var stmt = 'SELECT u.id, u.nickname, u.user_url ' +
          'FROM user u ' +
          'INNER JOIN question q ON u.id = q.user_id ' +
          'WHERE q.id = ?';
        connection.query(stmt, comment.question_id, function (err, user) {
          if (err) {
            return cb(err, null);
          }

          for(var i = 0; i < userIds.length; i++) {
            var notification = {
              qid: comment.question_id,  //질문 아이디
              ruid: userIds[i].user_id, //수신자 유저 아이디
              suid: sender.userId, //발송자 유저 아이디
              spsu: sender.profile, //발송자 프로필 이미지 URL
              snik: sender.nickname, //발송자 닉네임
              surl: sender.user_url, //소유자 유저 유얄엘
              cdate: comment.created_date,  //작성일
              content: comment.content, //내용
              own: (userIds[i].user_id === ownerId) ? true: false,  //수신자 소유 게시물 여부
              oid: user[0].id,
              onik: user[0].nickname,
              ourl: user[0].user_url, //소유자 유저 유얄엘
              evt: ENUMS.EVENT_ID.COMMENT,  //이벤트 아이디
              qcid: activityId,
              push: userIds[i].on_all
            };
            notifications.push(notification);
          }
          Notification.create(notifications, function (err){
            if(err)
              return cb(err, null);
            return cb(null, null);
          });
        });
      }
    ],
    function (err, results){
      if(err) {
        connection.rollback();
        connection.release();
        return callback(err, null);
      }
      connection.commit(function(err) {
        if (err) {
          connection.rollback();
          connection.release();
          return callback(err, null);
        }
        connection.release();
        return callback(null, {commentId: activityId, notifications: notifications});
      });//end of commit
    });//end of waterfall
  });//end of getConnecton
}


function selectComments (questionId, userId, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }

    var stmt = 'SELECT '
      + 'qc.id, qc.user_id, qc.question_id, qc.content, qc.created_date, '
      + 'u.nickname, u.user_url, pp.small_url AS profile_small_url '
      + 'FROM question_comment qc '
      + 'INNER JOIN user u ON qc.user_id = u.id '
      + 'INNER JOIN profile_photo pp ON qc.user_id = pp.user_id '
      + 'WHERE qc.question_id = ? AND deleted_date IS NULL ORDER BY qc.id';

    var sql = connection.query(stmt, [questionId], function (err, comments){
      console.log('sql.sql', sql.sql);
      connection.release();
      if(err)
        return callback(err, null);
      return callback(null, comments);
    });
  });//end of getConnecton
}


function deleteComment (commentId, userId, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }

    var stmt = 'UPDATE question_comment SET deleted_date = now() WHERE id = ? AND user_id = ?';
    connection.query(stmt, [commentId, userId], function (err, result){
      connection.release();
      if(err)
        return callback(err, null);
      if(result.affectedRows > 0) {
        //알림 목록에서 삭제
        Notification.remove({qcid: commentId}, function (err){
          if(err)
            return callback(err, null);
          return callback(null, ENUMS.RESULT.SUCCESS);
        });
      }else {
        return callback(null, ENUMS.RESULT.FAIL);
      }
    });
  });//end of getConnecton
}


function insertQuestionLike (like, ownerId, sender, callback) {
  var activityId = null;

  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }

    var stmt = 'SELECT count(*) AS cnt FROM question_like WHERE question_id = ? AND user_id = ?';
    connection.query(stmt, [like.question_id, like.user_id], function (err, result){
      if (err){
        connection.release();
        return callback(err, null);
      }

      if (result[0].cnt) { //이미 좋아요를 한 경우
        connection.release();
        return callback(null, ENUMS.RESULT.FAIL);
      }

      async.waterfall([
        function transactionStart(cb){
          connection.beginTransaction(function (err){
            if(err)
              return cb(err, null);
            return cb(null, null);
          });
        },
        //좋아요 db에 삽입
        function (result, cb){  //좋아요 삽입
          var stmt = 'INSERT INTO question_like SET ?';
          connection.query(stmt, like, function (err, result){
            if (err)
              return cb(err, null);
            activityId = result.insertId;
            return cb(null, connection, like.question_id, like.user_id);
          });
        },
        //알림 받을 유저 리스트
        //이벤트를 발생시킨 유저는 제외
        serviceUtil.selectUserIdsForQuestionLike,
        //알림 테이블에 알림 내용 삽입
        function (userIds, cb) {
          if(_.isEmpty(userIds))  //알림 대상이 없는 경우 (본인 글에 처음으로 좋아요를 눌렀을 경우)
            return cb(null, null);

          var stmt = 'SELECT u.id, u.nickname, u.user_url ' +
            'FROM user u ' +
            'INNER JOIN question q ON u.id = q.user_id ' +
            'WHERE q.id = ?';
          connection.query(stmt, like.question_id, function (err, user) {
            if (err) {return cb(err, null);}

            var notifications = [];
            for(var i = 0; i < userIds.length; i++) {
              var notification = {
                qid: like.question_id,  //질문 아이디
                ruid: userIds[i].user_id, //수신자 유저 아이디
                suid: sender.userId, //발송자 유저 아이디
                surl: sender.user_url, //발송자 유저 url
                spsu: sender.profile, //발송자 프로필 이미지 URL
                snik: sender.nickname, //발송자 닉네임
                cdate: like.created_date,  //작성일
                own: (userIds[i].user_id === ownerId) ? true: false,  //수신자 소유 게시물 여부
                oid: user[0].id,
                ourl: user[0].user_url, //소유자 유저 url
                onik: user[0].nickname,
                evt: ENUMS.EVENT_ID.QUESTION_LIKE,  //이벤트 아이디
                qlid: activityId //질문 좋아요 아이디
              };
              notifications.push(notification);
            }
            Notification.create(notifications, function (err){
              if(err)
                return cb(err, null);
              return cb(null, null);
            });
          });

        }
      ],
      function (err, results){
        if(err) {
          console.log('error', err);
          connection.rollback();
          connection.release();
          return callback(err, null);
        }
        connection.commit(function(err) {
          if (err) {
            connection.rollback();
            connection.release();
            return callback(err, null);
          }
          connection.release();
          return callback(null, ENUMS.RESULT.SUCCESS);  //좋아요 성공
        });//end of commit
      });//end of waterfall
    });//end of connection.query
  });//end of getConnecton
}

function deleteQuestionLike (questionId, userId, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }

    var stmt = 'DELETE FROM question_like WHERE question_id = ? AND user_id = ?';
    connection.query(stmt, [questionId, userId], function (err, result){
      connection.release();
      if(err)
        return callback(err, null);

      if(result.affectedRows > 0) {
        //알림 목록에서 삭제
        Notification.remove({qid: questionId, suid: userId, evt: ENUMS.EVENT_ID.QUESTION_LIKE}, function (err){
          if(err)
            return callback(err, null);
          return callback(null, ENUMS.RESULT.SUCCESS);//삭제 성공
        });
      }else {
        return callback(null, ENUMS.RESULT.FAIL); //이미 삭제된 경우
      }
    });
  });//end of getConnecton
}

function insertAnswerLike (like, questionId, ownerId, sender, callback) {
  var activityId = null;

  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }

    var stmt = 'SELECT count(*) AS cnt FROM answer_like WHERE answer_id = ? AND user_id = ?';
    connection.query(stmt, [like.answer_id, like.user_id], function (err, result){
      if (err){
        connection.release();
        return callback(err, null);
      }

      if (result[0].cnt) { //이미 좋아요를 한 경우
        connection.release();
        return callback(null, ENUMS.RESULT.FAIL);
      }

      async.waterfall([
          function transactionStart(cb){
            connection.beginTransaction(function (err){
              if(err)
                return cb(err, null);
              return cb(null, null);
            });
          },
          //좋아요 db에 삽입
          function (result, cb){  //좋아요 삽입
            var stmt = 'INSERT INTO answer_like SET ?';
            connection.query(stmt, like, function (err, result){
              if (err)
                return cb(err, null);
              activityId = result.insertId;
              return cb(null, connection, like.answer_id, like.user_id);
            });
          },
          //알림 받을 유저 리스트
          //이벤트를 발생시킨 유저는 제외
          serviceUtil.selectUserIdsForAnswerLike,
          //알림 테이블에 알림 내용 삽입
          function (userIds, cb) {
            if(_.isEmpty(userIds))  //알림 대상이 없는 경우 (본인 글에 처음으로 좋아요를 눌렀을 경우)
              return cb(null, null);

            var stmt = 'SELECT u.id, u.nickname, u.user_url ' +
              'FROM user u ' +
              'INNER JOIN answer a ON u.id = a.user_id ' +
              'WHERE a.id = ?';
            connection.query(stmt, like.answer_id, function (err, user) {
              if (err) {return cb(err, null);}

              var notifications = [];
              for(var i = 0; i < userIds.length; i++) {
                var notification = {
                  qid: questionId,  //질문 아이디
                  aid: like.answer_id,  //답변 아이디
                  ruid: userIds[i].user_id, //수신자 유저 아이디
                  suid: sender.userId, //발송자 유저 아이디
                  surl: sender.user_url, //발송자 유저 URL
                  spsu: sender.profile, //발송자 프로필 이미지 URL
                  snik: sender.nickname, //발송자 닉네임
                  cdate: like.created_date,  //작성일
                  own: (userIds[i].user_id === ownerId) ? true: false,  //수신자 소유 게시물 여부
                  oid: user[0].id,
                  ourl: user[0].user_url,
                  onik: user[0].nickname,
                  evt: ENUMS.EVENT_ID.ANSWER_LIKE,  //이벤트 아이디
                  alid: activityId //답변 좋아요 아이디
                };
                notifications.push(notification);
              }
              Notification.create(notifications, function (err){
                if(err)
                  return cb(err, null);
                return cb(null, null);
              });

            });


          }
        ],
        function (err, results){
          if(err) {
            connection.rollback();
            connection.release();
            return callback(err, null);
          }
          connection.commit(function(err) {
            if (err) {
              connection.rollback();
              connection.release();
              return callback(err, null);
            }
            connection.release();
            return callback(null, ENUMS.RESULT.SUCCESS);  //좋아요 성공
          });//end of commit
        });//end of waterfall
    });//end of connection.query
  });//end of getConnecton
}

function deleteAnswerLike (answerId, userId, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }

    var stmt = 'DELETE FROM answer_like WHERE answer_id = ? AND user_id = ?';
    connection.query(stmt, [answerId, userId], function (err, result){
      connection.release();
      if(err)
        return callback(err, null);

      if(result.affectedRows > 0) {
        //알림 목록에서 삭제
        Notification.remove({aid: answerId, suid: userId, evt: ENUMS.EVENT_ID.ANSWER_LIKE}, function (err){
          if(err)
            return callback(err, null);
          return callback(null, ENUMS.RESULT.SUCCESS);//삭제 성공
        });
      }else {
        return callback(null, ENUMS.RESULT.FAIL); //이미 삭제된 경우
      }
    });
  });//end of getConnecton
}


function insertQuestion (post, questionPhotos, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }

    async.waterfall([
      function transactionStart(cb) {
        connection.beginTransaction(function (err){
          if(err)
            return cb(err, null);
          return cb(null, null);
        });
      },
      //질문 테이블에 입력
      function insertQuestion (result, cb) {
        var stmt = 'INSERT INTO question SET ?';
        connection.query(stmt, post, function (err, questionResult) {
          if(err)
            return cb(err, null);
          return cb(null, questionResult.insertId); //질문의 아이디를 넘겨줌
        });
      },
      //질문 사진 테이블에 입력
      function insertQuestionPhoto (insertId, cb) {
        for(var i = 0; i < questionPhotos.length; i++) //질문 아이디
          questionPhotos[i][0] = insertId; //question_id

        var stmt = 'INSERT INTO question_photo (question_id, width, height, url) VALUES ?';
        connection.query(stmt, [questionPhotos], function (err, result) {
          if(err)
            return cb(err, null);
          return cb(null, insertId);
        });
      }
    ], function (err, insertId){
      if(err){
        connection.rollback();
        connection.release();
        return callback(err, null);
      }
      connection.commit(function(err) {
        if (err) {
          connection.rollback();
          connection.release();
          return callback(err, null);
        }
        connection.release();
        return callback(null, insertId);//질문 아이디
      });
    });//end of waterfall
  });
}

/**
 * 해당 질문의 관련된 내용을 가져옴
 * @param questionId  질문 아이디
 * @param callback false 삭제된 질문 인 경우, results 질문내용
 */
function selectQuestion (questionId, userId, views, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }

    async.parallel({
      post: function (cb) {
        //질문에 대한 모든 정보를 가져옴
        //질문아이디, 성별아이디, 성별이름, 카테고리 아이디, 카테고리명, 내용
        //작성일, 질문 사진url, 질문자아이디, 닉네임, 작은프로필url, 좋아요갯수
        var stmt = 'SELECT q.id, g.id AS gender_id, g.name AS gender_name, c.id AS category_id, '
          + 'c.name AS category_name, q.content AS content, '
          + 'q.created_date AS created_date, qp.url AS question_image_url, '
          + 'qp.width AS question_image_width, qp.height AS question_image_height, q.user_id AS user_id, '
          + 'u.nickname AS nickname, u.user_url, pp.small_url AS profile_small_url, '
          + 'ql.liked, qlc.like_count, ac.answer_count, qcc.comment_count '
          + 'FROM question AS q '
          + 'INNER JOIN user AS u ON q.user_id = u.id '
          + 'INNER JOIN category AS c ON q.category_id = c.id '
          + 'INNER JOIN gender AS g ON q.gender_id = g.id '
          + 'INNER JOIN profile_photo AS pp ON q.user_id = pp.user_id '
          + 'INNER JOIN question_photo AS qp ON q.id = qp.question_id '
          + 'LEFT OUTER JOIN (SELECT question_id, count(*) AS liked FROM question_like WHERE user_id = ? GROUP BY question_id) AS ql '
          + 'ON q.id = ql.question_id '
          + 'LEFT OUTER JOIN (SELECT question_id, count(*) AS like_count FROM question_like GROUP BY question_id) AS qlc '
          + 'ON q.id = qlc.question_id '
          + 'LEFT OUTER JOIN (SELECT question_id, count(*) AS answer_count FROM answer WHERE deleted_date IS NULL GROUP BY question_id) AS ac '
          + 'ON q.id = ac.question_id '
          + 'LEFT OUTER JOIN (SELECT question_id, count(*) AS comment_count FROM question_comment WHERE deleted_date IS NULL GROUP BY question_id) AS qcc '
          + 'ON q.id = qcc.question_id '
          + 'WHERE q.id = ? AND q.deleted_date IS NULL AND qp.width = ?';

        connection.query(stmt, [userId, questionId, ENUMS.QUESTION_PHOTO_WIDTH.MEDIUM],
          function (err, questionResult) {
          if (err) {
            return cb(err, null);
          }
          return cb(null, questionResult);
        });
      },
      ////질문의 댓글을 가져옴
      ////댓글 아이디, 닉네임, 내용, 작성일
      //questionComments: function (cb) {
      //  var stmt = 'SELECT qc.id, u.nickname, qc.user_id, pp.medium_url, qc.content, qc.created_date '
      //    +'FROM question_comment AS qc, user AS u, profile_photo AS pp '
      //    +'WHERE qc.question_id = ? AND qc.deleted_date IS NULL AND qc.user_id = u.id AND qc.user_id = pp.user_id ORDER BY qc.id';
      //
      //  connection.query(stmt, questionId, function (err, questionCommentResult) {
      //    if (err) {
      //      console.log('questionComments', err);
      //      return cb(err, null);
      //    }
      //    return cb(null, questionCommentResult);
      //  });
      //},
      answers: function (cb) { //답변에 대한 모든 정보를 받아오는 쿼리
        var stmt = 'SELECT a.id, a.question_id, a.user_id, a.price, a.link, a.content, al.liked, alc.like_count, '
          + 'c.name AS category_name, u.nickname, u.user_url, pp.small_url AS profile_small_url, g.name AS grade_name, '
          + 'ap.url AS answer_image_url, ap.width AS answer_image_width, ap.height AS answer_image_height '
          + 'FROM answer AS a '
          + 'INNER JOIN user AS u ON a.user_id = u.id '
          + 'INNER JOIN profile_photo AS pp ON a.user_id = pp.user_id '
          + 'INNER JOIN answer_photo AS ap ON a.id = ap.answer_id '
          + 'INNER JOIN question AS q ON a.question_id = q.id '
          + 'INNER JOIN category AS c ON q.category_id = c.id '
          + 'INNER JOIN grade AS g ON u.grade_id = g.id '
          + 'LEFT OUTER JOIN (SELECT answer_id, count(*) AS liked FROM answer_like WHERE user_id = ? GROUP BY answer_id) AS al '
          + 'ON a.id = al.answer_id '
          + 'LEFT OUTER JOIN (SELECT answer_id, count(*) AS like_count FROM answer_like GROUP BY answer_id) AS alc '
          + 'ON a.id = alc.answer_id '
          + 'WHERE a.question_id = ? AND a.deleted_date IS NULL AND ap.width = ?';

        var sql = connection.query(stmt, [userId, questionId, ENUMS.ANSWER_PHOTO_WIDTH.MEDIUM],
        function (err, answerResult) {
          if (err) {
            return callback(err, null);
          }
          return cb(null, answerResult);
        });
      },
      views: function (cb) { //답변에 대한 모든 정보를 받아오는 쿼리
        if(!views)
          return cb(null, null);
        var stmt = 'UPDATE question SET hits=hits+1 WHERE id = ? AND deleted_date IS NULL';

        connection.query(stmt, questionId, function (err, answerResult) {
            if (err) {
              return callback(err, null);
            }
            return cb(null, null);
          });
      }
    }, function (err, results) {
      connection.release();
      if(err) {return callback(err, null);}
      if(_.isEmpty(results.post))
        return callback(null, false);
      return callback(null, results);
    });
  });
}
/**
 * 질문글 수정
 * 본인글이 맞는지 확인
 * 질문 수정
 * @param questionId  수정할 질문 아이디
 * @param userId  유저 아이디
 * @param post  수정할 질문 내용
 * @param callback false: 본인글이 아님, null: 수정완료
 */
function updateQuestion (questionId, userId, post, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }

    //질문 수정 요청 유저의 질문이 맞는지 확인
    var stmt = 'SELECT id, user_id FROM question WHERE id = ? AND user_id = ?';
    connection.query(stmt, [questionId, userId], function (err, question) {
      if(err) {
        connection.release();
        return callback(err, null);
      }
      if(_.isEmpty(question)) { //본인 질문이 아닌 경우
        connection.release();
        return callback(null, false);
      }

      async.waterfall([
        function transactionStart(cb) {
          connection.beginTransaction(function (err){
            if(err)
              return cb(err, null);
            return cb(null, null);
          });
        },
        //질문 수정
        function updatetQuestion (result, cb) {
          var stmt = 'UPDATE question SET ? WHERE id = ?';
          connection.query(stmt, [post, questionId], function (err, result) {
            if(err)
              return cb(err, null);
            return cb(null, null);
          });
        }
      ], function (err, result){
        if(err){
          connection.rollback();
          connection.release();
          return callback(err, null);
        }connection.commit(function(err) {
          if (err) {
            connection.rollback();
            connection.release();
            return callback(err, null);
          }
          connection.release();
          return callback(null, null);
        });
      });//end of waterfall
    });
  });
};

function deleteQuestion (questionId, userId, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }
    var stmt = 'UPDATE question SET deleted_date = now() WHERE id = ? AND user_id = ?';
    connection.query(stmt, [questionId, userId], function (err, result){
      connection.release();
      if(err)
        return callback(err, null);
      return callback(null, result.affectedRows);
    });
  });
}

function insertAnswer (answer, answerPhotos, ownerId, sender, callback) {
  POOL.getConnection(function (err, connection) {
    var activityId = null;
    var notifications = [];

    if (err) { return callback(err, null); }

    async.waterfall([
      function transactionStart(cb) {
        connection.beginTransaction(function (err){
          if(err)
            return cb(err, null);
          return cb(null, null);
        });
      },
      //답변 테이블에 입력
      function insertAnswer (result, cb) {
        var stmt = 'INSERT INTO answer SET ?';
        connection.query(stmt, answer, function (err, result) {
          if(err)
            return cb(err, null);
          activityId = result.insertId; //답변의 아이디
          return cb(null, null); //답변의 아이디
        });
      },
      //답변 사진 테이블에 입력
      function insertAnswerPhoto (result, cb) {
        for(var i = 0; i < answerPhotos.length; i++) //답변 아이디
          answerPhotos[i][0] = activityId; //answer_id

        var stmt = 'INSERT INTO answer_photo (answer_id, width, height, url) VALUES ?';
        connection.query(stmt, [answerPhotos], function (err, result){
          if(err){return cb(err, null);}
          return cb(null, connection, answer.question_id, answer.user_id);
        });
      },
      //알림 받을 유저 리스트
      //이벤트를 발생시킨 유저는 제외
      serviceUtil.selectUserIdsForAnswer,
      //알림 테이블에 알림 내용 삽입
      function (userIds, cb) {
        if(_.isEmpty(userIds))  //알림 대상이 없는 경우
          return cb(null, null);

        var stmt = 'SELECT u.id, u.nickname, u.user_url ' +
          'FROM user u ' +
          'INNER JOIN question q ON u.id = q.user_id ' +
          'WHERE q.id = ?';
        connection.query(stmt, answer.question_id, function (err, user){
          if(err){return cb(err, null);}

          for(var i = 0; i < userIds.length; i++) {
            var notification = {
              qid: answer.question_id,  //질문 아이디
              ruid: userIds[i].user_id, //수신자 유저 아이디
              suid: sender.userId, //발송자 유저 아이디
              surl: sender.user_url, //발송자 유저 URL
              spsu: sender.profile, //발송자 프로필 이미지 URL
              snik: sender.nickname, //발송자 닉네임
              cdate: answer.created_date,  //작성일
              own: (userIds[i].user_id === ownerId) ? true: false,  //수신자 소유 게시물 여부
              oid: user[0].id,
              ourl: user[0].user_url,
              onik: user[0].nickname,
              evt: ENUMS.EVENT_ID.ANSWER,  //이벤트 아이디
              aid: activityId, //답변 아이디
              push: userIds[i].on_all
            };
            notifications.push(notification);
          }
          Notification.create(notifications, function (err){
            if(err)
              return cb(err, null);
            return cb(null, null);
          });


        });

      }
    ], function (err, result){
      if(err){
        connection.rollback();
        connection.release();
        return callback(err, null);
      }
      connection.commit(function(err) {
        if (err) {
          connection.rollback();
          connection.release();
          return callback(err, null);
        }
        connection.release();
        return callback(null, {answerId: activityId, notifications: notifications});//질문 아이디
      });
    });//end of waterfall
  });
}


function deleteAnswer (answerId, userId, callback) {
  POOL.getConnection(function (err, connection) {
    if (err) { return callback(err, null); }

    var stmt = 'UPDATE answer SET deleted_date = now() WHERE id = ? AND user_id = ?';
    connection.query(stmt, [answerId, userId], function (err, result){
      connection.release();
      if(err)
        return callback(err, null);
      if(result.affectedRows > 0) {
        //알림 목록에서 삭제
        Notification.remove({aid: answerId}, function (err){
          if(err)
            return callback(err, null);
          return callback(null, ENUMS.RESULT.SUCCESS);
        });
      }else {
        return callback(null, ENUMS.RESULT.FAIL);
      }
    });
  });//end of getConnecton
}