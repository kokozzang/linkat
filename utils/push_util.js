var _ = require('underscore');
var serviceUtil = require('../services/service_util');
var gcm = require('node-gcm');
var async = require('async');

module.exports.sendPush = sendPush; // 푸시 전송

function sendPush (notifications, callback) {
  console.log('notifications', notifications);

  var notifications = _.where(notifications, {push: 1});
  var owner = _.where(notifications, {own: true});
  var others = _.where(notifications, {own: false});

  console.log('owner', owner);
  console.log('notifications own: false', others);

  var sender = new gcm.Sender(PUSH_CONFIG.GCM.API_KEY.SERVER);
  var registrationIds = [];

  async.parallel([
    function (cb){
      if(owner.length > 0){
        owner = owner[0];
        serviceUtil.selectPushListByUserIds([owner.ruid], function (err, pushList) {
          //pushList [{user_id, regid}]
          if (err)
            return callback(err, null);
          console.log('owner pushList', pushList);

          var message = null;
          switch(owner.evt) {
            case ENUMS.EVENT_ID.COMMENT:
              message = owner.snik + '님이 회원님의 게시물에 댓글을 남겼습니다.';
              break;
            case ENUMS.EVENT_ID.QUESTION_LIKE:
              break;
            case ENUMS.EVENT_ID.ANSWER:
              message = owner.snik + '님이 회원님의 게시물에 답변을 남겼습니다.';
              break;
            case ENUMS.EVENT_ID.ANSWER_LIKE:
              break;
          }

          var ownMessage = new gcm.Message({
            collapseKey: ENUMS.EVENT_ID_NAMES[owner.evt],
            delayWhileIdle: false,
            timeToLive: 86400,
            data: {
              title:'이옷어디',
              message: message,
              postId: owner.qid,
              ownerId: owner.oid,
              commentId: owner.qcid || null,
              answerId: owner.aid || null,
              evt: owner.evt
            }
          });
          registrationIds = _.pluck(pushList, 'regid');
          sender.send(ownMessage, registrationIds, 4, function (err, result) {
            if (err)
              return cb(err, null);
            console.log('owner push result', result);
            return cb(null, null);
          });
        });
      } //게시물 소유자가 있을 경우
    },
    function (cb){
      if(others.length > 0){
        var userIds = _.pluck(others, 'ruid');
        serviceUtil.selectPushListByUserIds(userIds, function (err, pushList) {
          //pushList [{user_id, regid}]
          if (err)
            return callback(err, null);
          console.log('others pushList', pushList);

          var message = null;
          switch(others[0].evt) {
            case ENUMS.EVENT_ID.COMMENT:
              message = others[0].snik + '님도 게시물에 댓글을 남겼습니다.';
              break;
            case ENUMS.EVENT_ID.QUESTION_LIKE:
              break;
            case ENUMS.EVENT_ID.ANSWER:
              message = others[0].snik + '님도 게시물에 답변을 남겼습니다.';
              break;
            case ENUMS.EVENT_ID.ANSWER_LIKE:
              break;
          }

          var othersMessage = new gcm.Message({
            collapseKey: ENUMS.EVENT_ID_NAMES[others[0].evt],
            delayWhileIdle: false,
            timeToLive: 86400,
            data: {
              title: '이옷어디',
              message: message,
              postId: others[0].qid,
              ownerId: others[0].oid,
              commentId: others[0].qcid || null,
              answerId: others[0].aid || null,
              evt: others[0].evt
            }
          });
          registrationIds = _.pluck(pushList, 'regid');
          sender.send(othersMessage, registrationIds, 4, function (err, result) {
            if (err)
              return cb(err, null);
            console.log('others push result', result);
            return cb(null, null);
          });
        });
      } //게시물 소유자가 있을 경우
    }
  ], function (err, results){
    if (err)
      return callback(err, null);
    return callback(null, null);
  });
}