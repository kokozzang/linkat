var winstonConfig = require('../config/winstonConfig');
var winston = require('winston');
require('winston-mongodb').MongoDB;

winston.loggers.add('accesslog', {
  console: winstonConfig.console,
  file: winstonConfig.file,
  MongoDB: winstonConfig.mongodb.accesslog
});

winston.loggers.add('errorlog', {
  console: winstonConfig.console,
  MongoDB: winstonConfig.mongodb.errorlog
});

module.exports.accesslogger = winston.loggers.get('accesslog');
module.exports.errorlogger = winston.loggers.get('errorlog');