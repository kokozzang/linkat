var _ = require('underscore');

module.exports.emailDuplicationCheck = emailDuplicationCheck; //이메일 중복 체크
module.exports.userUrlDuplicationCheck = userUrlDuplicationCheck; //user url 중복 체크
module.exports.registerInfo = registerInfo; //회원 정보 입력

module.exports.userUrlParamValidation = userUrlParamValidation; //userUrl 유효성 검사
module.exports.mypageQuery = mypageQuery; //mypage 쿼리
module.exports.insertFollow = insertFollow;
module.exports.deleteFollow = deleteFollow;


var MSG_VALIDATION = ENUMS.MSG_VALIDATION;


function emailDuplicationCheck (req, res, next){
  req.checkBody('value', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('value').toString();
  req.sanitize('value').trim();
  req.checkBody('value', MSG_VALIDATION.IS_EMAIL).isEmail();
  req.checkBody('value', MSG_VALIDATION.LEN).len(0, ENUMS.LIMIT.EMAIL);
  req.sanitize('value').normalizeEmail();
  //req.sanitize('value').escape();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function userUrlDuplicationCheck (req, res, next){
  req.checkBody('value', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('value').toString();
  req.sanitize('value').trim();
  req.checkBody('value', MSG_VALIDATION.IS_ALPHANUMERIC).isAlphanumeric();
  req.checkBody('value', MSG_VALIDATION.LEN).len(ENUMS.LIMIT.USER_URL.MIN, ENUMS.LIMIT.USER_URL.MAX);
  //req.sanitize('value').escape();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function registerInfo (req, res, next){
  //req.checkBody('email', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  //req.sanitize('email').toString();
  //req.sanitize('email').trim();
  //req.checkBody('email', MSG_VALIDATION.IS_EMAIL).isEmail();
  //req.checkBody('email', MSG_VALIDATION.LEN).len(0, ENUMS.LIMIT.EMAIL);
  //req.sanitize('email').normalizeEmail();
  //req.sanitize('email').escape();
  //
  //req.checkBody('password', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  //req.sanitize('password').toString();
  //req.sanitize('password').trim();
  //req.checkBody('password', MSG_VALIDATION.LEN).len(ENUMS.LIMIT.PASSWORD.MIN, ENUMS.LIMIT.PASSWORD.MAX);

  req.checkBody('nickname', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('nickname').toString();
  req.sanitize('nickname').trim();
  req.checkBody('nickname', MSG_VALIDATION.IS_ALPHAKORNUMERIC).isAlphaKorNumeric();
  req.checkBody('nickname', MSG_VALIDATION.LEN).len(ENUMS.LIMIT.NICKNAME.MIN, ENUMS.LIMIT.NICKNAME.MAX);
  //req.sanitize('nickname').escape();

  req.checkBody('userUrl', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('userUrl').toString();
  req.sanitize('userUrl').trim();
  //req.checkBody('userUrl', MSG_VALIDATION.IS_ALPHANUMERIC).isAlphanumeric();
  //req.checkBody('userUrl', MSG_VALIDATION.LEN).len(ENUMS.LIMIT.USER_URL.MIN, ENUMS.LIMIT.USER_URL.MAX);
  //req.sanitize('userUrl').escape();

  req.checkBody('birth', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('birth').toDate();
  req.checkBody('birth', MSG_VALIDATION.IS_DATE).isDate();

  req.checkBody('gender', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('gender').toInt();
  req.checkBody('gender', MSG_VALIDATION.IS_IN).isIn(_.values(ENUMS.USER_GENDER));

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}


function userUrlParamValidation (req, res, next){
  req.checkParams('userUrl', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('userUrl').toString();
  req.sanitize('userUrl').trim();
  req.checkParams('userUrl', MSG_VALIDATION.IS_ALPHANUMERIC).isAlphanumeric();
  req.checkParams('userUrl', MSG_VALIDATION.LEN).len(ENUMS.LIMIT.USER_URL.MIN, ENUMS.LIMIT.USER_URL.MAX);
  //req.sanitize('userUrl').escape();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function mypageQuery (req, res, next){
  req.checkQuery('page', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('page').toInt();
  req.checkQuery('page', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  req.checkQuery('startId', MSG_VALIDATION.IS_POSITIVE_NUMBER).optional().isPositiveNumber();

  req.checkQuery('limit', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('limit').toInt();
  req.checkQuery('limit', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function insertFollow (req, res, next){
  req.checkBody('targetId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('targetId').toInt();
  req.checkBody('targetId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  //자신이 자신을 팔로우하는지 확인
  if(req.user.id === req.body.targetId)
    return next('wrong targetId: targetId === userId');

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function deleteFollow (req, res, next){
  req.checkParams('targetId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('targetId').toInt();
  req.checkParams('targetId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  //자신이 자신을 팔로우하는지 확인
  if(req.user.id === req.params.targetId)
    return next('wrong targetId: targetId === userId');

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}