var _ = require('underscore');

module.exports.updateProfilePhoto = updateProfilePhoto; //프로필 이미지 수정
module.exports.updateEmail = updateEmail; //이메일 수정
module.exports.updateNickname = updateNickname; //닉네임 중복 체크
module.exports.updateUserURL = updateUserURL; //userURL 수정
module.exports.updateIntroduce = updateIntroduce; //자기소개 수정
module.exports.insertDevice = insertDevice; //device 추가

var MSG_VALIDATION = ENUMS.MSG_VALIDATION;

function updateProfilePhoto (req, res, next){
  req.checkFiles('img', MSG_VALIDATION.IS_IMAGE).isImage();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function updateEmail (req, res, next){
  req.checkBody('email', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('email').toString();
  req.sanitize('email').trim();
  req.checkBody('email', MSG_VALIDATION.IS_EMAIL).isEmail();
  req.checkBody('email', MSG_VALIDATION.LEN).len(0, ENUMS.LIMIT.EMAIL);
  req.sanitize('email').normalizeEmail();
  //req.sanitize('email').escape();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function updateNickname (req, res, next){
  req.checkBody('nickname', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('nickname').toString();
  req.sanitize('nickname').trim();
  req.checkBody('nickname', MSG_VALIDATION.IS_ALPHAKORNUMERIC).isAlphaKorNumeric();
  req.checkBody('nickname', MSG_VALIDATION.LEN).len(ENUMS.LIMIT.NICKNAME.MIN, ENUMS.LIMIT.NICKNAME.MAX);
  //req.sanitize('nickname').escape();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function updateUserURL (req, res, next){
  req.checkBody('userUrl', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('userUrl').toString();
  req.sanitize('userUrl').trim();
  req.checkBody('userUrl', MSG_VALIDATION.IS_ALPHANUMERIC).isAlphanumeric();
  req.checkBody('userUrl', MSG_VALIDATION.LEN).len(ENUMS.LIMIT.USER_URL.MIN, ENUMS.LIMIT.USER_URL.MAX);
  //req.sanitize('userUrl').escape();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function updateIntroduce (req, res, next){
  req.checkBody('introduce', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('introduce').toString();
  req.sanitize('introduce').trim();
  req.checkBody('introduce', MSG_VALIDATION.LEN).len(0, ENUMS.LIMIT.INTRODUCE);
  //req.sanitize('introduce').escape();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function insertDevice (req, res, next){
  req.checkBody('uuid', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('uuid').toString();
  req.sanitize('uuid').trim();
  //req.sanitize('uuid').escape();

  req.checkBody('regid', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('regid').toString();
  req.sanitize('regid').trim();
  //req.sanitize('regid').escape();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}