var _ = require('underscore');

module.exports.signIn = signIn; //signIn
module.exports.signUp = signUp; //signUp

var MSG_VALIDATION = ENUMS.MSG_VALIDATION;


function signIn (req, res, next){
  req.checkBody('email', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('email').toString();
  req.sanitize('email').trim();
  req.checkBody('email', MSG_VALIDATION.IS_EMAIL).isEmail();
  req.checkBody('email', MSG_VALIDATION.LEN).len(0, ENUMS.LIMIT.EMAIL);
  req.sanitize('email').normalizeEmail();
  //req.sanitize('email').escape();

  req.checkBody('password', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('password').toString();
  req.sanitize('password').trim();
  req.checkBody('password', MSG_VALIDATION.LEN).len(ENUMS.LIMIT.PASSWORD.MIN, ENUMS.LIMIT.PASSWORD.MAX);

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function signUp (req, res, next){
  req.checkBody('email', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('email').toString();
  req.sanitize('email').trim();
  req.checkBody('email', MSG_VALIDATION.IS_EMAIL).isEmail();
  req.checkBody('email', MSG_VALIDATION.LEN).len(0, ENUMS.LIMIT.EMAIL);
  req.sanitize('email').normalizeEmail();
  //req.sanitize('email').escape();

  req.checkBody('password', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('password').toString();
  req.sanitize('password').trim();
  req.checkBody('password', MSG_VALIDATION.LEN).len(ENUMS.LIMIT.PASSWORD.MIN, ENUMS.LIMIT.PASSWORD.MAX);

  req.checkBody('nickname', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('nickname').toString();
  req.sanitize('nickname').trim();
  req.checkBody('nickname', MSG_VALIDATION.IS_ALPHAKORNUMERIC).isAlphaKorNumeric();
  req.checkBody('nickname', MSG_VALIDATION.LEN).len(ENUMS.LIMIT.NICKNAME.MIN, ENUMS.LIMIT.NICKNAME.MAX);
  //req.sanitize('nickname').escape();

  req.checkBody('userUrl', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('userUrl').toString();
  req.sanitize('userUrl').trim();
  //req.checkBody('userUrl', MSG_VALIDATION.IS_ALPHANUMERIC).isAlphanumeric();
  //req.checkBody('userUrl', MSG_VALIDATION.LEN).len(ENUMS.LIMIT.USER_URL.MIN, ENUMS.LIMIT.USER_URL.MAX);
  //req.sanitize('userUrl').escape();

  req.checkBody('birth', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('birth').toDate();
  req.checkBody('birth', MSG_VALIDATION.IS_DATE).isDate();

  req.checkBody('gender', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('gender').toInt();
  req.checkBody('gender', MSG_VALIDATION.IS_IN).isIn(_.values(ENUMS.USER_GENDER));

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}