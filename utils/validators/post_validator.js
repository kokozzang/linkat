var _ = require('underscore');
//validation 메소드

module.exports.crawl = crawl; //image crawl

module.exports.insertQuestion = insertQuestion; //질문 입력
module.exports.selectQuestion = selectQuestion; //질문 조회
module.exports.updateQuestion = updateQuestion; //질문 수정
module.exports.deleteQuestion = deleteQuestion; //질문 삭제

module.exports.insertComment = insertComment; //댓글 입력
module.exports.deleteComment = deleteComment; //댓글 삭제

module.exports.insertQuestionLike = insertQuestionLike; //질문 좋아요 입력
module.exports.deleteQuestionLike = deleteQuestionLike; //질문 좋아요 삭제

module.exports.insertAnswerLike = insertAnswerLike; //답변 좋아요 입력
module.exports.deleteAnswerLike = deleteAnswerLike; //답변 좋아요 삭제

module.exports.selectPosts = selectPosts; //뉴스피드 페이지 요청

module.exports.insertAnswer = insertAnswer; //답변 입력
module.exports.deleteAnswer = deleteAnswer; //답변 삭제


var MSG_VALIDATION = ENUMS.MSG_VALIDATION;

function crawl(req, res, next){
  req.checkParams('questionId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('questionId').toInt();
  req.checkParams('questionId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();


  req.checkBody('link', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('link').toString();
  req.sanitize('link').trim();
  req.checkBody('link', MSG_VALIDATION.IS_URL).isURL({protocols:['http', 'https']});
  req.checkBody('link', MSG_VALIDATION.LEN).len(0, ENUMS.LIMIT.ANSWER_LINK);
  //req.sanitize('link').escape();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function insertQuestion (req, res, next){
  req.checkBody('gender', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('gender').toInt();
  req.checkBody('gender', MSG_VALIDATION.IS_IN).isIn(_.values(ENUMS.USER_GENDER));

  req.checkBody('category', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('category').toInt();
  req.checkBody('category', MSG_VALIDATION.IS_IN).isIn(_.values(ENUMS.CATEGORY));

  req.checkBody('content', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('content').toString();
  req.sanitize('content').trim();
  req.checkBody('content', MSG_VALIDATION.LEN).len(0, ENUMS.LIMIT.QUESTION_CONTENT);
  //req.sanitize('content').escape();
  req.checkFiles('imgs', MSG_VALIDATION.IS_IMAGE).isImage();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function selectQuestion (req, res, next){
  req.checkParams('questionId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('questionId').toInt();
  req.checkParams('questionId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function updateQuestion (req, res, next){
  req.checkParams('questionId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('questionId').toInt();
  req.checkParams('questionId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  req.checkBody('gender', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('gender').toInt();
  req.checkBody('gender', MSG_VALIDATION.IS_IN).isIn(_.values(ENUMS.USER_GENDER));

  req.checkBody('category', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('category').toInt();
  req.checkBody('category', MSG_VALIDATION.IS_IN).isIn(_.values(ENUMS.CATEGORY));

  req.checkBody('content', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('content').toString();
  req.sanitize('content').trim();
  req.checkBody('content', MSG_VALIDATION.LEN).len(0, ENUMS.LIMIT.QUESTION_CONTENT);
  //req.sanitize('content').escape();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function deleteQuestion (req, res, next){
  req.checkParams('questionId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('questionId').toInt();
  req.checkParams('questionId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function insertChop (req, res, next){
  req.checkBody('questionId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('questionId').toInt();
  req.checkBody('questionId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  req.checkBody('answerId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('answerId').toInt();
  req.checkBody('answerId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  req.checkBody('ownerId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('ownerId').toInt();
  req.checkBody('ownerId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function deleteChop (req, res, next){
  req.checkParams('questionId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('questionId').toInt();
  req.checkParams('questionId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  req.checkBody('answerId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('answerId').toInt();
  req.checkBody('answerId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function insertComment (req, res, next){
  req.checkParams('questionId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('questionId').toInt();
  req.checkParams('questionId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  req.checkBody('ownerId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('ownerId').toInt();
  req.checkBody('ownerId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  req.checkBody('content', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('content').toString();
  req.sanitize('content').trim();
  req.checkBody('content', MSG_VALIDATION.LEN).len(0, ENUMS.LIMIT.QUESTION_COMMENT_CONTENT);
  //req.sanitize('content').escape();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function deleteComment (req, res, next){
  req.checkParams('questionId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('questionId').toInt();
  req.checkParams('questionId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  req.checkParams('commentId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('commentId').toInt();
  req.checkParams('commentId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function insertQuestionLike (req, res, next){
  req.checkParams('questionId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('questionId').toInt();
  req.checkParams('questionId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  req.checkBody('ownerId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('ownerId').toInt();
  req.checkBody('ownerId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function deleteQuestionLike (req, res, next){
  req.checkParams('questionId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('questionId').toInt();
  req.checkParams('questionId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function insertAnswerLike (req, res, next){
  req.checkParams('questionId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('questionId').toInt();
  req.checkParams('questionId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  req.checkParams('answerId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('answerId').toInt();
  req.checkParams('answerId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  req.checkBody('ownerId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('ownerId').toInt();
  req.checkBody('ownerId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function deleteAnswerLike (req, res, next){
  req.checkParams('answerId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('answerId').toInt();
  req.checkParams('answerId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function selectPosts (req, res, next){
  req.checkQuery('page', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('page').toInt();
  req.checkQuery('page', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  //req.checkQuery('startId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  //req.sanitize('startId').optional().toInt();
  req.checkQuery('startId', MSG_VALIDATION.IS_POSITIVE_NUMBER).optional().isPositiveNumber();


  req.checkQuery('limit', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('limit').toInt();
  req.checkQuery('limit', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function insertAnswer (req, res, next){
  req.checkParams('questionId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('questionId').toInt();
  req.checkParams('questionId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  req.checkBody('ownerId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('ownerId').toInt();
  req.checkBody('ownerId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  //req.checkBody('product', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  //req.sanitize('product').toString();
  //req.sanitize('product').trim();
  //req.checkBody('product', MSG_VALIDATION.LEN).len(0, ENUMS.LIMIT.ANSWER_PRODUCT);
  ////req.sanitize('product').escape();

  req.checkBody('price', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('price').toInt();
  req.checkBody('price', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  req.checkBody('link', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('link').toString();
  req.sanitize('link').trim();
  req.checkBody('link', MSG_VALIDATION.IS_URL).isURL({protocols:['http', 'https']});
  req.checkBody('link', MSG_VALIDATION.LEN).len(0, ENUMS.LIMIT.ANSWER_LINK);
  //req.sanitize('link').escape();

  req.checkBody('content', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('content').toString();
  req.sanitize('content').trim();
  req.checkBody('content', MSG_VALIDATION.LEN).len(0, ENUMS.LIMIT.QUESTION_CONTENT);
  //req.sanitize('content').escape();

  req.checkFiles('imgs', MSG_VALIDATION.IS_IMAGE).isImage();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}

function deleteAnswer (req, res, next){
  req.checkParams('answerId', MSG_VALIDATION.NOT_EMPTY).notEmpty();
  req.sanitize('answerId').toInt();
  req.checkParams('answerId', MSG_VALIDATION.IS_POSITIVE_NUMBER).isPositiveNumber();

  var errors = req.validationErrors();
  if(errors)
    return next(errors);
  return next();
}