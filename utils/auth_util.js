var request = require('request');

exports.accessTokenVerifier = function (req, res, next){
  console.log('accessTokenVerifier ');
  var accessToken = req.headers['x-auth-token'];
  var method = 'GET';
  var url = 'https://graph.facebook.com/me?access_token=' + accessToken;
  var options = {
    method: method,
    url: url
  };
  request(options, function (err, response, body){
    if (err) { return next(err); }

    var body = JSON.parse(body);
    //유효하지 않은 토큰인 경우
    if(response.statusCode === 400){
      //return next(new Error('accessTokenVerifier: invalid access token'));
      console.error(body);
      return next(new Error(body));
    }
    req.profile = body;
    return next();
  });
};

module.exports.getUserIdByOAuth = function (req, res, next){
  var oauth_id = ENUMS.OAUTH.FACEBOOK;
  var oauth_account = req.profile.id;

  POOL.getConnection(function (err, connection){
    if (err) { return next(err); }

    var stmt = 'SELECT u.id, u.nickname, u.user_url, pp.small_url AS profile_small_url '
      + 'FROM oauth_connected oc '
      + 'INNER JOIN user u ON oc.user_id = u.id '
      + 'INNER JOIN user_state us ON oc.user_id = us.user_id '
      + 'INNER JOIN profile_photo pp ON oc.user_id = pp.user_id '
      + 'WHERE oc.oauth_account = ? AND oc.oauth_id = ? AND us.end_date IS NULL';

    connection.query(stmt, [oauth_account, oauth_id], function (err, result){
      connection.release();
      if (err) { return next(err); }
      req.user = {};
      req.user.id = result[0].id;
      req.user.nickname = result[0].nickname;
      req.user.user_url= result[0].user_url;
      req.user.profile_small_url = result[0].profile_small_url;
      return next();
    });
  });
};