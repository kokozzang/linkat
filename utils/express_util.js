
exports.errorHandler = function (err, req, res, next) {
//  if (!Array.isArray(err)) {
//    err = [err];
//  }
//  err.forEach(function (err) {
//    if (err instanceof Error) {
////      console.error('errHandler 객체', err.stack || err);
//    } else {
////      console.error('errHandler', err);
//    }
//  });
  if (req.xhr) {
    res.status(err.httpStatus).send({error: err});
  } else {
		//console.error('error', err);
    res.status(err.httpStatus).send({error: err});
    //res.render('error', {error: err.message});
  }
};

exports.requirePermission = function (role, level) {
	return function (req, res, next) {
		var user = req.session.user;
		if (!user) {
			return res.redirect('/signin');
		}

		var roles = Object.keys(user.roles);
		var hasPermission = roles.some(function (r) {
			if (r === role && (!level || (level && user.roles[r] <= level))) {
				return true;
			}
		});
		if (!hasPermission) {
			return next('permission denied', 403);
		}

		next();
	};
};

exports.requireAdminPermission = function (level) {
	return exports.requirePermission('ADMIN', level);
};

exports.paginationHelper = function () {
	return function (req, res, next) {
		var page = req.query.page ? parseInt(req.query.page, 10) : 1;
		var perPage = req.query.perPage ? parseInt(req.query.perPage, 10) : 20;
		var sortField = req.query.sort;
		var sortOrder = (req.query.order === '1' ? 1 : -1);

		var sort;
		if (sortField) {
			sort = {};
			sort[sortField] = sortOrder;
		}

		var pagination = {
			page: page,
			perPage: perPage,
			skip: Math.max(0, page - 1) * perPage
		};
		if (sort) {
			pagination.sort = sort;
			pagination.order = sortOrder;
		}
		req.pagination = pagination;

		next();
	};
};
