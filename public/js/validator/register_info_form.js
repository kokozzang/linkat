$(document).ready(function() {
  $('#birth').datepicker({
    language: 'kr',
    format: 'yyyy.mm.dd',
    autoclose: true
  }).on('changeDate', function(e){
    $('#registerInfoForm').bootstrapValidator('revalidateField', 'birth');
  });

  $('#registerInfoForm').bootstrapValidator({
    fields: {
      nickname: { //닉네임
        validators: {
          notEmpty: {message: '닉네임을 입력해주세요.'},
          stringLength: {
            max: 10,
            message: '1자 이상 10자 이하만 가능합니다.'
          },
          regexp: {
            regexp: /^[가-힣a-zA-Z0-9\s]+$/i,
            message: '한글, 숫자, 영문자, 공백만 가능합니다.'
          }
        }
      },
      user_url: {  //url
        validators: {
          notEmpty: {message: 'URL을 입력해주세요.'},
          stringLength: {
            max: 10,
            message: '1자 이상 10자 이하만 가능합니다. '
          },
          regexp: {
            regexp: /^[a-zA-Z0-9]+$/i,
            message: '영문자, 숫자만 가능합니다.'
          },
          remote: {
            message: '사용중인 URL 입니다',
            url: '/user/userUrlDuplicationCheck'
          }
        }
      },
      birth: {  //생일
        validators: {
          notEmpty: {message: '생일을 입력해주세요.'}
//          date: {
//            format: 'YYYY.MM.DD',
//            message: 'The value is not a valid date'
//          }
        }
      },
      gender: {  //성별
        validators: {
          notEmpty: {message: '성별을 선택해주세요.'}
        }
      }
    }//end of fields
  });

});