$(document).ready(function() {
  $('#modifyForm').bootstrapValidator({
    fields: {
      email: {
        validators: {
//          notEmpty: {message: '이메일을 입력해주세요.'},
          emailAddress: {
            message: '올바르지 않은 이메일 형식입니다.　'
          },
          remote: {
            message: '사용중인 이메일 입니다.',
            url: '/user/emailDuplicationCheck'
          }
        }
      },
      nickname: { //닉네임
        validators: {
          notEmpty: {message: '닉네임을 입력해주세요.'},
          stringLength: {
            max: 10,
            message: '1자 이상 10자 이하만 가능합니다.'
          },
          regexp: {
            regexp: /^[가-힣a-zA-Z0-9\s]+$/i,
            message: '한글, 숫자, 영문자, 공백만 가능합니다.'
          }
        }
      },
      user_url: {  //url
        validators: {
          notEmpty: {message: 'URL을 입력해주세요.'},
          stringLength: {
            max: 10,
            message: '1자 이상 10자 이하만 가능합니다. '
          },
          regexp: {
            regexp: /^[a-zA-Z0-9]+$/i,
            message: '영문자, 숫자만 가능합니다.'
          },
          remote: {
            message: '사용중인 URL 입니다',
            url: '/user/userUrlDuplicationCheck'
          }
        }
      }
    }//end of fields
  });
  $('#changePassword').bootstrapValidator({
    submitButtons: 'button[type="submit"]',
    fields: {
      inputPassword: {
        validators: {
//          notEmpty: {message: '패스워드를 입력해주세요.'},
          stringLength: {
            min: 6,
            message: '6글자 이상 입력해주세요.　'
          },
          identical: {
            field: 'reInputPassword',
            message: '패스워드를 확인해주세요.　'
          }
        }
      },
      reInputPassword: {
        validators: {
//          notEmpty: {message: '패스워드를 한번 더 입력해주세요.'},
          stringLength: {
            min: 6,
            message: '6글자 이상 입력해주세요.　'
          },
          identical: {
            field: 'inputPassword',
            message: '패스워드를 확인해주세요.　'
          }
        }
      }
    }
  }).on('success.form.bv', function (e) {
    e.preventDefault();
    alert('ab');
    var $form = $(e.target);
    console.log($form.serialize());

    $.ajax({
      url: '/account/password',
      type: 'POST',
      data: $form.serialize(),
      dataType: 'json',
      success: function (result) {
        if(result) {
          $('#presentPassword').val('');
          $('#inputPassword').val('');
          $('#reInputPassword').val('');
          $('certificationModal').modal('hide');
          alert('성공적으로 변경되었습니다.');
        }
        else {
          alert('현재 비밀번호를 확인해 주세요.');
        }
      }
    });
  });

});