$(document).ready(function() {
  $('#loginForm').bootstrapValidator({
    submitButtons:'button[type="submit"]',
    fields: {
      inputEmail: {
        validators: {
          emailAddress: {
            message: '올바르지 않은 이메일 형식입니다.'
          }
        }
      },
      inputPassword: {
        validators: {
          stringLength: {
            min: 6,
            message: '6글자 이상 입력해주세요'
          }
        }
      }
    }//end of fields
  }).on('success.form.bv', function(e) {
    e.preventDefault();
    var $form = $(e.target);

    var params = {
      inputEmail: $('#inputEmail').val(),
      inputPassword: $('#inputPassword').val(),
      redirectUrl: getParameterByName(location.search, 'redirectUrl')
    };


    $.post('/auth/local/login', params, function (res){
      if(res.result === false){
        console.log('로그인 실패');
      }else{
        window.location.replace(res.url);
      }
    }, 'json');
  });
});
function getParameterByName(search, name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}