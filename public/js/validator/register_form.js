$(document).ready(function() {
  $('#registerForm').bootstrapValidator({
    fields: {
      inputEmail: {
        validators: {
//          notEmpty: {message: '이메일을 입력해주세요.'},
          emailAddress: {
            message: '올바르지 않은 이메일 형식입니다.　'
          },
          remote: {
            message: '사용중인 이메일 입니다.',
            url: '/user/emailDuplicationCheck'
          }
        }
      },
      inputPassword: {
        validators: {
//          notEmpty: {message: '패스워드를 입력해주세요.'},
          stringLength: {
            min: 6,
            message: '6글자 이상 입력해주세요.　'
          },
          identical: {
            field: 'reInputPassword',
            message: '패스워드를 확인해주세요.　'
          }
        }
      },
      reInputPassword: {
        validators: {
//          notEmpty: {message: '패스워드를 한번 더 입력해주세요.'},
          stringLength: {
            min: 6,
            message: '6글자 이상 입력해주세요.　'
          },
          identical: {
            field: 'inputPassword',
            message: '패스워드를 확인해주세요.　'
          }
        }
      }
    }//end of fields
  });
});