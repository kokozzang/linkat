$(document).ready(function() {
  var socket = io.connect('http://localhost');

  socket.on('notificationCount', function (notificationCount){
    if(notificationCount !== 0)
      $('#notiBtn').text(notificationCount);
  });


  var $container = $('#wrapper').masonry({
    itemSelector: '.post',
    transitionDuration: 0
  });
//  var $container = $('#wrapper');
  $container.imagesLoaded( function() {
//    $container.masonry({
//      itemSelector: '.post',
//      transitionDuration: 0
//    });

    $container.masonry();
  });

  $container.infinitescroll({
      navSelector: '#page-nav', // selector for the paged navigation
      nextSelector: '#page-nav a', // selector for the NEXT link (to page 2)
      itemSelector: '.post', // selector for all items you'll retrieve
      pixelsFromNavToBottom: 1000,
      loading: {
        finishedMsg: 'No more pages to load.',
        img: 'http://i.imgur.com/6RMhx.gif'
//        msgText: "<em>Loading the next set of posts...</em>"
      }
    },
// trigger Masonry as a callback
    function( newElements ) {
// hide new items while they are loading
      var $newElems = $( newElements ).css({ opacity: 0 });
// ensure that images load before adding to masonry layout
      $newElems.imagesLoaded(function(){
// show elems now they're ready
        $newElems.animate({ opacity: 1 });
        $container.masonry( 'appended', $newElems, true );
      });
    });



//  $(window).unbind('.infscr');
//
//// Resume Infinite Scroll
//  $('.nav-previous a').click(function(){
//    $container.infinitescroll('retrieve');
//    return false;
//  });


  var options = {
    placement: 'bottom',
    triger: 'click',
    title: 'title',
    content: 'loading...'
  };
  $('[data-toggle="popover"]').popover(options);
  $('[data-toggle="popover"]').on('shown.bs.popover', function () {
    $.get('/notificationList', function (result){
      $('.popover-content:visible').html(result);
    });
  });
  $('body').on('click', function(e) {
    $('[data-toggle="popover"]').each(function () {
      if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0)
        $(this).popover('hide');
    });
  });

  //뉴스피드 - 질문 좋아요
  $('.questionLikeBtn').click(function(e) {
    var $questionLikeBtn = $(this);
    var questionId = $questionLikeBtn.data('question-id');
    var eventMessage = 'insertQuestionLike';

    if($questionLikeBtn.hasClass('liked'))
      eventMessage = 'deleteQuestionLike';

    socket.emit(eventMessage, questionId);
  });

  //좋아요 이미지 처리
  socket.on('questionLikeInserted', function (likeResult){
    var $questionLikeBtn = $('button[data-question-id=' + likeResult.questionId + ']');
    var imageSrc = 'heart.png';
    $questionLikeBtn.addClass('liked');
    $questionLikeBtn.children('img').attr('src', '/img/icon/'+imageSrc);
    $questionLikeBtn.children('span').html(likeResult.likeCount);
  });
  socket.on('questionLikeDeleted', function (likeResult){
    var $questionLikeBtn = $('button[data-question-id=' + likeResult.questionId + ']');
    var imageSrc = 'heart0.png';
    $questionLikeBtn.removeClass('liked');
    $questionLikeBtn.children('img').attr('src', '/img/icon/'+imageSrc);
    $questionLikeBtn.children('span').html((likeResult.likeCount !== 0)? likeResult.likeCount: '');
  });
  socket.on('updateQuestionLikeCount', function (likeResult){
    var $questionLikeBtn = $('button[data-question-id=' + likeResult.questionId + ']');
    $questionLikeBtn.children('span').html((likeResult.likeCount !== 0)? likeResult.likeCount: '');
  });

  $('#postingModal').on('hide.bs.modal', function () {
    $(this).removeData();
  });
  $('#questionModal').on('hide.bs.modal', function (e){
    $(this).removeData();
  });


  $('#questionModal.modal.fade').click(function (e) {
    //질문모달의 백그라운드 영역을 클릭했을 경우
    if (e.target === $('div#questionModal.modal.fade')[0] && $('body').hasClass('modal-open')) {
      history.back();
    }
  });

  $(document).on('click', '.list-group-item', function(e){
    var qid = $(this).data('qid');
    location.href='/posts/' + qid;
  });
  $(document).on('click', '.snik', function(e){
    e.stopPropagation();
    alert('snik');
  });
});
//
//function test (result){
//  if()
//}