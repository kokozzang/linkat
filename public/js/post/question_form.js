$(document).ready(function() {
  $('#questionForm').submit(function(e) {
    var formData = new FormData(this);

    $.ajax({
      url: '/posts',
      type: 'POST',
      data: formData,
      mimeType: 'multipart/form-data',
      contentType: false,
      cache: false,
      processData:false,
      dataType: 'json',
      success: function(result){
        //질문 입력 성공시 질문폼 모달을 닫고 pjax로 질문 상세 모달을 띄움
        $('#postingModal').modal('hide');
        $('#post_pjax').attr('href', result.url).click();
      },
      error: function(jqXHR, textStatus, errorThrown){
//        alert(errorThrown);
      }
    });
    e.preventDefault();
  });
});