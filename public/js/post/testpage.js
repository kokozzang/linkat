$(document).ready(function() {
  $('#abc').on('click', function () {
    $.get('/testFunction', function (data) {
      $('#goAway').html(data);
    });
  });
  $('.toggle').on('click', function () {
    $(this).toggleClass('on')
  });
  $('.question.unliked').on('click', function () {
    $(this).addClass('liked');
    $(this).removeClass('unliked');
  });
  $('.question.liked').on('click', function (){
    $(this).addClass('unliked');
    $(this).removeClass('liked');
  })
});

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ko_KR/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
