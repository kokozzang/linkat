var $container = null;
var socketEvent = null;

$(document).ready(function () {
  //socket
//  var socket = io.connect('http://106.241.53.215');
  var socket = io.connect('http://localhost');
  $(document).pjax('a[data-pjax], [data-pjax] a', '#questionModal');

  socketEvent = new SocketEvent(socket); //소켓 이벤트 처리 객체 생성


  $(document).on('pjax:complete', function() {
    $('#questionModal').modal();

    $('#questionModal').on('shown.bs.modal', function (e) {
      $container = $('#answer_wrapper').masonry({
        itemSelector: '.answer',
        transitionDuration: 0
      });
      $container.imagesLoaded( function() {
        $container.masonry();
      });
    });
    $('.setting').hide();


    //좋아요
    $('.questionLikeBtn').click(function(e) {
      var $questionLikeBtn = $(this);
      var questionId = $questionLikeBtn.data('questionId');
      var ajaxJson = {
        url: '/posts/' + questionId + '/like',
        type: 'POST',
        data: {
          ownerId: $('.question').data('userId')
        }
      };

      if($questionLikeBtn.hasClass('liked')) {
        ajaxJson.type = 'DELETE';
        delete ajaxJson.data;
      }

      $.ajax(ajaxJson)
        .done(function (result, textStatus, jqXHR ){
          if(!jqXHRChecker(jqXHR))
            return false;
          if(result){
            if(!$questionLikeBtn.hasClass('liked'))
              socketEvent.emit.insertQuestionLike(questionId);  //좋아요 입력 이벤트
            else
              socketEvent.emit.deleteQuestionLike(questionId);  //좋아요 삭제 이벤트
          }else {
            alert('좋아요에 문제 있음');
          }
        });
    });

    $('#btnQuestionModify').on('click', function () {setting();});
    $('#btnQuestionDelete').on('click', function () {
      if (confirm('정말 삭제하시겠습니까?')) {
        $.ajax({
          url:'/post/'+questionId,
          type: 'DELETE',
          success: function (result) {
            if(result.result == 'SUCCESS')
              alert('삭제했음.');
            else
              alert('삭제안됐음.');
            window.location.replace('/');
          }
        });
      }
    });
    //댓글 입력
    var questionId = $('.question').data('questionId');


    $('#cmtBtn').click(function (e){
      e.preventDefault();
      var ajaxJson = {
        url: '/posts/' + questionId + '/comment',
        type: 'POST',
        data: {
          content: $('#cmtText').val(),
          ownerId: $('.question').data('userId')
        }
      };

      $.ajax(ajaxJson)
        .done(function (result, textStatus, jqXHR ){
          if(!jqXHRChecker(jqXHR))
            return false;
          socketEvent.emit.insertComment(questionId, result.commentId);
        });
      $('#cmtText').val('');
    });

    $('#cmtText').keyup(function(e){
      if(e.keyCode == 13){
        $('#cmtBtn').click();
      }
    });

    //댓글 삭제 버튼 클릭
    $('.list-group').on('click', '#cmtDel', function (e){
      e.preventDefault();
      e.stopPropagation();
      var commentId = $(this).val();
      var ajaxJson = {
        url: '/posts/' + questionId + '/comment/' + commentId,
        type: 'DELETE'
      };

      $.ajax(ajaxJson)
        .done(function (result){
          if(result.result)
            socketEvent.emit.deleteComment(questionId, commentId);  //댓글 삭제 이벤트
          else
            alert('댓글 삭제 실패');
        });
    });

    //댓글이 삭제됨
    socket.on('commentDeleted', function (commentId){
      $('.list-group-item[data-comment-id=' + commentId +']').remove();
    });

    socketEvent.emit.viewQuestion(questionId);  //질문 조회 이벤트 발생

    $('.answer').hover(function() {
      $(this).children('.answer_btn_group').show();
    },function() {
      $(this).children('.answer_btn_group').hide();
    });

    $('.answer_like').click(function(e) {
      e.preventDefault();
      var $answer = $(this).parent().parent();
      var answerId = $answer.data('answerId');
    });
    $('.answer_chop').click(function(e) {
      e.preventDefault();
      var $answer = $(this).parent().parent();
      var answerId = $answer.data('answerId');

      var ajaxJson = {
        url: '/chop',
        type: 'POST',
        data: {
          questionId: questionId,
          answerId: answerId,
          ownerId: $answer.data('userId')
        }
      };

      var isChopped = $answer.hasClass('chopped');
      if(isChopped) {
        ajaxJson.url = ajaxJson.url + '/' + questionId;
        ajaxJson.type = 'DELETE';
        ajaxJson.data = {answerId: answerId};
      }

      $.ajax(ajaxJson)
        .done(function (result, textStatus, jqXHR ){
          if(!jqXHRChecker(jqXHR))
            return false;
          if(!isChopped) {
            if(result.result)
              socketEvent.emit.insertChop(questionId, answerId);  //찹 입력 이벤트
            else
              alert('찹 gg안됨');
//              $('.answer[data-answer-id=' + answerId +']').addClass('chopped');
          }else {
            if(result.result)
              socketEvent.emit.deleteChop(answerId);  //찹 제거 이벤트
            else
              alert('찹 제거 안됨');
//              $('.answer[data-answer-id=' + answerId +']').removeClass('chopped');
          }
        });
    });

    $('.answer_delete').click(function(e) {
      e.preventDefault();
      if (confirm('정말 삭제하시겠습니까?')) {
        var $answer = $(this).parent().parent();
        var answerId = $answer.data('answerId');
        var questionId = $('.question').data('questionId');
        $.ajax({
          url: '/answer/'+answerId,
          type: 'DELETE',
          success: function(result){
            if(result.result) {
              socketEvent.emit.deleteAnswer(questionId, answerId);  //답변 입력 이벤트
            }else {
              alert('삭제안됐음.');
            }
          },
          error: function(jqXHR, textStatus, errorThrown){
            alert(errorThrown);
          }
        });
      }
    });


    $('#answerForm').bootstrapValidator({
      submitButtons:'button[type="submit"]',
      fields: {
        product: {
          validators: {
            notEmpty: {
              message: '상품명을 입력하세요'
            }
          }
        },
        price: {
          validators: {
            digits: {
              message: '양의 정수.'
            },
            notEmpty: {
              message: '가격을 입력하세요.'
            }
          }
        },
        link: {
          validators: {
            uri: {
              message: '올바르지 않은 주소입니다.'
            },
            notEmpty: {
              message: '링크를 입력하세요.'
            }
          }
        },
        content: {
          validators: {
            stringLength: {
              min: 6,
              message: '6글자 이상 입력해주세요.'
            },
            notEmpty: {
              message: '내용를 입력하세요'
            }
          }
        },
        imgs: {
          validators: {
            file: {
              extension: 'jpeg,jpg,png,bmp,gif',
              type: 'image/jpeg,image/png,image/x-ms-bmp,image/gif',
              maxSize: 2 * 1024 * 1024,   // 5 MB
              message: ' 2MB 이하로 아직 큰이미지 처리를 잘 못해여.'
            },
            notEmpty: {
              message: '이미지를 첨부해주세요.'
            }
          }
        }
      }//end of fields
    }).on('success.form.bv', function(e) {
      e.preventDefault();
      var formData = new FormData(this);
      formData.append('ownerId', $('.question').data('userId'));
      $.ajax({
        url: '/answer',
        type: 'POST',
        data: formData,
        mimeType: 'multipart/form-data',
        dataType: 'json',
        contentType: false,
        cache: false,
        processData:false,
        success: function(result){
          socketEvent.emit.insertAnswer(questionId, result.answerId);  //답변 입력 이벤트
          $('#answeringModal').modal('hide');
        },
        error: function(jqXHR, textStatus, errorThrown){
          alert(errorThrown);
        }
      });
    });

    $('#answeringModal').on('hidden.bs.modal', function () {
      $('body').addClass('modal-open');
      $(this).removeData();
    });

  });

  socketEvent.on.commentInserted();  //DOM 객체에 댓글 삽입 이벤트
  socketEvent.on.answerInserted();  //DOM 객체에 답변 삽입 이벤트

  socket.on('answerDeleted', function (answerId){
    $container.masonry('remove', $('.answer[data-answer-id=' + answerId +']'));
    $container.masonry();
  });

  socket.on('chopInserted', function (answerId){
    $('.answer[data-answer-id=' + answerId +']')
      .addClass('chopped')
      .children('.answer_btn_group')
      .children('.answer_chop')
      .html('CHOP 취소');
  });

  socket.on('chopDeleted', function (answerId){
    $('.answer[data-answer-id=' + answerId +']')
      .removeClass('chopped')
      .children('.answer_btn_group')
      .children('.answer_chop')
      .html('CHOP');
  });

  $(document).on('pjax:popstate', function() {
    var questionId = $('#q_id').val();
    socketEvent.emit.exitQuestion(questionId);  //질문 조회 종료 이벤트 발생
    $('#questionModal').modal('hide');
  });
});


/**
 * 소켓 이벤트 Emit 클래스
 * @param socket  소켓 객체
 */
function SocketEvent (socket) {
  this.emit = new SocketEmit(socket); //소켓 객체
  this.on = new SocketOn(socket); //소켓 객체
};

var SocketEmit = function (socket) {
  this.socket = socket;
};

var SocketOn = function (socket) {
  this.socket = socket;
};

SocketOn.prototype = {
  /**
   * DOM 객체에 댓글 삽입 이벤트
   */
  commentInserted: function (){
    this.socket.on('commentInserted', function (data){
      var divComment = '<div class="list-group-item" data-comment-id=' + data.commentId + '>'
        + '<div>'
        + '<span>' + data.nickname + '</span>'
        + '<span>' + data.createdDate + '</span>'
        + '<div class="pull-right">'
        + '<button id="cmtDel" value=' + data.commentId + '> x'
        + '</div>'
        + '</div>'
        + '<div>' + data.content + '</div>'
        + '</div>';

      $('.list-group').append(divComment);
      $(document).on('click', 'button #cmtDel', function (e){
        e.preventDefault();
        e.stopPropagation();
        var commentId = $(this).val();
        socketEvent.emit.deleteComment($('#q_id').val(), commentId);  //댓글 삭제 이벤트 발생
      });
    });
  },
  answerInserted: function (){
    this.socket.on('answerInserted', function (data){
      var divAnswer = '<div data-answer-id=' + data.id+ ' class="answer">'
        + '<div class="answer_btn_group" style="display: none;">'
//        + '<button class="qv_btn_group_bottom answer_like">'
//        + '<img src="/img/icon/heart0.png">'
//        + '</button>'
        + '<button class="qv_btn_group_bottom answer_chop">CHOP</button>'
        + '<button class="qv_btn_group_bottom answer_delete">삭제</button>'
        + '</div>'
        + '<a href="' + data.link + '">'
        + '<div class="answer_img">'
        + '<img src="' + data.answer_image_url+ '" '
        + 'style="width:' + data.answer_image_width + ';height:' + data.answer_image_height + ';"/>'
        + '</div>'
        + '<div class="bottom_answer_section">'
        + '<div class="content_section">'
        + '<div id="item_name">' + data.product_name + '</div>'
        + '<div id="item_price">' + data.price + '</div>'
        + '<div id="item_content">' + data.content + '</div>'
        + '</div></div></a>'
        + '<div class="pf_section">'
        + '<div class="answer_pf_img"><img src="' + data.profile_small_url+ '"/></div>'
        + '<div id="bottom_user_name">닉네임 ' + data.nickname + '　|등급　' + data.grade_name + '</div>'
        + '</div></div>';

      $container.append(divAnswer);
      var $answer = $('.answer[data-answer-id=' + data.id +']');

      $container.imagesLoaded(function(){
        $container.masonry( 'appended', $answer, true);
      });
      $answer.hover(function() {
        $(this).children('.answer_btn_group').show();
      },function() {
        $(this).children('.answer_btn_group').hide();
      });

      $answer.on('click', '.answer_delete', function (e){
        e.preventDefault();
        if (confirm('정말 삭제하시겠습니까?')) {
          var $answer = $(this).parent().parent();
          var answerId = $answer.data('answerId');
          var questionId = $('.question').data('questionId');
          $.ajax({
            url: '/answer/'+answerId,
            type: 'DELETE',
            success: function(result){
              if(result.result) {
                socketEvent.emit.deleteAnswer(questionId, answerId);  //답변 입력 이벤트
              }else {
                alert('삭제안됐음.');
              }
            },
            error: function(jqXHR, textStatus, errorThrown){
              alert(errorThrown);
            }
          });
        }
      });

      $answer.on('click', '.answer_chop', function(e) {
        e.preventDefault();
        var $answer = $(this).parent().parent();
        var answerId = $answer.data('answerId');
        var questionId = $('.question').data('questionId');
        var ajaxJson = {
          url: '/chop',
          type: 'POST',
          data: {
            questionId: questionId,
            answerId: answerId,
            ownerId: $answer.data('userId')
          }
        };

        var isChopped = $answer.hasClass('chopped');
        if(isChopped) {
          ajaxJson.url = ajaxJson.url + '/' + questionId;
          ajaxJson.type = 'DELETE';
          ajaxJson.data = {answerId: answerId};
        }

        $.ajax(ajaxJson)
          .done(function (result, textStatus, jqXHR ){
            if(!jqXHRChecker(jqXHR))
              return false;
            if(!isChopped) {
              if(result.result)
                socketEvent.emit.insertChop(questionId, answerId);  //찹 입력 이벤트
              else
                alert('찹 안됨');
            }else {
              if(result.result)
                socketEvent.emit.deleteChop(answerId);  //찹 제거 이벤트
              else
                alert('찹 제거 안됨');
            }
          });
      });

    });// end of this.socket.on('answerInserted')
  }// end of answerInserted
};

SocketEmit.prototype = {
  /**
   * 질문 조회 이벤트
   * @param questionId   질문 아이디
   */
  viewQuestion: function (questionId){
    this.socket.emit('viewQuestion', {questionId: questionId});
  },
  /**
   * 질문 조회 종료 이벤트
   * @param questionId   질문 아이디
   */
  exitQuestion: function (questionId){
    this.socket.emit('exitQuestion', {questionId: questionId});
  },
  /**
   * 댓글 삽입 이벤트
   * @param questionId   질문 아이디
   * @param content       댓글 내용
   */
  insertComment: function (questionId, commentId){
    this.socket.emit('insertComment', {questionId: questionId, commentId: commentId});
  },
  /**
   * 댓글 삭제 이벤트
   * @param questionId  질문 아이디
   * @param commentId   질문 아이디
   */
  deleteComment: function (questionId, commentId){
    this.socket.emit('deleteComment', {questionId: questionId, commentId: commentId});
  },
  /**
   * 답변 입력 이벤트
   * @param answerId  답변 아이디
   */
  insertAnswer: function (questionId, answerId){
    this.socket.emit('insertAnswer', {questionId: questionId, answerId: answerId});
  },
  /**
   * 답변 삭제 이벤트
   * @param questionId  질문 아이디
   * @param answerId    답변 아이디
   */
  deleteAnswer: function (questionId, answerId){
    this.socket.emit('deleteAnswer', {questionId: questionId, answerId: answerId});
  },
  /**
   * 찹 입력 이벤트
   * @param questionId  질문 아이디
   * @param answerId    답변 아이디
   */
  insertChop: function (questionId, answerId){
    this.socket.emit('insertChop', {questionId: questionId, answerId: answerId});
  },
  /**
   * 찹 삭제 이벤트
   * @param answerId    답변 아이디
   */
  deleteChop: function (answerId){
    this.socket.emit('deleteChop', {answerId: answerId});
  },
  /**
   * 질문 좋아요 입력 이벤트
   * @param questionId  질문 아이디
   */
  insertQuestionLike: function (questionId){
    this.socket.emit('insertQuestionLike', {questionId: questionId});
  },
  /**
   * 질문 좋아요 삭제 이벤트
   * @param questionId  질문 아이디
   * @param answerId    답변 아이디
   */
  deleteQuestionLike: function (questionId){
    this.socket.emit('deleteQuestionLike', {questionId: questionId});
  }
};


function setting() {
  $('.set').hide();
  $('.setting').show();
}


/**
 * [formatDate description]
 * Date 객체를 받아서 날짜 포맷을 만들어서 리턴함
 * @param  {Date} date Date 객체
 * @return {String}      날짜 포맷을 만들어서 리턴
 */
function formatDate(date) {
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  var day = date.getDate();

  return year + '.' + month + '.' + day;
}

/**
 * [formatAMPM description]
 * Date 객체를 받아서 am pm 시간을 만들어서 리턴!
 * @param  {Date} date Date 객체
 * @return {String}      AM PM 시간 문자열
 */
function formatAMPM(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = ampm + hours + ':' + minutes;
  return strTime;
}
/**
 * [getCreatedTime description]
 * db의 게시 일자를 받아서 게시 일자를 만들어서 돌려줌
 * @param  {Date} date    Date 객체
 * @return {String}      게시 일자를 돌려줌
 */
function getCreatedTime(date){
  if(!date)
    return date;
  else{
    var postDate = new Date(date);          //게시물 날짜
    var postFormat = formatDate(postDate);

    var today = new Date();                 //오늘
    var todayFormat = formatDate(today);    //yyyy-mm-dd

    var yesterday = new Date();
    yesterday.setDate(today.getDate()-1);   //어제 날짜로 셋팅
    yesterdayFormat =  formatDate(yesterday); //yyyy-mm-dd

    var created_time = null;

    if(postFormat === todayFormat){         //게시 일자가 오늘인 경우
      var timelapse = (today - postDate)/1000;  //(오늘 - 게시일)초단위
      if(timelapse < 60){                 //게시한지 1분이내인 경우 '방금막 으로 표기'
        created_time = '방금 막';
      }else if(timelapse/60 < 60){
        created_time = parseInt(timelapse/60, 10) + '분전';
      }else{
        created_time = parseInt(timelapse/60/60, 10) + '시간전';
      }
    }else if(postFormat === yesterdayFormat){        //게시 일자가 어제인 경우
      created_time = '어제';
    }else{                                  //게시 일자가 어제 이전인 경우
      var strTime = formatAMPM(postDate);
      created_time = postFormat + ' ' + strTime;
    }

    return created_time;
  }
}
function jqXHRChecker(jqXHR){
  if(jqXHR.getResponseHeader('X-Auth-Required')) {
    location.href = jqXHR.responseJSON;
    return false;
  }
  return true;
}