var mongoose = require('mongoose');
var dc = require('../lib/dateConverter');

//알림 스키마
var NotificationSchema = exports.NotificationSchema = new mongoose.Schema({
  qid: Number,  //질문 아이디
  ruid: Number, //수신자 유저 아이디
  suid: Number, //발송자 유저 아이디
  snik: String, //발송자 닉네임
  spsu: String, //발송자 프로필 이미지 URL
  surl: String, //발송자 user URL
  cdate: Date,  //작성일
  ctnt: String, //내용
  own: Boolean,  //수신자 소유 게시물 여부
  oid: Number,  //소유자 유저 아이디
  onik: String,  //소유자 닉네임
  ourl: String, //소유자 user URL
  evt: Number,  //이벤트 아이디
  qcid: Number, //질문 댓글 아이디
  qlid: Number, //질문 좋아요 아이디
  alid: Number, //답변 좋아요 아이디
  aid: Number, //답변 아이디
  push: Boolean,  //푸시 여부
  seen: {type: Boolean, default: false}//조회 여부

  //~님이 내 질문에 댓글을 남겼습니다
  //~님도 질문에 댓글을 남겼습니다
  //~님이 내 답변에 좋아요를 남겼습니다
  //~님이 내 답변을 담았습니다
  //~님이 댓글에서 회원님을 언급했습니다
});

NotificationSchema.set(function (content){
  if(content.length > ENUMS.LIMIT.NOTIFICATION_CONTENT)
    this.ctnt = ctnt.substring(0, LIMIT.NOTIFICATION_CONTENT);
  else
    this.ctnt = content;
});