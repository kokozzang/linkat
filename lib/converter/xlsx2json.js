var XLSX = require('node-xlsx');

//new Xlsx2Json('member').convert(function (err, json) {
//	if (err) return console.error('[err] ', err);
//
//	console.log(json);
//});

function Xlsx2Json(source, schema) {
	this.source = source;
	this.schema = schema;
	this.DEFAULT_PATH = __dirname + '/../../data/';
}

Xlsx2Json.prototype.convert = function (callback) {
	var self = this;

	self._readXlsx(function (error, sheets) {
		if (error) return callback(error);
		if (!sheets) return callback(null, {});

		var json = self._createJson(sheets);
		return callback(null, json);
	});
};

/**
 * xlsx에서 데이터를 읽어오는 함수
 * @param callback
 * @private
 */
Xlsx2Json.prototype._readXlsx = function (callback){
	var self = this;
	var source = self.source;
	if (!source) return callback('not input source');

	if (-1 === source.indexOf('/')) {
		source = self.DEFAULT_PATH + source;
	}
	if (-1 === source.indexOf('.xlsx')) {
		source = source + '.xlsx';
	}

	var xlsxData = XLSX.parse(source);
	var sheets = (xlsxData || {}).worksheets || [];
	if (0 === sheets.length) {
		return callback('no data');
	}
	return callback(null, sheets);
};

/**
 * 쉬트에 있는 데이터를 JSON 데이터로 변경하는 함수
 * @param sheets 엑셀의 쉬트 정보들
 * @returns {*}
 * @private
 */
Xlsx2Json.prototype._createJson = function (sheets) {
	if (!sheets) { return callback('no sheet data'); }

	var json = {};
	sheets.forEach(function (sheet) {
		json[sheet.name] = [];
		var data = sheet.data;
		if (!data || 0 === data.length) {
			return ;
		}

		var keys = [];
		var isFirstRow = true;
		data.forEach(function (row) {
			var rowObj = {};
			row.forEach(function (col, idx) {
				if (!col) { return ; }

				var val = col.value;
				if (isFirstRow) {
					if (0 === val.trim().indexOf('#')) return ;

					keys[idx] = val;
				} else {
					if (!val || !keys[idx]) return ;

					var valType = typeof val;
					if ('number' === valType) {
						val = parseInt(val, 10);
					} else if ('string' === valType) {
						val = val.trim();
					}

					rowObj[keys[idx]] = val;
				}
			});

			if (!isFirstRow) {
				json[sheet.name].push(rowObj);
			} else {
				isFirstRow = false;
			}
		});
	});
	return json;
}
