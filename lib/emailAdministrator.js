var mailer = require('nodemailer');
var async = require('async');
var transport = mailer.createTransport('SMTP', {
  service: 'Gmail',
  auth: {
    user: 'chopsteralarm@gmail.com',
    pass: 'ckq!@tmxj'
  }
});

module.exports.sendEmail = sendEmail;



/**
 *
 * @param title : 보내는 메일의 주소
 * @param message : 메일의 내용(html의 형태)
 * @param recipients : 메일 받을 사람의 이메일 주소(배열의 형태)
 */
function sendEmail(title, message, recipients, callback) {
  async.each(recipients, function ( address, cb) {
    var mailOptions = {
      from: 'Chopster <chopsteralarm@gmail.com>',
      to: '<' + address + '>',
      subject: title,
      html: message
    };
    transport.sendMail(mailOptions, function (err, result) {
      if (err) {
        return cb(err, null);
      }
      else {
        return cb(null, result);
      }
    });
  }, function (err, results) {
    if (err) {
      return callback(err, null);
    }
    else {
      return callback(null, results);
    }
  });
}