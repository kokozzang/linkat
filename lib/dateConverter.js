var moment = require('moment');

module.exports.fromNow = fromNow;      // 회원 정보 입력

/**
 * [formatDate description]
 * Date 객체를 받아서 날짜 포맷을 만들어서 리턴함
 * @param  {Date} date Date 객체
 * @return {String}      날짜 포맷을 만들어서 리턴
 */
function formatDate(date) {
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  var day = date.getDate();

  return year + '.' + month + '.' + day;
}

/**
 * [formatAMPM description]
 * Date 객체를 받아서 am pm 시간을 만들어서 리턴!
 * @param  {Date} date Date 객체
 * @return {String}      AM PM 시간 문자열
 */
function formatAMPM(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = ampm + hours + ':' + minutes;
  return strTime;
}
/**
 * [fromNow description]
 * db의 게시 일자를 받아서 게시 일자를 만들어서 돌려줌
 * @param  {Date} date    Date 객체
 * @return {String}      게시 일자를 돌려줌
 */
function fromNow(date){
  var postDate = new Date(date);          //게시물 날짜
  var postFormat = formatDate(postDate);

  var today = new Date();                 //오늘
  var todayFormat = formatDate(today);    //yyyy-mm-dd

  var created_time = null;
  moment.locale('ko');

  if(postFormat === todayFormat)         //게시 일자가 오늘인 경우
    created_time = moment(postDate).fromNow();
  else  //게시 일자가 어제인 경우
    created_time = moment(postDate).calendar();

  return created_time;
 }
//function fromNow(date){
//  if(!date)
//    return date;
//  else{
//    var postDate = new Date(date);          //게시물 날짜
//    var postFormat = formatDate(postDate);
//
//    var today = new Date();                 //오늘
//    var todayFormat = formatDate(today);    //yyyy-mm-dd
//
//    var yesterday = new Date();
//    yesterday.setDate(today.getDate()-1);   //어제 날짜로 셋팅
//    yesterdayFormat =  formatDate(yesterday); //yyyy-mm-dd
//
//    var created_time = null;
//
//    if(postFormat === todayFormat){         //게시 일자가 오늘인 경우
//      var timelapse = (today - postDate)/1000;  //(오늘 - 게시일)초단위
//      if(timelapse < 60){                 //게시한지 1분이내인 경우 '방금막 으로 표기'
//        created_time = '방금 막';
//      }else if(timelapse/60 < 60){
//        created_time = parseInt(timelapse/60, 10) + '분전';
//      }else{
//        created_time = parseInt(timelapse/60/60, 10) + '시간전';
//      }
//    }else if(postFormat === yesterdayFormat){        //게시 일자가 어제인 경우
//      created_time = '어제';
//    }else{                                  //게시 일자가 어제 이전인 경우
//      var strTime = formatAMPM(postDate);
//      created_time = postFormat + ' ' + strTime;
//    }
//
//    return created_time;
//  }
//}