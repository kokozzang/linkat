var _ = require('underscore');
var notiService = require('../services/notification_service');
var dc = require('../lib/dateConverter');
var async = require('async');
var emailAdiministerator = require('../lib/emailAdministrator');

module.exports.init = init;

var connectingUsers = {}; //접속 중인 유저
var questionRooms = {};  //질문 룸

function init (io, sessionStore){
  io.enable('browser client minification'); // send minified client
  io.enable('browser client etag'); // apply etag caching logic based on version number
  io.enable('browser client gzip'); // gzip the file
  io.set('log level', 1); //로그 줄이기
  io.set('transports', [
    'websocket'
    , 'flashsocket'
    , 'htmlfile'
    , 'xhr-polling'
    , 'jsonp-polling'
  ]);
  io.set('heartbeat timeout', 10);
  io.set('heartbeat interval', 4);
  io.set('authorization', function(handshake, accept){
    if (!handshake.headers.cookie) {    //세션 쿠키가 없을 때
      return accept('Session cookie required.', false);
    }
    var SESSION_CONFIG = CHOPSTER_CONFIG.SESSION;
    //세션 쿠키 파싱
    handshake.cookie = require('cookie').parse(handshake.headers.cookie);
    //signedCookie 파싱해서 세션 아이디 저장
//    handshake.sessionID = require('cookie-parser').signedCookie(handshake.cookie['apink.sid'], 'sid');
    handshake.sessionID = require('cookie-parser').signedCookie(handshake.cookie[SESSION_CONFIG.KEY], SESSION_CONFIG.SECRET);
    //세션 저장소에서 세션 정보를 가져와서 로그인 한 경우만 소켓을 생성함
    sessionStore.get(handshake.sessionID, function(err, session){
      if (err) {
        return accept('Error in session store.', false);
      } else if (!session) {
        return accept('Session not found.', false);
      } else if (!_.has(session.passport, 'user')) {//로그인 한 유저 인지 확인
        return accept('not login user.', false);  //소켓 생성 안함
      }

      handshake.session = session;  //소켓에 세션 정보 담기
      console.log('handshake.session', handshake.session);
      return accept(null, true);
    });

  });
  //소켓 커넥션
  io.sockets.on('connection', function(socket){
    var handshake = socket.handshake;
    var userId = handshake.session.passport.user.id;  //유저 아이디

    console.log('A socket with sessionID ' + handshake.sessionID + ' connected.');
    //handshake.sessionId: 세션 아이디 (암호화 되어 있음)

    socket.set('userId', userId, function() {
      if(connectingUsers[userId] === undefined){  //현재 접속자 룸
        connectingUsers[userId] = new Object();
      }
      connectingUsers[userId].socket_id = socket.id;
      console.log('connectingUsers', connectingUsers);

      notiService.selectNotificationCount(userId, function (err, notificationCount){
        if(err){
          console.error('getNotificationCount', err);
          return;
        }

        socket.emit('notificationCount', notificationCount);
      });
    });

    socket.on('viewQuestion', function (data){
      var questionRoom = data.questionId;  //질문 아이디를 룸의 아이디로 함
      socket.join(questionRoom);  //해당 질문 룸에 join
    });//end of socket.on('viewQuestion')

    //질문 상세보기 이벤트
    socket.on('viewQuestion', function (data){
      var questionRoom = data.questionId;  //질문 아이디를 룸의 아이디로 함
      socket.join(questionRoom);  //해당 질문 룸에 join
    });//end of socket.on('viewQuestion')
    //질문 상세보기 닫기 이벤트
    socket.on('exitQuestion', function () {
      socket.get('questionRoom', function (err, questionRoom) {
        socket.leave(questionRoom);
        if(err) throw err;

        if(questionRoom !== undefined && questionRooms[questionRoom] !== undefined){
          socket.get('userId', function (err, userId) {
            /*console.log('A socket with sessionID '+handshake.sessionID+' leave room.');
            console.log('userId' + userId + ' has been leave room');
            // 여기에 방을 나갔다는 메세지를 broad cast 하기
            if (userId !== undefined) {
              if (questionRooms[questionRoom].socket_ids !== undefined
                && questionRooms[questionRoom].socket_ids[userId] !== undefined)
                delete questionRooms[questionRoom].socket_ids[userId];
              if (_.isEmpty(questionRooms[questionRoom].socket_ids))
                delete questionRooms[questionRoom];
            }*/
          });
        }
      }); //get
    });//end of socket.on('exitQuestion')

    //댓글 입력 이벤트
    socket.on('insertComment', function (data){
      var questionId = data.questionId;
      var commentId = data.commentId;

      notiService.selectCommentAndUserIdsForCommentNotification(commentId, function (err, result){
        if(err) {
          console.error('insertComment', err);
          return;
        }
        var comment = result.comment;
        var commentEmitData = {
          commentId: comment.id,
          nickname: comment.nickname,
          content: comment.content,
          createdDate: dc.fromNow(comment.created_date)
        };

        io.sockets.in(questionId).emit('commentInserted', commentEmitData);
        if(result.userIdsAndCount !== null) {//알림대상이 있는 경우
          var userIdsAndCount = result.userIdsAndCount; //실시간 알림 받을 유저 리스트
          console.log('userIdsAndCount', userIdsAndCount);
          var userIds = [];
          for (var i = 0; i < userIdsAndCount.length; i++) {
            userIds.push(userIdsAndCount[i]._id);
            if (_.has(connectingUsers, userIdsAndCount[i]._id))
              io.sockets.socket(connectingUsers[userIdsAndCount[i]._id].socket_id).emit('notificationCount', userIdsAndCount[i].cnt);
          }

          if(userIds[0] !== undefined) {
            notiService.getEmailAddress(userIds, function (err, addressSet) {
              if(err) {
                console.log('err', err)
                return;
              };
              var title = commentEmitData.nickname + '님 께서 댓글을 달았습니다.';
              var html  = '댓글 내용 : ' + commentEmitData.content;
              async.each(addressSet, function (address, callback) {
                emailAdiministerator.sendEmail (title, html, [address.email], function (err, result) {
                  if(err)
                    callback(err);
                  callback();
                });
              }, function (err) {
                if(err) {
                  console.log('err', err);
                  return;
                }
              });
            });
          }
        }
      });
    });//end of socket.on('insertComment')
    //댓글 삭제 이벤트
    socket.on('deleteComment', function (data){
      io.sockets.in(data.questionId).emit('commentDeleted', data.commentId);
    });//end of socket.on('deleteComment')

    //질문 좋아요 이벤트
    socket.on('insertQuestionLike', function (data){
      notiService.selectUserIdsForQuestionLikeNotification (data.questionId, userId, function (err, result){
        var err = new Error();
        if(err) {
          console.log('g');
          console.error('insertQuestionLike', err);
          return;
        }
        //좋아요 이미지 처리
        var likeResult = {
          questionId: data.questionId,
          likeCount: result.questionLikeCount
        };
        socket.emit('questionLikeInserted', likeResult);
        io.sockets.in(data.questionId).emit('updateQuestionLikeCount', likeResult);

        if(!_.isEmpty(result.userIdsAndCount)){//알림대상이 있는 경우
          var userIdsAndCount = result.userIdsAndCount;
          //알림대상 (알림 카운트 emit)
          var userIds = [];
          for(var i = 0; i < userIdsAndCount.length; i++) {
            userIds.push(userIdsAndCount[i]._id);
            if(_.has(connectingUsers, userIdsAndCount[i]._id))
              io.sockets.socket(connectingUsers[userIdsAndCount[i]._id].socket_id).emit('notificationCount', userIdsAndCount[i].cnt);
          }
          if(userIds[0] !== undefined) {
            notiService.getEmailAddress(userIds, function (err, addressSet) {
              if(err) {
                console.log('err', err);
                return;
              };
              var title = result.userNickname[0].nickname + '님 께서 회원님의 질문을 좋아합니다.';
              var html  = '그... 그렇다구요.';
              async.each(addressSet, function (address, callback) {
                emailAdiministerator.sendEmail (title, html, [address.email], function (err, result) {
                  if(err)
                    callback(err);
                  callback();
                });
              }, function (err) {
                if(err) {
                  console.log('err', err);
                  return;
                }
              });
            });
          }
        }
      });
    });//end of socket.on('insertQuestionLike')
    //좋아요 취소 이벤트
    socket.on('deleteQuestionLike', function (data){
      notiService.selectQuestionLikeCount(data.questionId, function (err, likeCount) {
        if (err) {
          console.error('deleteQuestionLike', err);
          return;
        }
        else {
          var likeResult = {
            questionId: data.questionId,
            likeCount: likeCount
          };
          socket.emit('questionLikeDeleted', likeResult);
          io.sockets.in(data.questionId).emit('updateQuestionLikeCount', likeResult);
        }
      });
    });//end of socket.on('deleteQuestionLike')

    //답변 입력 이벤트
    socket.on('insertAnswer', function (data) {
      //답변 내용과 알림 대상을 가져옴
      //result{answer:답변내용, userIds:알림대상}
      notiService.selectAnswerAndUserIdsForAnswerNotification(data.questionId, data.answerId, userId, function (err, result){
        if(err) {
          console.error('insertAnswer', err);
          return;
        }
        //해당 질문 룸에 있는 사용자에게 답변 내용 emit
        io.sockets.in(result.answer.question_id).emit('answerInserted', result.answer);
        console.log('result.answer', result.answer[0]);

        if(!_.isEmpty(result.userIdsAndCount)) {//알림대상이 있는 경우
          var userIdsAndCount = result.userIdsAndCount; //실시간 알림 받을 유저 리스트
          var userIds = [];
          for (var i = 0; i < userIdsAndCount.length; i++) {
            userIds.push(userIdsAndCount[i]._id);
            if (_.has(connectingUsers, userIdsAndCount[i]._id))
              io.sockets.socket(connectingUsers[userIdsAndCount[i]._id].socket_id).emit('notificationCount', userIdsAndCount[i].cnt);
          }
          if(userIds[0] !== undefined) {
            notiService.getEmailAddress(userIds, function (err, addressSet) {
              if(err) {
                console.log('err', err);
                return;
              }
              var title = result.answer.nickname + '님 께서 회원님의 질문에 답변을 달았습니다.';
              var html  = '그... 그렇다구요.';
              async.each(addressSet, function (address, callback) {
                emailAdiministerator.sendEmail (title, html, [address.email], function (err, result) {
                  if(err)
                    callback(err);
                  callback();
                });
              }, function (err) {
                if(err) {
                  console.log('err', err);
                  return;
                }
              });
            });
          }
        }
      });
    });//end of socket.on('insertComment')
    //답변 삭제 이벤트
    socket.on('deleteAnswer', function (data){
      io.sockets.in(data.questionId).emit('answerDeleted', data.answerId);
    });//end of socket.on('deleteAnswer')

    //찹 입력 이벤트
    socket.on('insertChop', function (data) {
      //알림 대상을 가져옴
      notiService.selectUserIdsAndCountForChopNotification(data.questionId, data.answerId, userId, function (err, userIdsAndCount){
        if(err) {
          console.error('insertChop', err);
          return;
        }

        //해당 질문 룸에 있는 사용자에게 찹 emit
        io.sockets.in(data.question_id).emit('chopInserted', data.answerId);

        if(!_.isEmpty(userIdsAndCount)) {//알림대상이 있는 경우
          for (var i = 0; i < userIdsAndCount.length; i++) {
            if (_.has(connectingUsers, userIdsAndCount[i]._id))
              io.sockets.socket(connectingUsers[userIdsAndCount[i]._id].socket_id).emit('notificationCount', userIdsAndCount[i].cnt);
          }
        }
      });
    });//end of socket.on('insertChop')

    //찹 삭제 이벤트
    socket.on('deleteChop', function (data){
      socket.emit('chopDeleted', data.answerId);
    });//end of socket.on('deleteAnswer')

    socket.on('disconnect', function () {
      console.log('socket.on disconnect');
      socket.get('questionRoom', function (err, questionRoom) {
        if(err) throw err;

        if(questionRoom !== undefined && questionRooms[questionRoom] !== undefined){
          socket.get('userId', function (err, userId) {
            console.log('A socket with sessionID '+handshake.sessionID+' disconnected.');
            console.log('userId' + userId + ' has been disconnected');
            // 여기에 방을 나갔다는 메세지를 broad cast 하기
            if (userId !== undefined) {
              if (questionRooms[questionRoom].socket_ids != undefined
                && questionRooms[questionRoom].socket_ids[userId] !== undefined)
                delete questionRooms[questionRoom].socket_ids[userId];
            }
          });
        }
      }); //get
    });

//    socket.on('disconnect', function () {
//      console.log('A socket with sessionID '+handshake.sessionID+' disconnected.');
////      clearInterval(intervalID);
//    });


//    socket.on('disconnect', function (){
//      console.log('sessionId', socket.sessionId);
//      console.log('disconnect');
//      delete socketIds[socket.sessionId];
//    });
  });

}

function register (socket, socketIds, cnt){
  socket.sessionId = cnt;
//  console.log('sessionId', socket.sessionId);
  socketIds[cnt] = socket.id;
//  console.log('socketIds', socketIds);
}