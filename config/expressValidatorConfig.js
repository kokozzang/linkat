var _ = require('underscore');
var validator = require('validator');
module.exports = {
  errorFormatter: function (param, msg, value) {
    var namespace = param.split('.');
    var root = namespace.shift();
    var formParam = root;

    while (namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
//      console.log({
//        param: formParam,
//        msg: msg,
//        value: value
//      });
    return {
      param: formParam,
      msg: msg,
      value: value
    };
  },
  customValidators: {
    //양의 정수 검사
    isAlphaKorNumeric: function(value) {
      var reg = /^[가-힣a-zA-Z0-9]+$/i;
      if(!reg.test(value))
        return false;
      return true;
    },
    //양의 정수 검사
    isPositiveNumber: function(value) {
      if(!validator.isInt(value))
        return false;
      if(parseInt(value) < 1)
        return false;
      return true;
    },
    //업로드 이미지 검사
    isImage: function(file) {
      if(file === undefined){
        console.error('파일이 없음.');
        return false;
      }
      //파일 확장자 추출
      var ext = (/[.]/.exec(file.originalFilename))
        ? /[^.]+$/.exec(file.originalFilename)  //있으면 배열로 반환 ext[0]이 확장자
        : undefined;  //확장자가 없으면 undefined
      if(!validator.isIn(file.type, _.values(ENUMS.IMAGE_CONTENT_TYPE))){  //파일 타입 검사
        console.error('허용되지 않은 파일 타입. ' + 'file.type: ' + file.type);
        return false;
      }if(ext === undefined){ // 파일 확장자가 있는지 검사
        console.error('파일 확장자가 없음. ' + 'file.originalfileName: ' + file.originalFilename);
        return false;
      }if(!validator.isIn(ext[0].toLowerCase(), _.keys(ENUMS.IMAGE_CONTENT_TYPE))){ //확장자검사
        console.log('허용되지 않은 파일 확장자. ' + 'ext: ' + ext[0]);
        return false;
      }
      return true;
    }
  }
};