var console = {
//    level: 'silly',
  colorize: 'true'
//    label: 'category one'
};
var file = {
  filename: 'somefile.log'
};
var mongodb = {
  accesslog: {
    collection: 'accesslogs',
    dbUri: 'mongodb://'
      + CHOPSTER_CONFIG.MONGODB.USER + ':'+ CHOPSTER_CONFIG.MONGODB.PASSWORD
      + '@' + CHOPSTER_CONFIG.MONGODB.HOST
      + ':' + CHOPSTER_CONFIG.MONGODB.PORT  + '/' + CHOPSTER_CONFIG.MONGODB.DB
  },
  errorlog: {
    collection: 'errorlogs',
    dbUri: 'mongodb://'
      + CHOPSTER_CONFIG.MONGODB.USER + ':'+ CHOPSTER_CONFIG.MONGODB.PASSWORD
      + '@' + CHOPSTER_CONFIG.MONGODB.HOST
      + ':' + CHOPSTER_CONFIG.MONGODB.PORT  + '/' + CHOPSTER_CONFIG.MONGODB.DB
  }
};


module.exports.console = console;
module.exports.mongodb = mongodb;
module.exports.file = file;
