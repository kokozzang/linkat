var models = require('../db').models;

var User = models.User;

exports.init = function () {
	User.checkAdminUser(function (err) {
		if (err) return console.error('[ADMIN]', err);
	});
};