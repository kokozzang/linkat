// publics
var http = require('http');
var yamlish = require('yamlish');


// shortcuts
var res = http.ServerResponse.prototype;

/**
 * {result: false, reason: reason} 형태로 응답을 내보냄.
 * reason이 Sorry일 때는 디비에 기록하지 않음
 * reason이 Error의 종류일 때는 디비에 기록함
 *
 * Examples:
 *
 *     res.err(Sorry('uh-oh'));
 *     res.err('uh-oh'); - deprecated
 *     res.err(err);
 *     res.err(new LogicError('Invalid CoachId'));
 *     res.err(new LogicError('Invalid CoachId'), new Date());
 *
 * @param {Array} of {String|Error} reasons
 * @return {ServerResponse}
 * @api public
 */

res.err = function err(reason, until) {
	var self = this;
	var response = {
		result: false,
		until: until
	};

	function store(error) {
		if (error instanceof Error) {
			return error.message;
		}
		return error;
	}

	if (!Array.isArray(reason)) {
		reason = [reason];
	}
	reason = reason.map(store).join('\n\n');
	response.reason = reason;

	return this.json(response);
};

/**
 *
 * Examples:
 *
 *     res.json(null);
 *     res.json({ user: 'tj' });
 *     res.json('oh noes!');
 *
 * @param {Mixed} obj
 * @param {Object} headers
 * @return {ServerResponse}
 * @api public
 */
// 응답 정보에 {result: true}를 덧붙여주는 일반적인 헬퍼
res.ok = function ok(data) {
	data = data || {};
	data.result = true;
	return this.json(data);
};

// 응답 정보를 YAML로 변환해주는 일반적인 헬퍼
res.yaml = function yaml(data) {
	this.charset = 'UTF-8';
	this.header('Content-Type', 'application/yaml');
	return this.end(yamlish.encode(JSON.clone(data)));
};
