var fs = require('fs');

var authService = require('../services/auth_service');
var async = require('async');
var request = require('request');
var authUtil = require('../utils/auth_util');

/**
 * oauth 디렉토리 내의 파일들의 delegate 메소드를 실행
 * @param app
 * @param oauthConfig
 */
module.exports = function(app, passport){
  init(passport);

  var oauths = fs.readdirSync(__dirname);
  async.each(oauths, function(oauth, callback) {
    if ('index.js' === oauth) return ;
    console.log('oauths/'+ oauth);
    require(__dirname + '/' + oauth)(app, passport);
    callback();
  }, function(err){
    // if any of the file processing produced an error, err would equal that error
    if( err ) {

    }
  });

  app.get('/facebookTest', authUtil.accessTokenVerifier);



  app.get('/login', function (req, res){
    res.render('login', null);
  });
  app.get('/logout', function(req, res){
    //
    // passport 에서 지원하는 logout 메소드이다.
    // req.session.passport 의 정보를 삭제한다.
    //
    req.logout();
    res.redirect('/');
  });
};

function init(passport) {
  passport.serializeUser(function(user, done) {
    console.log('serializeUser', user);
    var sessionVal = {
      id: user.id,
      views: []
    };
    done(null, sessionVal);
  });

  passport.deserializeUser(function deserializerUserCallback(user, done) {
    authService.getUserInfoById(user.id, function (err, result){
      if (err) { return done(err, null); }
      console.log('deserial');
      done(null, result[0]);
    });
  });
}