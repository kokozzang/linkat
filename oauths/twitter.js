var TwitterStrategy = require('passport-twitter').Strategy;
var authService = require('../services/auth_service');

module.exports = function (app, passport) {
  useStrategy(passport);

  app.get('/auth/twitter', passport.authenticate('twitter'));

  app.get('/auth/twitter/callback', function (req, res, next){
    passport.authenticate('twitter', function (err, user, info){
      if(err) return next(err);
      if(!user) return res.json({result: false});  //로그인 실패, 아이디 또는 패스워드

      req.logIn(user, function (err){
        if(err) return next(err);
//        console.log('req.user.state_id', req.user.state_id);
        //로그인 성공시
        switch (req.user.state_id){
          case ENUMS.USER_STATE.ACTIVE:   //활성
            return res.redirect('/');
          case ENUMS.USER_STATE.PENDING:  //회원정보미입력
            return res.redirect('/user/registerInfo');
          case ENUMS.USER_STATE.UNCERTIFIED:  //미인증
            return res.redirect('/');
          case ENUMS.USER_STATE.SUSPENDED:  //계정정지
            return res.redirect('/');
          case ENUMS.USER_STATE.DEACTIVATED:  //비활성화
            return res.redirect('/');
          default : //기타 모든 경우 로그인 실패 등
            return res.redirect('/');
        }
      });
    })(req, res, next);
  });

};

function useStrategy(passport) {
  passport.use(new TwitterStrategy({
    consumerKey: OAUTH_CONFIG.TWITTER.TWITTER_CONSUMER_KEY,
    consumerSecret: OAUTH_CONFIG.TWITTER.TWITTER_CONSUMER_SECRET,
    callbackURL: OAUTH_CONFIG.TWITTER.CALLBACK_URL
  }, function(token, tokenSecret, profile, done) {
//    console.log('token', token);
//    console.log('tokenSecret', tokenSecret);
//    console.log('profile._json', profile._json);

    authService.getUserInfoByOAuth(ENUMS.OAUTH.TWITTER, profile._json.id, function (err, userInfo) {
      if (err) {
        return done(err, false);
      }

      if (userInfo.length > 0) {  //기존 사용자
        return done(null, userInfo[0]);
      } else {//회원 가입의 경우
        var user = {
          email: null,       //이메일
          password: null, //패스워드
          join_date: new Date(),  //가입일시
          user_url: null,     //사용자 url
          nickname: null,     //닉네임
          grade_id: null,     //등급
          gender_id: null,    //성별
          birth: null         //생일
        };
        var profilePhoto = {
          user_id: null,
          small_url: profile.profile_image_url,
          medium_url: profile.profile_image_url,
          large_url: profile.profile_image_url
        };
        var oauth = {
          oauth_id: ENUMS.OAUTH.TWITTER,
          oauth_account: profile._json.id
        };

        authService.registerUser(user, profilePhoto, oauth, function (err, userInfo){
          if(err) {
            return done(null, false);
          }else {
            return done(null, userInfo);
          }
        });//end of authService.registerUser

      }
    });

    //return done(null, profile);
  }));
}