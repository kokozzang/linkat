var LocalStrategy = require('passport-local').Strategy;
var authService = require('../services/auth_service');
var crypto = require('crypto');
var _ = require('underscore');
var authValidator = require('../utils/validators/auth_validator');

module.exports = function (app, passport) {
  useStrategy(passport);
  app.post('/auth/local/signUp', function (req, res, handleError) {
    var email = req.body.email;
    var pass = req.body.password;
    var nickname = req.body.nickname;
    var userUrl = req.body.url;
    var birth = req.body.birth;
    var gender = req.body.gender;
    var salt = (Math.round((new Date().valueOf() * Math.random()))).toString().substring(0, 10);

    //add: 유효성검사
    var user = {
      email: email,       //이메일
      password: createPassword(pass, salt), //패스워드
      salt: salt,
      join_date: new Date(),  //가입일시
      nickname: nickname,     //닉네임
      user_url: userUrl,
      birth: new Date(birth),
      gender_id: gender,
      grade_id: ENUMS.USER_GRADE.BRONZE
    };

    var profilePhoto = {
      user_id: ENUMS.DEFAULT_AVATAR_URL,
      small_url: ENUMS.DEFAULT_AVATAR_URL,
      medium_url: ENUMS.DEFAULT_AVATAR_URL,
      large_url: ENUMS.DEFAULT_AVATAR_URL
    };
    var oauth = {
      oauth_id: ENUMS.OAUTH.LOCAL,
      oauth_account: null
    };

    authService.registerUser(user, profilePhoto, oauth, function (err, result) {
      if (err) {
        return handleError(err);
      } else {
        passport.authenticate('local')(req, res, function () {
          //res.redirect('/user/registerInfo');
          return res.send(ENUMS.HTTP_STATUS.OK.CODE);
        });
      }
    });//end of authService.registerUser
  });//end of app.post('/auth/local/signIn')



  app.post('/auth/local/signIn', function (req, res, next){
    var redirectUrl = req.body.redirectUrl || null;
    var email = req.body.email;

    //add: 유효성 검사 해야됨ㅋ
    if (email === '')
      return res.redirect('/');

    passport.authenticate('local', function (err, user, info) {
      if (err) return next(err);
      if (!user)
        return res.send(ENUMS.HTTP_STATUS.UNAUTHORIZED.CODE,
        {message: ENUMS.HTTP_STATUS.UNAUTHORIZED.MESSAGE});  //로그인 실패, 아이디 또는 패스워드


      req.logIn(user, function (err) {
        if (err) return next(err);
        var result = {
          result: true,
          url   : '/'
        };

        //로그인 성공시
        switch (req.user.state_id) {
          case ENUMS.USER_STATE.ACTIVE:   //활성
            console.log('active');
            return res.send(ENUMS.HTTP_STATUS.OK.CODE);
          case ENUMS.USER_STATE.PENDING:  //회원정보미입력
            console.log('PENDING');
            result.url = '/user/registerInfo';
            return res.send(ENUMS.HTTP_STATUS.OK.CODE);
          case ENUMS.USER_STATE.UNCERTIFIED:  //미인증
            console.log('UNCERTIFIED');
            result.url = redirectUrl;
            return res.send(ENUMS.HTTP_STATUS.OK.CODE);
          case ENUMS.USER_STATE.SUSPENDED:  //계정정지
            console.log('SUSPENDED');
            return res.send(ENUMS.HTTP_STATUS.OK.CODE);
          case ENUMS.USER_STATE.DEACTIVATED:  //비활성화
            console.log('DEACTIVATED');
            result.url = '/';
            return res.send(ENUMS.HTTP_STATUS.OK.CODE);
          default : //기타 모든 경우 로그인 실패 등
            console.log('UNAUTHORIZED');
            return res.send(ENUMS.UNAUTHORIZED.CODE,
              {message: ENUMS.UNAUTHORIZED.MESSAGE});  //기타 모든 경우 로그인 실패 등
        }
      });
    })(req, res, next);
  });
};

/**
 * email, password로 DB에서 사용자 정보를 가져옴
 * @param passport
 */
function useStrategy (passport) {
  passport.use(new LocalStrategy ({
    usernameField: OAUTH_CONFIG.LOCAL.USER_NAME_FIELD,
    passwordField: OAUTH_CONFIG.LOCAL.PASSWORD_FILED,
    passReqToCallback: true
  }, function(req, email, password, done) {
    authService.getSalt(email, function (err, user) {
      if(err)
        return done(null, false);
      if(_.isUndefined(user))
        return done(null, false);

      authService.getUserInfoByEmailAndPassword(email, createPassword(password, user.salt), function (err, userInfo) {
        if (err) {
          return done(err, false);
        }
        if (userInfo.length > 0) {
          return done(null, userInfo[0]);
        } else {//로그인 실패, 아이디 또는 패스워드가 틀림
          return done(null, false);
        }
      });
    })
  }));  //end of userStrategy
}

function createPassword(hashPass, salt) {
  var pass = hashPass;
  for(var i = 0; i < 100; i++) {
    pass = crypto.createHash('sha256').update(pass+salt).digest('hex');
  }
  return pass;
}
