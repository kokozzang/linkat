var FacebookStrategy = require('passport-facebook').Strategy;
var request = require('request');
var async = require('async');
var authService = require('../services/auth_service');
var authUtil = require('../utils/auth_util');


module.exports = function (app, passport) {
  //useStrategy(passport);
  app.post('/auth/facebook', authUtil.accessTokenVerifier, function (req, res, handlError){
    var accessToken = req.headers['x-auth-token'];
    var profile = req.profile;

    authService.getUserInfoByOAuth(ENUMS.OAUTH.FACEBOOK, profile.id, function (err, userInfo) {
      if (err) { return handlError(err); }

      //userInfo {id, nickname, state_id, change_date, end_date}
      if (userInfo.length > 0) {  //기존 사용자
        console.log('기존 사용자', userInfo);
        return res.send(ENUMS.HTTP_STATUS.OK.CODE, userInfo[0]);
      }

      //기존 사용자가 아닌경우
      async.waterfall([
        function requestUserInfo(callback){
          var query = 'me?fields=id,picture.height(400),name,email&access_token=';
          var method = 'GET';
          var url = 'https://graph.facebook.com/' + query + accessToken;

          var options = {
            method: method,
            url: url
          };
          request(options, function (err, response, body){
            if (err) { return callback(err, null); }

            var male = ENUMS.USER_GENDER.MALE;
            var gender_id = (profile.gender === ENUMS.USER_GENDER_NAMES[male].toLowerCase())
              ? ENUMS.USER_GENDER.MALE
              : ENUMS.USER_GENDER.FEMALE;

            var facebookInfo = JSON.parse(body);
            var userInfo = {
              profile_url: facebookInfo.picture.data.url,
              gender_id: gender_id,
              email: facebookInfo.email || null
            }
            console.log('facebookInfo', facebookInfo);
            return callback(null, userInfo);
          });
        },
        function registerUser (userInfo, callback){
          var user = {
            email: userInfo.email,  //이메일
            password: null, //패스워드
            join_date: new Date(),  //가입일시
            user_url: null,     //사용자 url
            nickname: null,     //닉네임
            grade_id: null,     //등급
            gender_id: userInfo.gender_id,    //성별
            birth: null         //생일
          };
          var profilePhoto = {
            user_id: null,
            small_url: userInfo.profile_url,
            medium_url: userInfo.profile_url,
            large_url: userInfo.profile_url
          };
          var oauth = {
            oauth_id: ENUMS.OAUTH.FACEBOOK,
            oauth_account: profile.id
          };

          authService.registerUser(user, profilePhoto, oauth, function (err, result){
            if(err) {
              return callback(err, null);
            }else {
              return callback(null, result);
            }
          });//end of authService.registerUser
        }
      ],function (err, userInfo){
        if(err)
          return handlError(err);
        return res.send(ENUMS.HTTP_STATUS.OK.CODE, userInfo);
      });
    });
  });
};

function useStrategy(passport) {
  passport.use(new FacebookStrategy ({
    clientID: OAUTH_CONFIG.FACEBOOK.FACEBOOK_APP_ID,
    clientSecret: OAUTH_CONFIG.FACEBOOK.FACEBOOK_APP_SECRET,
    callbackURL: OAUTH_CONFIG.FACEBOOK.CALLBACK_URL
  }, function(accesstoken, refreshToken, profile, done) {
//    console.log('accesstoken', accesstoken);
//    console.log('refreshToken', refreshToken);
//    console.log('profile', profile);


    authService.getUserInfoByOAuth(ENUMS.OAUTH.FACEBOOK, profile._json.id, function (err, userInfo) {
      if (err) { return done(err, false); }

      if (userInfo.length > 0) {  //기존 사용자
        console.log('기존 사용자');
        return done(null, userInfo[0]);
      }
      async.waterfall([
        function requestUserInfo(callback){
          var query = 'me?fields=id,picture.height(600),name&access_token=';
          var method = 'GET';
          var url = 'https://graph.facebook.com/' + query + accesstoken;

          var options = {
            method: method,
            url: url
          };
          request(options, function (err, response, body){
            if (err) { return callback(err, null); }

            var male = ENUMS.USER_GENDER.MALE;
            var gender_id = (profile.gender === ENUMS.USER_GENDER_NAMES[male].toLowerCase())
            ? ENUMS.USER_GENDER.MALE
            : ENUMS.USER_GENDER.FEMALE;

            var facebookInfo = JSON.parse(body);
            var userInfo = {
              profile_url: facebookInfo.picture.data.url,
              gender_id: gender_id
            }
            return callback(null, userInfo);
          });
        },
        function registerUser (userInfo, callback){
          var user = {
            email: null,       //이메일
            password: null, //패스워드
            join_date: new Date(),  //가입일시
            user_url: null,     //사용자 url
            nickname: null,     //닉네임
            grade_id: null,     //등급
            gender_id: userInfo.gender_id,    //성별
            birth: null         //생일
          };
          var profilePhoto = {
            user_id: null,
            small_url: userInfo.profile_url,
            medium_url: userInfo.profile_url,
            large_url: userInfo.profile_url
          };
          var oauth = {
            oauth_id: ENUMS.OAUTH.FACEBOOK,
            oauth_account: profile._json.id
          };

          authService.registerUser(user, profilePhoto, oauth, function (err, result){
            if(err) {
              return callback(err, null);
            }else {
              return callback(null, result);
            }
          });//end of authService.registerUser
        }
      ],function (err, userInfo){
        if(err)
          return done(err, false);
        return done(null, userInfo);
      });
    });
  }));
}