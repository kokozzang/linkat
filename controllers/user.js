var userService = require('../services/user_service');
var accountService = require('../services/account_service');
var validator = require('validator');
var request = require('request');
var uuid = require('node-uuid');
var emailAdministrator = require('../lib/emailAdministrator');
var passportConf = require('../config/passportConfig');
var userValidator = require('../utils/validators/user_validator');
var accountValidator = require('../utils/validators/account_validator');
var authUtil = require('../utils/auth_util');
var crypto = require('crypto');
var gm = require('gm');
var fs = require('fs');
var async = require('async');

module.exports = function (app) {
  app.post('/device', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, accountValidator.insertDevice, insertDevice); //device 추가

  app.post('/user/emailDuplicationCheck', userValidator.emailDuplicationCheck, emailDuplicationCheck); //email 중복검사
  app.post('/user/userUrlDuplicationCheck', userValidator.userUrlDuplicationCheck, userUrlDuplicationCheck); //url 중복검사
  app.post('/user/registerInfo', authUtil.accessTokenVerifier, userValidator.registerInfo, registerInfo);    //회원 정보 입력

  app.get('/account', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, selectAccount);//계정정보 가져오기
  app.put('/account/profilePhoto', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, accountValidator.updateProfilePhoto, updateProfilePhoto);//프로필 이미지 수정
  app.put('/account/email', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, accountValidator.updateEmail, updateEmail);//이메일 수정
  app.put('/account/nickname', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, accountValidator.updateNickname, updateNickname);//닉네임 수정
  app.put('/account/userurl', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, accountValidator.updateUserURL, updateUserURL);//URL 수정
  app.put('/account/introduce', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, accountValidator.updateIntroduce, updateIntroduce);//자기소개 수정
  app.put('/account/notification', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, updateNotification);//알림 설정 수정
  app.put('/account/password', updatePassword);

  app.get('/user/sendAuthEmail', passportConf.ensureAuthenticated, sendAuthEmail); //이메일 인증 메일 전송
  app.get('/user/receiveAuthEmail/:userId/:token', passportConf.ensureAuthenticated, receiveAuthEmail); //이메일에서 타고온 링크

};


function insertDevice (req, res, handleError) {
  console.log('device');
  var device = {
    user_id: req.user.id,
    uuid: req.body.uuid,
    regid: req.body.regid
  };
  console.log('device', device);
  accountService.insertDevice(device, function (err, result) {
    if(err) { return handleError(err);}
    return res.send(ENUMS.HTTP_STATUS.OK.CODE);
  });
}

/**
 * email 중복검사
 * email이 중복되는지 검사하여 사용가능한지 boolean 값으로 응답
 * @param req
 * @param res
 * @param handleError
 */
function emailDuplicationCheck (req, res, handleError){
  var email = req.body.value;  //중복 검사 할 이메일

  userService.emailDuplicationCheck(email , function (err, result){
    if(err) {
      return handleError(err);
    }else{
      if(result[0].isUsing > 0 ){
        return res.send(ENUMS.HTTP_STATUS.OK.CODE, {isValid: false});
      }
      return res.send(ENUMS.HTTP_STATUS.OK.CODE, {isValid: true});
    }
  });
}

/**
 * user url 중복 검사
 * user url 이 중복되는지 검사하여 사용가능한지 boolean 값으로 응답
 * @param req
 * @param res
 * @param handleError
 */
function userUrlDuplicationCheck (req, res, handleError){
  var userUrl = req.body.value;  //중복 검사 할 url

  userService.userUrlDuplicationCheck(userUrl , function (err, result){
    if(err) {
      return handleError(err);
    }else{
      if(result[0].isUsing > 0 ){
        return res.send(ENUMS.HTTP_STATUS.OK.CODE, {isValid: false});
      }
      return res.send(ENUMS.HTTP_STATUS.OK.CODE, {isValid: true});
    }
  });
}

/**
 * 회원정보 입력
 * @param req
 * @param res
 * @param handleError
 */
function registerInfo (req, res, handleError){
  console.log('registerInfo');
  var oauthId = req.profile.id;
  //var salt = (Math.round((new Date().valueOf() * Math.random()))).toString().substring(0, 10);

  userService.getUserIdByOAuth(ENUMS.OAUTH.FACEBOOK, oauthId, function (err, userId) {
    if(err)
      return handleError(err);

    var userInfo = {
      id: userId[0].id,
      nickname: req.body.nickname,
      user_url: req.body.userUrl,
      birth: req.body.birth,
      gender: req.body.gender,
      grade_id: ENUMS.USER_GRADE.BRONZE,
      salt: null,
      password: null
      //password: createPassword(req.body.password, salt) //패스워드
    };

    userService.registerInfo(userInfo, function (err, result){
      if(err)
        return handleError(err);
      return res.send(ENUMS.HTTP_STATUS.OK.CODE, result);
      //return res.redirect('/user/sendAuthEmail');
    });
  });
}


function selectAccount (req, res, handleError) {
  var userId = req.user.id;

  accountService.selectAccount(userId, function (err, account) {
    if(err) { return handleError(err);}
    return res.send(ENUMS.HTTP_STATUS.OK.CODE, {account: account});
  });
}

function updateProfilePhoto (req, res, handleError) {
  var userId = req.user.id;
  var image = req.files.img;


  async.parallel({
    SMALL: function (callback){
      s3ProfileUploader(image, ENUMS.PROFILE_PHOTO_WIDTH['SMALL'], function (err, result){
        if(err)
          return callback(err, null);
        return callback(null, result);
      });
    },
    MEDIUM: function (callback) {
      s3ProfileUploader(image, ENUMS.PROFILE_PHOTO_WIDTH['MEDIUM'], function (err, result){
        if(err)
          return callback(err, null);
        return callback(null, result);
      });
    },
    LARGE: function (callback) {
      s3ProfileUploader(image, ENUMS.PROFILE_PHOTO_WIDTH['LARGE'], function (err, result){
        if(err)
          return callback(err, null);
        return callback(null, result);
      });
    }
  }, function (err, results) {
    if(err)
      return handleError(err);

    var profilePhotos = {
      small_url: results['SMALL'].url,
      medium_url: results['MEDIUM'].url,
      large_url: results['LARGE'].url
    };

    accountService.updateProfilePhoto(userId, profilePhotos, function (err, result) {
      if(err) { return handleError(err);}
      return res.send(ENUMS.HTTP_STATUS.OK.CODE, {
        profile_small_url: profilePhotos.small_url,
        profile_medium_url: profilePhotos.medium_url
      });
    });
  });

}

function updateEmail (req, res, handleError) {
  var userId = req.user.id;
  var email = req.body.email;

  accountService.updateEmail(userId, email, function (err, result) {
    if(err) { return handleError(err);}
    return res.send(ENUMS.HTTP_STATUS.OK.CODE);
  });
}

function updateNickname (req, res, handleError) {
  var userId = req.user.id;
  var nickname = req.body.nickname;

  accountService.updateNickname(userId, nickname, function (err, result) {
    if(err) { return handleError(err);}
    return res.send(ENUMS.HTTP_STATUS.OK.CODE);
  });
}

function updateUserURL (req, res, handleError) {
  var userId = req.user.id;
  var userUrl = req.body.userUrl;

  accountService.updateUserURL(userId, userUrl, function (err, result) {
    if(err) { return handleError(err);}
    return res.send(ENUMS.HTTP_STATUS.OK.CODE);
  });
}

function updateIntroduce (req, res, handleError) {
  var userId = req.user.id;
  var introduce = req.body.introduce;

  accountService.updateIntroduce(userId, introduce, function (err, result) {
    if(err) { return handleError(err);}
    return res.send(ENUMS.HTTP_STATUS.OK.CODE);
  });
}

function updateNotification (req, res, handleError) {
  var userId = req.user.id;

  accountService.updateNotification(userId, function (err, result) {
    if(err) { return handleError(err);}
    return res.send(ENUMS.HTTP_STATUS.OK.CODE);
  });
}

function updatePassword (req, res, handleError) {
  var userId = req.user.id;
  var presentPassword = req.body.presentPassword;
  var inputPassword = req.body.inputPassword;
  accountService.updatePassword(userId, presentPassword, inputPassword, function (err, result) {
    if(err) { return handleError(err);}
    return res.send(ENUMS.HTTP_STATUS.OK.CODE);
    //  res.json(result);
  })
}

function createPassword(hashPass, salt) {
  var pass = hashPass;
  for(var i = 0; i < 100; i++) {
    pass = crypto.createHash('sha256').update(pass+salt).digest('hex');
  }
  return pass;
}



function sendAuthEmail(req, res, handleError) {
  var userId = req.user.id;
  var universalId = uuid.v4({
    msecs: new Date().getTime()
  });

  userService.saveUserToken(userId, universalId, function (err, email) {
    if(err)
      return handleError(err);
    else {
      var title = 'Chopster의 인증코드가 도착했습니다.';
      var html = '<h1>Chopster에 가입한걸 환영합니다.</h1>' +
        '<p><img src="http://cfile24.uf.tistory.com/image/185B493C4F92EB1509535D"></p>' +
        '<a href="http://localhost:3000/user/receiveAuthEmail/' + userId + '/' + universalId +
        '">이메일인증</a>';
      emailAdministrator.sendEmail(title, html, [email], function (err, result) {
        if(err) {
          console.log(err);
        }
        else {
          res.render('sendAuthEmail');
        }
      });
    }
  });
};

function receiveAuthEmail(req, res, handleError) {

  var userId = req.params.userId;
  var token = req.params.token;
  console.log('userId', userId);
  console.log('token', token);
  userService.checkUserToken(userId, token, function (err, nickname) {
    if(err) {
      return handleError(err);
    }
    else {
      if (nickname){
        res.render('welcome', {'userId': nickname});
      }
      else {
        res.render('welcome', {'userId': '인증실패'});
      }
    }
  });
}



function s3ProfileUploader (image, width, callback) {
  var originalFileName = image.originalFilename;

  //파일명 해시
  var sha1 = crypto.createHash('sha1');                    // sha1 해쉬 객체 생성
  var currentDate = (new Date()).valueOf().toString();   // 오늘 날짜 타임스탬프
  var randomNum = Math.random().toString();              // 랜덤 숫자 생성
  var hashedName = sha1.update(originalFileName + currentDate + randomNum)
      .digest('hex') + '.jpg'; //업로드할 파일 이름 생성(해쉬)

  //사이즈별 디렉토리에 해쉬파일명으로 디렉토리 생성
  var folder = 'profile/' + ENUMS.PROFILE_PHOTO_WIDTH_NAMES[width].toLowerCase()  + '/'
    + hashedName.substring(0,2) + '/'
    + hashedName.substring(2,4) + '/'
    + hashedName.substring(4,6);
  var bucket = AWS_CONFIG.S3.BUCKET + '/' + folder;

  var readStream = fs.createReadStream(image.path); //리드 스트림
  var imageSize = {};

  async.waterfall([
    function calSize(cb) {
      gm(readStream).size(function (err, size){
        if(err)
          return cb(err, null);
        var ratio = size.height / size.width;
        imageSize.width = width;
        imageSize.height = Math.round(width * ratio);
        return cb(null, null);
      });
    },
    function createBucket(result, cb) {
      s3.createBucket({Bucket: bucket + '/'}, function(err, data) {
        if(err)
          return cb(err, null);
        return cb(null, null)
      });
    },
    function resizeAndUpload(result, cb) {
      readStream = fs.createReadStream(image.path); //리드 스트림
      gm(readStream)
        .resize(imageSize.width, imageSize.height)
        .quality(100)
        .stream('jpg', function(err, stdout, stderr) {
          if (err)
            return cb(err, null);

          var buf = new Buffer(0);
          stdout.on('data', function(data) {
            buf = Buffer.concat([buf, data]);
          });
          var expires = new Date();
          expires.setDate(expires.getDate() + 1);
          stdout.on('end', function() {
            var params = {
              Bucket: bucket,
              Key: hashedName,
              Expires: expires,
//              Expires: 86400,
              Body: buf,
              ContentType: 'image/jpeg',
              ACL: 'private'
//              ACL: 'public-read'
            };
            s3.putObject(params, function (err, data) {
              if (err)
                return cb(err, null);
              return cb(null, null);
            });
          });
        });
    }
  ], function (err, results){
    if (err)
      return callback(err, null);
    var result = {
      url: AWS_CONFIG.CLOUDFRONT.URL + folder + '/' + hashedName,
      size: imageSize
    };
    return callback(null, result);
  });
}