var notiService = require('../services/notification_service');
var authUtil = require('../utils/auth_util');
var dc = require('../lib/dateConverter');

module.exports = function (app) {
  app.get('/user/notifications', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, selectNotificationList);		//알림 목록
};

function selectNotificationList (req, res, handleError){
  var userId = req.user.id;

  notiService.selectNotificationList(userId, function (err, notifications){
    if(err)
      return handleError(err);
    return res.send(200, {notifications: notifications});
  });
}