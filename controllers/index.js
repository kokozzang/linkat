var fs = require('fs');
var async = require('async');

module.exports = function(app){
//	fs.readdir(__dirname, function(err, controllers) {
//		if (err) {
//			console.error(err);
//			return ;
//		}
//		controllers.forEach(function (controller) {
//			if ('index.js' === controller) return ;
//      console.log('controller/'+ controller);
//			require(__dirname + '/' + controller)(app);
//		});
//	});

  var controllers = fs.readdirSync(__dirname);
//
//  for(var i = 0; i < controllers.length; i++){
//    if ('index.js' === controllers[i]) return ;
//    console.log('controller/'+ controllers[i]);
//    require(__dirname + '/' + controllers[i]).delegate(app);
//  }
  async.each(controllers, function(controller, callback) {
    if ('index.js' === controller) return ;
    console.log('controllers/'+ controller);
    require(__dirname + '/' + controller)(app);
    callback();
  }, function(err){
    // if any of the file processing produced an error, err would equal that error
    if( err ) {

    }
  });

  //404
//  app.get('*', function(req, res, handleError) {
//    var error = new Error();
//    error.name = 'PageNotFoundError';
//    error.httpStatus = ENUMS.HTTP_STATUS.NOT_FOUND.CODE;
//    error.message = ENUMS.HTTP_STATUS.NOT_FOUND.MESSAGE;
//    error.url = req.originalUrl;
//    handleError(error);
//  });
};
