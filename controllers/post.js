var postService = require('../services/post_service');
var _ = require('underscore');
var gm = require('gm');
var fs = require('fs');
var async = require('async');
var uuid = require('node-uuid');
var path = require('path');
var url = require('url');             // url 처리
var crypto = require('crypto');
var postValidator = require('../utils/validators/post_validator');
var authUtil = require('../utils/auth_util');
var pushUtil = require('../utils/push_util');
var request = require('request');
var cheerio = require('cheerio');
var Q = require('q');
var validator = require('validator');

module.exports = function (app) {
  //app.get('/crawl', function (req, res){
  //  var url = req.query.url;
  //  if(!validator.isURL(url))
  //    return //wefwefwefwlfjwl;efjwelfjwelfjweklfjwlekf
  //  var url = 'http://www.lotteimall.com/goods/viewGoodsDetail.lotte?goods_no=1071544201&infw_disp_no_sct_cd=20&infw_disp_no=5349296&cart_sn=1&allViewYn=N';
  //  Q().then(function () {
  //    var defer = Q.defer();
  //
  //    request({
  //      url: url,
  //      headers: {//웹 브라우져인것처럼 헤더 보내기
  //        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:37.0) Gecko/20100101 Firefox/37.0'
  //      }
  //    }, function (err, resp, body) {
  //      if (err) {
  //        defer.reject(err);
  //      } else {
  //        defer.resolve(body);
  //      }
  //    });
  //
  //    return defer.promise;
  //  }).then(function (body) {
  //    var defer = Q.defer();
  //    var $ = cheerio.load(body);
  //    var arr = [];
  //    $('img').each(function (i, img) {
  //      var src = $(img).attr('src').toString();
  //
  //      //url validation
  //      if(!validator.isURL(src))
  //        return true;
  //      if(validator.contains(src, 'icon') || validator.contains(src, 'logo'))
  //        return true;
  //      //확장자 validation
  //      if(!_.has(ENUMS.IMAGE_CONTENT_TYPE, src.split('.').pop().toLowerCase()))
  //        return true;
  //
  //      arr.push(src);
  //    });
  //    defer.resolve(arr);
  //    return defer.promise;
  //  }).then(function (classStr) {
  //    res.
  //  });
  //});


  app.get('/s3', function (req, res){
    var bucket = AWS_CONFIG.S3.BUCKET + '/50/40';
    var readStream = fs.createReadStream(path.resolve(ENUMS.UPLOAD_BASE, 'detail_02.png')); //리드 스트림
    s3.createBucket({Bucket: bucket + '/'}, function() {
      var params = {
        Bucket: bucket,
        Key: 'tlqk.png',
        Expires: 60,
        Body: readStream,
        ContentType: 'image/png',
        ACL: 'public-read'
      };
      var urlParams = {Bucket: bucket, Key: 'tlqk.png'};
//      s3.getSignedUrl('getObject', urlParams, function(err, url){
//        console.log('gg', url.split('?')[0]);
//      });
      s3.putObject(params, function(err, data) {
        if (err)
          console.log(err)
        console.log('Successfully uploaded data to ' + bucket);
      });
    });
  });
  app.get('/new',
    authUtil.accessTokenVerifier,
    authUtil.getUserIdByOAuth,
    postValidator.selectPosts,
    newPosts); //뉴스피드

  app.get('/hot',
    authUtil.accessTokenVerifier,
    authUtil.getUserIdByOAuth,
    postValidator.selectPosts,
    newPosts); //뉴스피드

  app.get('/categories/clothes',
    authUtil.accessTokenVerifier,
    authUtil.getUserIdByOAuth,
    postValidator.selectPosts,
    clothesPosts); //의류

  app.get('/categories/shoes',
    authUtil.accessTokenVerifier,
    authUtil.getUserIdByOAuth,
    postValidator.selectPosts,
    shoesPosts); //신발

  app.get('/categories/etcs',
    authUtil.accessTokenVerifier,
    authUtil.getUserIdByOAuth,
    postValidator.selectPosts,
    etcsPosts); //잡화


  //댓글 CRD
  app.post('/posts/:questionId/comments', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, postValidator.insertComment, insertComment);
  app.get('/posts/:questionId/comments', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, selectComments);
  app.delete('/posts/:questionId/comments/:commentId', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, postValidator.deleteComment, deleteComment);

  //나도궁금(질문 좋아요) CD
  app.post('/posts/:questionId/like', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, postValidator.insertQuestionLike, insertQuestionLike);
  app.delete('/posts/:questionId/like', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, postValidator.deleteQuestionLike, deleteQuestionLike);

  //갖고싶어(답변 좋아요) CD
  app.post('/posts/:questionId/answers/:answerId/like', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, postValidator.insertAnswerLike, insertAnswerLike);
  app.delete('/posts/:questionId/answers/:answerId/like', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, postValidator.deleteAnswerLike, deleteAnswerLike);


  //질문 CRUD
  app.post('/posts', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, postValidator.insertQuestion, insertQuestion); //질문 입력
  app.get('/posts/:questionId',
    authUtil.accessTokenVerifier,
    authUtil.getUserIdByOAuth,
    postValidator.selectQuestion, selectQuestion); //질문 조회
  app.put('/posts/:questionId', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, postValidator.updateQuestion, updateQuestion); //질문 수정
  app.delete('/posts/:questionId', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, postValidator.deleteQuestion, deleteQuestion);  //질문 삭제

  app.get('/posts', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, postValidator.selectPosts, selectPosts);  //뉴스피드 페이지 요청

  //답변 CUD
  app.post('/posts/:questionId/answers',
    authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, postValidator.insertAnswer, insertAnswer);  //답변 입력
//  app.post('/answer', updateAnswer);  //답변 수정
  app.delete('/posts/:questionId/answers/:answerId',
    authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, postValidator.deleteAnswer, deleteAnswer);  //답변 삭제
};


/**
 * 뉴스피드 요청 메소드
 * @param req
 * @param res
 * @param handleError
 */

function hotPosts (req, res, handleError) {
  var userId = req.user.id;
  //var userId = null;
  console.log('userId', userId);

  postService.newPosts(userId, 1, ENUMS.PAGE_LIMIT, function (err, posts) {
    if(err)
      return handleError(err);
    return res.send(200, {posts: posts});
  });
}

function newPosts (req, res, handleError) {
  var userId = req.user.id;
  //var userId = null;
  var page = req.query.page;
  var startId = req.query.startId || null;
  var limit = req.query.limit;

  console.log('startId', startId);
  console.log('page', page);
  console.log('limit', limit);

  postService.newPosts(userId, startId, page, limit, function (err, posts) {
    if(err)
      return handleError(err);
    return res.send(200, {posts: posts});
  });
}

function clothesPosts (req, res, handleError) {
  var userId = req.user.id;
  //var userId = null;
  var page = req.query.page;
  var startId = req.query.startId || null;
  var limit = req.query.limit;

  postService.categorizedPosts(userId, ENUMS.CATEGORY.CLOTHES, startId, page, limit, function (err, posts) {
    if(err)
      return handleError(err);
    return res.send(200, {posts: posts});
  });
}

function shoesPosts (req, res, handleError) {
  var userId = req.user.id;
  //var userId = null;
  var page = req.query.page;
  var startId = req.query.startId || null;
  var limit = req.query.limit;

  postService.categorizedPosts(userId, ENUMS.CATEGORY.SHOES, startId, page, limit, function (err, posts) {
    if(err)
      return handleError(err);
    return res.send(200, {posts: posts});
  });
}

function etcsPosts (req, res, handleError) {
  var userId = req.user.id;
  //var userId = null;
  var page = req.query.page;
  var startId = req.query.startId || null;
  var limit = req.query.limit;

  postService.categorizedPosts(userId, ENUMS.CATEGORY.ETCS, startId, page, limit, function (err, posts) {
    if(err)
      return handleError(err);
    return res.send(200, {posts: posts});
  });
}

function selectPosts (req, res, handleError) {
  var userId = req.user.id;
  //var userId = null;
  var page = req.query.page;
  var limit = req.query.limit;

  postService.newPosts(userId, page, limit, function (err, posts) {
    if(err)
      return handleError(err);
//    return res.render('infiniteTest', {questionResult: questionResult} );
    return res.render('infiniteTest', {posts: posts} );
  });
}

function insertComment (req, res, handleError) {
  var questionId = req.params.questionId;
  var content = req.body.content;
  var ownerId = req.body.ownerId;

  var sender = {
    userId: req.user.id,
    nickname: req.user.nickname,
    profile: req.user.profile_small_url,
    user_url: req.user.user_url
  };

  var comment = {
    question_id: questionId,  //질문 키
    user_id: sender.userId,
    content: content,
    created_date: new Date(),
    deleted_date: null
  };

  postService.insertComment(comment, ownerId, sender, function (err, result) {
    if(err)
      return handleError(err);
    console.log('result', result);
    if(result.notifications.length > 0) {
      pushUtil.sendPush(result.notifications, function (err, sendPushResult) {
        if (err)
          return handleError(err);
        //return res.send(ENUMS.HTTP_STATUS.OK.CODE, {commentId: result.commentId});
      });
    }
    return res.send(ENUMS.HTTP_STATUS.OK.CODE, {commentId: result.commentId});
  });
}

function selectComments (req, res, handleError) {
  var userId = null;
  var questionId = req.params.questionId;

  if (req.isAuthenticated()) {
    userId = req.user.id;
  }

  postService.selectComments(questionId, userId, function (err, comments) {
    if(err)
      return handleError(err);

    return res.send(ENUMS.HTTP_STATUS.OK.CODE, {comments: comments} );
  });
}

function deleteComment (req, res, handleError) {
  var commentId = req.params.commentId;
  var userId = req.user.id;

  postService.deleteComment(commentId, userId, function (err, result) {
    if(err)
      return handleError(err);
    return res.send(ENUMS.HTTP_STATUS.OK.CODE);
  });
}

function insertQuestionLike (req, res, handleError) {
  var ownerId = req.body.ownerId;

  var sender = {
    userId: req.user.id,
    nickname: req.user.nickname,
    profile: req.user.profile_small_url,
    user_url: req.user.user_url
  };

  var like = {
    question_id: req.params.questionId,  //질문 키
    user_id: sender.userId,
    created_date: new Date()
  };

  postService.insertQuestionLike(like, ownerId, sender, function (err, result) {
    if(err)
      return handleError(err);
    return res.send(ENUMS.HTTP_STATUS.OK.CODE);
  });
}

function deleteQuestionLike (req, res, handleError) {
  var questionId = req.params.questionId;
  var userId = req.user.id;

  postService.deleteQuestionLike(questionId, userId, function (err, result) {
    if(err)
      return handleError(err);
    return res.send(ENUMS.HTTP_STATUS.OK.CODE);
  });
}

function insertAnswerLike (req, res, handleError) {
  var ownerId = req.body.ownerId;
  var questionId = req.params.questionId;

  var sender = {
    userId: req.user.id,
    nickname: req.user.nickname,
    profile: req.user.profile_small_url,
    user_url: req.user.user_url
  };

  var like = {
    answer_id: req.params.answerId,  //답변 키
    user_id: sender.userId,
    created_date: new Date()
  };

  postService.insertAnswerLike(like, questionId, ownerId, sender, function (err, result) {
    if(err)
      return handleError(err);
    return res.send(ENUMS.HTTP_STATUS.OK.CODE);
  });
}

function deleteAnswerLike (req, res, handleError) {
  var answerId = req.params.answerId;
  var userId = req.user.id;

  postService.deleteAnswerLike(answerId, userId, function (err, result) {
    if(err)
      return handleError(err);
    return res.send(ENUMS.HTTP_STATUS.OK.CODE);
  });
}

/**
 * 질문 입력 요청 메소드
 * @param req
 * @param res
 * @param handleError
 */
function insertQuestion (req, res, handleError) {
  var image = req.files.imgs;

  var post = {
    user_id: req.user.id,
    gender_id: req.body.gender,
    category_id: req.body.category,
    content: req.body.content,
    created_date : new Date(),
    modified_date : null
  };
  
  async.parallel({
    SMALL: function (callback){
      s3Uploader(image, ENUMS.QUESTION_PHOTO_WIDTH.SMALL, function (err, result){
        if(err)
          {
            return callback(err, null);}
        return callback(null, result);
      });
    },
    MEDIUM: function (callback) {
      s3Uploader(image, ENUMS.QUESTION_PHOTO_WIDTH.MEDIUM, function (err, result){
        if(err)
          return callback(err, null);
        return callback(null, result);
      });
    },
    LARGE: function (callback) {
      s3Uploader(image, ENUMS.QUESTION_PHOTO_WIDTH.LARGE, function (err, result){
        if(err)
          return callback(err, null);
        return callback(null, result);
      });
    }
  }, function (err, results) {
    if(err)
      return handleError(err);

    var questionPhotos = [];
    for(var key in ENUMS.QUESTION_PHOTO_WIDTH){
      questionPhotos.push([
        null, // question_id
        results[key].size.width,  //width
        results[key].size.height, //height
        results[key].url  //url
      ]);
    }

    postService.insertQuestion(post, questionPhotos, function (err, questionId) {
      if(err)
        return handleError(err);
      return res.send(ENUMS.HTTP_STATUS.OK.CODE, {questionId: questionId} );
    });
  });
};

function selectQuestion (req, res, handleError) {
  var views = false;
  var userId = req.user.id;
  var questionId = req.params.questionId;


  postService.selectQuestion(questionId, userId, views, function (err, results) {
    if (err)
      return handleError(err);
    if (results === false)
      return res.send(ENUMS.HTTP_STATUS.NOT_FOUND.CODE);

    var post = results.post[0];
    var questionValues = {
      post: post, //질문 내용
      answers: results.answers, //답변들
      //questionComments: results.questionComments, //댓글들
      user_id: userId
    };

    return res.send(ENUMS.HTTP_STATUS.OK.CODE, questionValues);
  });
}

function updateQuestion (req, res, handlerError) {
  var questionId  = req.params.questionId;
  var data = req.body;
  var userId = req.user.id;
  var post = {
    gender_id: data.gender,
    category_id: data.category,
    content: data.content,
    modified_date: new Date()
  };

  postService.updateQuestion(questionId, userId, post, function (err, result) {
    if (err)
      return handlerError(err);
    if (result === false)
      return res.send(ENUMS.HTTP_STATUS.FORBBIDEN.CODE);
    else
      return res.send(ENUMS.HTTP_STATUS.OK.CODE);
  });
}

function deleteQuestion (req, res, handlerError) {
  var questionId = req.params.questionId;
  var userId = req.user.id;

  postService.deleteQuestion(questionId, userId, function (err, result) {
    if (err)
      return handlerError(err);
    if(result > 0)
      return res.send(ENUMS.HTTP_STATUS.OK.CODE);
    else
      return res.send(ENUMS.HTTP_STATUS.FORBBIDEN.CODE);
  });
};

function insertAnswer (req, res, handleError) {
  var image = req.files.imgs;
  var ownerId = req.body.ownerId;

  var sender = {
    userId: req.user.id,
    nickname: req.user.nickname,
    profile: req.user.profile_small_url,
    user_url: req.user.user_url
  };

  var answer = {
    user_id: sender.userId,
    question_id : req.params.questionId,
    price: req.body.price,
    link: req.body.link,
    content: req.body.content,
    created_date: new Date()
  };

  async.parallel({
    SMALL: function (callback){
      s3Uploader(image, ENUMS.ANSWER_PHOTO_WIDTH.SMALL, function (err, result){
        if(err)
          return callback(err, null);
        return callback(null, result);
      });
    },
    MEDIUM: function (callback) {
      s3Uploader(image, ENUMS.ANSWER_PHOTO_WIDTH.MEDIUM, function (err, result){
        if(err)
          return callback(err, null);
        return callback(null, result);
      });
    },
    LARGE: function (callback) {
      s3Uploader(image, ENUMS.ANSWER_PHOTO_WIDTH.LARGE, function (err, result){
        if(err)
          return callback(err, null);
        return callback(null, result);
      });
    }
  }, function (err, results) {
    if(err)
      return handleError(err, null);

    var answerPhotos = [];
    for(var key in ENUMS.ANSWER_PHOTO_WIDTH){
      answerPhotos.push([
        null, // question_id
        results[key].size.width,  //width
        results[key].size.height, //height
        results[key].url  //url
      ]);
    }

    postService.insertAnswer(answer, answerPhotos, ownerId, sender, function (err, result) {
      if(err)
        return handleError(err);

      console.log('result', result);
      if(result.notifications.length > 0) {
        pushUtil.sendPush(result.notifications, function (err, sendPushResult) {
          if (err)
            return handleError(err);
          //return res.send(ENUMS.HTTP_STATUS.OK.CODE, {answerId: result.answerId});
        });
      }
      return res.send(ENUMS.HTTP_STATUS.OK.CODE, {answerId: result.answerId});
    });
  });
}

function deleteAnswer (req, res, handlerError) {
  var answerId = req.params.answerId;
  var userId = req.user.id;

  postService.deleteAnswer(answerId, userId, function (err, result) {
    if (err)
      return handlerError(err);
    if(result === ENUMS.RESULT.SUCCESS)
      return res.send(ENUMS.HTTP_STATUS.OK.CODE);
    return res.send(ENUMS.HTTP_STATUS.OK.CODE);
  });
}


function s3Uploader (image, width, callback) {
  var originalFileName = image.originalFilename;

  //파일명 해시
  var sha1 = crypto.createHash('sha1');                    // sha1 해쉬 객체 생성
  var currentDate = (new Date()).valueOf().toString();   // 오늘 날짜 타임스탬프
  var randomNum = Math.random().toString();              // 랜덤 숫자 생성
  var hashedName = sha1.update(originalFileName + currentDate + randomNum)
    .digest('hex') + '.jpg'; //업로드할 파일 이름 생성(해쉬)

  //사이즈별 디렉토리에 해쉬파일명으로 디렉토리 생성
  var folder = ENUMS.QUESTION_PHOTO_WIDTH_NAMES[width].toLowerCase() + '/'
    + hashedName.substring(0,2) + '/'
    + hashedName.substring(2,4) + '/'
    + hashedName.substring(4,6);
  var bucket = AWS_CONFIG.S3.BUCKET + '/' + folder;

  var readStream = fs.createReadStream(image.path); //리드 스트림
  var imageSize = {};
  console.log('folder', folder);
  async.waterfall([
    function calSize(cb) {
      gm(readStream).size(function (err, size){
        if(err)
          return cb(err, null);
        var ratio = size.height / size.width;
        imageSize.width = width;
        imageSize.height = Math.round(width * ratio);
        return cb(null, null);
      });
    },
    function createBucket(result, cb) {
      s3.createBucket({Bucket: bucket + '/'}, function(err, data) {
        if(err)
          return cb(err, null);
        return cb(null, null)
      });
    },
    function resizeAndUpload(result, cb) {
      readStream = fs.createReadStream(image.path); //리드 스트림
      gm(readStream)
        .quality(100)
        .resize(imageSize.width, imageSize.height)
        .stream('jpg', function(err, stdout, stderr) {
          if (err)
            return cb(err, null);

          var buf = new Buffer(0);
          stdout.on('data', function(data) {
            buf = Buffer.concat([buf, data]);
          });
          var expires = new Date();
          expires.setDate(expires.getDate() + 1);
          stdout.on('end', function() {
            var params = {
              Bucket: bucket,
              Key: hashedName,
              Expires: expires,
//              Expires: 86400,
              Body: buf,
              ContentType: 'image/jpeg',
              ACL: 'private'
//              ACL: 'public-read'
            };
            s3.putObject(params, function (err, data) {
              if (err)
                return cb(err, null);
              return cb(null, null);
            });
          });
        });
    }
  ], function (err, results){
    if (err)
      return callback(err, null);
    var result = {
      url: AWS_CONFIG.CLOUDFRONT.URL + folder + '/' + hashedName,
      size: imageSize
    };
    return callback(null, result);
  });
}

function fileChecker(file){
  console.log("file.headers['content-type']", file.headers['content-type']);
  console.log('file.type', file.type);
  console.log('file.originalFilename', file.originalFilename);
//  var ext = (/[.]/.exec(file.originalFilename)) ? /[^.]+$/.exec(file.originalFilename) : undefined;
  var ext = (/[.]/.exec('gggg')) ? /[^.]+$/.exec('gggg') : undefined;
  console.log('ext', ext);
//  console.log('type', ext[0]);
}