var mypageService = require('../services/mypage_service');
var fs = require('fs');
var async = require('async');
var authUtil = require('../utils/auth_util');
var userValidator = require('../utils/validators/user_validator');

module.exports = function (app) {
  app.get('/users/:userUrl', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, userValidator.userUrlParamValidation, index);
  app.get('/users/:userUrl/qlikes', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, userValidator.userUrlParamValidation, userValidator.mypageQuery, selectLikedQuestions);
  app.get('/users/:userUrl/alikes', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, userValidator.userUrlParamValidation, userValidator.mypageQuery, selectLikedAnswers);
  app.get('/users/:userUrl/questions', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, userValidator.userUrlParamValidation, userValidator.mypageQuery, selectQuestions);
  app.get('/users/:userUrl/answers', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, userValidator.userUrlParamValidation, userValidator.mypageQuery, selectAnswers);

  app.post('/follow', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, userValidator.insertFollow, insertFollow);
  app.delete('/follow/:targetId', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, userValidator.deleteFollow, deleteFollow);

  app.get('/users/:userUrl/followers', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, userValidator.userUrlParamValidation, selectFollowers);
  app.get('/users/:userUrl/followings', authUtil.accessTokenVerifier, authUtil.getUserIdByOAuth, userValidator.userUrlParamValidation, selectFollowings);
};

function index(req, res, handleError) {
  var userId = req.user.id;
  var userUrl = req.params.userUrl;

  mypageService.index(userUrl, userId, function (err, user) {
    if (err) {
      return handleError(err);
    }
    return res.send(ENUMS.HTTP_STATUS.OK.CODE, {user: user});
  });
}

function selectLikedQuestions (req, res, handleError) {
  var userId = req.user.id;
  var userUrl = req.params.userUrl;

  var page = req.query.page;
  var startId = req.query.startId || null;
  var limit = req.query.limit;

  mypageService.selectLikedQuestions(userUrl, userId, startId, page, limit, function (err, questions) {
    if(err)
      return handleError(err);
    return res.send(ENUMS.HTTP_STATUS.OK.CODE, {questions: questions} );
  });
}

function selectLikedAnswers (req, res, handleError) {
  var userId = req.user.id;
  var userUrl = req.params.userUrl;

  var page = req.query.page;
  var startId = req.query.startId || null;
  var limit = req.query.limit;

  mypageService.selectLikedAnswers(userUrl, userId, startId, page, limit, function (err, answers) {
    if(err)
      return handleError(err);
    return res.send(ENUMS.HTTP_STATUS.OK.CODE, {answers: answers} );
  });
}

function selectQuestions(req, res, handleError) {
  var userId = req.user.id;
  var userUrl = req.params.userUrl;

  var page = req.query.page;
  var startId = req.query.startId || null;
  var limit = req.query.limit;

  mypageService.selectQuestions(userUrl, userId, startId, page, limit, function (err, questions) {
    if(err){
      return handleError(err);
    }
    return res.send(ENUMS.HTTP_STATUS.OK.CODE, {questions: questions} );
  });
}

function selectAnswers(req, res, handleError) {
  var userId = req.user.id;
  var userUrl = req.params.userUrl;

  var page = req.query.page;
  var startId = req.query.startId || null;
  var limit = req.query.limit;

  mypageService.selectAnswers(userUrl, userId, startId, page, limit, function (err, answers) {
    if(err) {return handleError(err); }

    return res.send(ENUMS.HTTP_STATUS.OK.CODE, {answers: answers} );
  });
}

function insertFollow (req, res, handleError) {
  var userId = req.user.id;
  var targetId = req.body.targetId;

  mypageService.insertFollow(targetId, userId, function (err, result){
    if(err) { return handleError(err);}
    return res.send(ENUMS.HTTP_STATUS.OK.CODE);
  });
}

function deleteFollow (req, res, handleError) {
  var userId = req.user.id;
  var targetId = req.params.targetId;

  mypageService.deleteFollow(targetId, userId, function (err, result){
    if(err) { return handleError(err);}
    return res.send(ENUMS.HTTP_STATUS.OK.CODE);
  });
}

function selectFollowers(req, res, handleError) {
  var userId = req.user.id;
  var userUrl = req.params.userUrl;

  mypageService.selectFollowers(userUrl, userId, function (err, followers) {
    if(err) { return handleError(err);}

    return res.send(ENUMS.HTTP_STATUS.OK.CODE, {followers: followers} );
  });
}

function selectFollowings(req, res, handleError) {
  var userId = req.user.id;
  var userUrl = req.params.userUrl;

  mypageService.selectFollowings(userUrl, userId, function (err, followings) {
    if(err) { return handleError(err);}
    return res.send(ENUMS.HTTP_STATUS.OK.CODE, {followings: followings} );
  });
}