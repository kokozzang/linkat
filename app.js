var yaml = require('js-yaml');
var express = require('express');
var redisStore = require('connect-redis')(express);
var http = require('http');
var path = require('path');
var fs = require('fs');
var expressValidator = require('express-validator');
var passport = require('passport');
var db = require('./db');
var expressUtil = require('./utils/express_util');
var expressValidatorConfig = require('./config/expressValidatorConfig');
//require('./extends/response');
var AWS = require('aws-sdk');
var cors = require('cors');

global.ENUMS = require('./db/enums');
var app = express();

var configPath = __dirname + '/config';
if(process.env.NODE_ENV === ENUMS.NODE_ENV.PRODUCTION){
  global.CHOPSTER_CONFIG = yaml.safeLoad(fs.readFileSync(configPath + '/default.config.yml', 'utf8')).PRODUCTION;
  global.AWS_CONFIG = yaml.safeLoad(fs.readFileSync(configPath + '/default.aws.yml', 'utf8')).PRODUCTION;
}else {
  global.CHOPSTER_CONFIG = yaml.safeLoad(fs.readFileSync(configPath + '/default.config.yml', 'utf8')).DEVELOP;
  global.AWS_CONFIG = yaml.safeLoad(fs.readFileSync(configPath + '/default.aws.yml', 'utf8')).DEVELOP;
}


ENUMS.S3_URL = 'https://' + AWS_CONFIG.S3.BUCKET + '.' + ENUMS.S3_URL;

function yamlGet(key, defaultValue) {
  var dir = key.split('.');
  var v = this;
  for (var i = 0; i < dir.length; i++) {
    v = v[dir[i]];
    if (!v)
      return defaultValue || undefined;
  }
  return v;
};
global.PUSH_CONFIG = yaml.safeLoad(fs.readFileSync(configPath + '/default.push.yml', 'utf8'));
global.OAUTH_CONFIG = yaml.safeLoad(fs.readFileSync(configPath + '/default.oauth.yml', 'utf8'));

global.CHOPSTER_CONFIG.get = yamlGet;
global.AWS_CONFIG.get = yamlGet;
global.OAUTH_CONFIG.get = yamlGet;

//아마존 셋팅
AWS.config.region = AWS_CONFIG.REGION;
AWS.config.apiVersions = {
  s3: '2006-03-01'
  // other service API versions
};

global.s3 = new AWS.S3(); //S3


//액세스 로그
var accesslogger = require('./utils/logger').accesslogger;
var errorlogger = require('./utils/logger').errorlogger;



sessionStore = new redisStore({
  host: CHOPSTER_CONFIG.REDIS.HOST,
  port: CHOPSTER_CONFIG.REDIS.PORT,
  prefix: CHOPSTER_CONFIG.REDIS.PREFIX
});

(function initApplication(app) {
  console.log('initApplication');
  global.POOL = db.mysqlInit(CHOPSTER_CONFIG.MYSQL);
  db.mongoInit(CHOPSTER_CONFIG.MONGODB);
  console.log('db_init');

  // all environments
  app.set('port', CHOPSTER_CONFIG.SERVER.PORT);
  app.set('views', __dirname + '/views');
  app.locals.basedir = app.get('views');
  app.set('view engine', 'jade');
  app.use(cors({credentials: true}));
  app.use(express.compress());
  app.use(express.favicon());

  var winstonStream = {
    write: function(message, encoding){
      accesslogger.info(message);
    }
  };
  // default ':remote-addr - - [:date] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent"'
  // :response-time ms
  var opt = {
    stream: winstonStream,
    format: ':remote-addr - - [:date] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" :response-time ms'
  };
  app.use(express.logger('dev'));
//  app.use(express.logger(opt));
  app.use('/uploads', express.directory(path.join(__dirname, 'public/upload')));
  app.use('/uploads', express.static(path.join(__dirname, 'public/upload'), {maxAge: 3600000*24*7}));
//  app.use(express.static(path.join(__dirname, 'public'), {maxAge: 7*24*60*60*1000}));
//  app.use(express.static(path.join(__dirname, 'public/css'), {maxAge: 7*24*60*60*1000}));
  app.use(express.static(path.join(__dirname, 'public'), {maxAge: 3600000*24*7}));//1주일
//  app.use(express.static(path.join(__dirname, 'public/css'), {maxAge: 3600000*24*7}));//1주일
//  app.use(express.bodyParser({ uploadDir: __dirname + '/public/upload/tmp'}));
  app.use(express.bodyParser());
  app.use(expressValidator(expressValidatorConfig));
  app.use(express.cookieParser('cookie'));
  app.use(express.session({
    store: sessionStore,
    secret: CHOPSTER_CONFIG.SESSION.SECRET,
    key: CHOPSTER_CONFIG.SESSION.KEY,
    cookie: {maxAge: CHOPSTER_CONFIG.SESSION.COOKIE.MAXAGE}
  }));
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(express.methodOverride());
  app.use(express.multipart());       //업로드 가능
  app.use(app.router);

//  app.use(function (req, res, next) {
//    res.locals.require = require;
//    res.locals.req = req;
//    res.locals.session = req.session;
//    next();
//  });

//  app.use(expressUtil.responseHelpers());
  require('./oauths')(app, passport);  //oauth 셋팅
  require('./controllers')(app);
  app.use(function pageNotFound(req, res, next){
    //404
    var error = new Error();
    error.name = 'PageNotFoundError';
    error.httpStatus = ENUMS.HTTP_STATUS.NOT_FOUND.CODE;
    error.message = ENUMS.HTTP_STATUS.NOT_FOUND.MESSAGE;
    error.url = req.originalUrl;
    next(error);
  });

  app.use(function errorLogger(err, req, res, next){
    if(err){
      var message = null;
      if(!err.httpStatus){
        err.httpStatus = err.httpStatus || ENUMS.HTTP_STATUS.INTERNAL_SERVER_ERROR.CODE;
        message = ENUMS.HTTP_STATUS.INTERNAL_SERVER_ERROR.MESSAGE;
      }else{//404
        message = err.message;
      }
      errorlogger.log('error', message, err);
      next(err);
    }
    next();
  });
  app.use(expressUtil.errorHandler);

  var io = null;
//  var httpsServer = http.createServer(app).listen(app.get('port'), function(){
  var httpsServer = http.createServer(app).listen(app.get('port'), function(){
    console.log('Server listening on port ' + app.get('port'));
    //socket.io setting
//    io = require('socket.io').listen(httpsServer);
    io = require('socket.io').listen(httpsServer, {
      logger: {
        debug: errorlogger.debug,
        info: errorlogger.info,
        error: errorlogger.error,
        warn: errorlogger.warn
      }
    });
    require('./socket_test').init(io, sessionStore);
//    require('./controllers').delegate(app);
    //    require('./batch').init();
  });
}(app));